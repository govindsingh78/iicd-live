<?php require_once 'rightusercheck.php';?>
<?php
require_once '../meekrodb.2.3.class.php';
$id = $_GET['id'];
?>
<?php
if (isset($_POST['submit'])) {

    if (isset($_FILES['applicant_photo'])) {

        if ($_FILES["applicant_photo"]["error"] == 0) {
            $temp_name = $_FILES['applicant_photo']['tmp_name'];
            $img_name = $_FILES['applicant_photo']['name'];
            $imgtype = $_FILES["applicant_photo"]["type"];
            $ext = pathinfo($img_name, PATHINFO_EXTENSION);
            $file_name = 'applicant_photo/' . time() . '_' . rand(0, 99) . '.' . $ext;
            $target_path = "../images/" . $file_name;
            move_uploaded_file($temp_name, $target_path);
        }

        $update = DB::update('user_details', array(
            'applicant_photo' => $file_name,
        ), "user_id=%i", $id);

        if ($update == true) {
            ?>
        <script>
          alert('Applicant Photo Successfully Updated!');
        </script>
        <?php
}

    } elseif (isset($_FILES['signatures'])) {
        if ($_FILES["signatures"]["error"] == 0) {
            $temp_name = $_FILES['signatures']['tmp_name'];
            $img_name = $_FILES['signatures']['name'];
            $imgtype = $_FILES["signatures"]["type"];
            $ext = pathinfo($img_name, PATHINFO_EXTENSION);
            $file_name = 'signatures/' . time() . '_' . rand(0, 99) . '.' . $ext;
            $target_path = "../images/" . $file_name;
            move_uploaded_file($temp_name, $target_path);
        }
        $update = DB::update('user_details', array(
            'signatures' => $file_name,
        ), "user_id=%i", $id);

        if ($update == true) {
            ?>
        <script>
          alert('Signature Successfully Updated!');
        </script>
        <?php
}
    } elseif (isset($_FILES['id_proof_front'])) {
        if ($_FILES["id_proof_front"]["error"] == 0) {
            $temp_name = $_FILES['id_proof_front']['tmp_name'];
            $img_name = $_FILES['id_proof_front']['name'];
            $imgtype = $_FILES["id_proof_front"]["type"];
            $ext = pathinfo($img_name, PATHINFO_EXTENSION);
            $file_name = 'id_proof_front/' . time() . '_' . rand(0, 99) . '.' . $ext;
            $target_path = "../images/" . $file_name;
            move_uploaded_file($temp_name, $target_path);
        }

        $update = DB::update('user_details', array(
            'id_proof_front' => $file_name,
        ), "user_id=%i", $id);

        if ($update == true) {
            ?>
        <script>
          alert('File Uploded Successfully !');
        </script>
        <?php
}
    } else {
        if ($_FILES["id_proof_back"]["error"] == 0) {
            $temp_name = $_FILES['id_proof_back']['tmp_name'];
            $img_name = $_FILES['id_proof_back']['name'];
            $imgtype = $_FILES["id_proof_back"]["type"];
            $ext = pathinfo($img_name, PATHINFO_EXTENSION);
            $file_name = 'id_proof_back/' . time() . '_' . rand(0, 99) . '.' . $ext;
            $target_path = "../images/" . $file_name;
            move_uploaded_file($temp_name, $target_path);
        }

        $update = DB::update('user_details', array(
            'id_proof_back' => $file_name,
        ), "user_id=%i", $id);

        if ($update == true) {
            ?>
        <script>
          alert('File Uploded Successfully !');
        </script>
        <?php
}
    }

}

function countryNameById($countryid)
{
    $sql = "SELECT country_name FROM country WHERE id = '" . $countryid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['country_name'];
}

function cityNameById($cityid)
{
    $sql = "SELECT city_name FROM city WHERE id = '" . $cityid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['city_name'];
}

function stateNameById($stateid)
{
    $sql = "SELECT state_name FROM state WHERE id = '" . $stateid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['state_name'];
}

$fetch = DB::queryFirstRow("SELECT * FROM user_details WHERE user_id=%i", $id);
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->

         <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- x-editable (bootstrap version) -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.6/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.4.6/bootstrap-editable/js/bootstrap-editable.min.js"></script>

        <!-- main.js -->
        <script src="main.js"></script>

    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once 'header.php';?>
                <?php
$usr = DB::queryFirstRow("SELECT * FROM users WHERE id=%i", $id);
$user = DB::queryFirstRow("SELECT * FROM user_details WHERE user_id=%i", $usr['id']);
?>
                    <!-- Page content -->
                    <div id="page-content">
                      <div class="row" style="padding: 20px">
                      <div class="col-md-12">
                        <div class="col-md-4"> <a href="users_list.php" class="btn btn-effect-ripple btn-danger">Back</a></div>
                          <div class="col-md-4">
                            <?php if ($usr['status'] == 0 && $usr['email_sent'] == 1 && $usr['payment_status'] == 'success') {
    ?>
                           <a href="#" title="Approve" class="btn btn-effect-ripple btn-xs btn-default" onClick="UpdateRecord(<?php echo $usr['id']; ?>);"><i class="fa fa-check">Verify</i></a>
                           <?php } elseif ($usr['status'] == 0 && $usr['email_sent'] == 0 && $usr['payment_status'] != 'success') {?> <p> update payment info to verify </p>

                           <?php } elseif ($usr['status'] == 1 && $usr['email_sent'] == 1 && $usr['payment_status'] == 'success') {?> <p style="color: green"> Verified </p>

<?php } else {?>
                             <!-- <p>Verified</p> -->

                            <?php }?>

                         </div>

                          <div class="col-md-4">
                            <?php if ($usr['email_sent'] == 0 && $usr['payment_status'] == 'success') {
    ?>
                           <button name="email_send"   class="btn btn-effect-ripple btn-xs btn-default" onClick="SendEmail(<?php echo $usr['id']; ?>);"> Send Email </button>
                           <?php } elseif ($usr['email_sent'] == 0 && $usr['payment_status'] == 'failure') {

    ?>
                            <p>Update payment info to send email</p>
                          <?php } elseif ($usr['email_sent'] == 0 && $usr['payment_status'] == null) {?>

                          <p>Update payment info to send email</p>

                           <?php } else {?>
                                    <p>Email Sent</p>
                           <?php }?>

                         </div>
                         </div>
                      </div>



          <!--### Here Admit Can only be previewed once the candidate status not 0 Then only we can generate or email there admit card -->

                  <?php
if ($usr['status'] == 0) {
    echo "<span style='font-size: 10px; color: red; margin: 3px'>Not Applicable for Generating Admit Card</span>";
} else {
    ?>
                  <form>
                  <input type="button" value="Preview Admit Card" onclick="openWin()">
                  </form>
                  <script>
                  function openWin() {
                  window.open("https://www.iicd.ac.in/admin/admit-card-preview.php?id=<?php echo base64_encode($id); ?>");
                  }
                  </script>
                  <?php if ($usr['pdf_sent'] == 0 || $usr['pdf_sent'] == null) {?>
                  <input id="sendadmit" name="admit" type="button" value="Send Admit card" onclick="SendAdmit(<?php echo $id; ?>)">
                  <?php } else {?>
                  <p>Admit card sent</p>
                  <?php
}
}
?>

        <!--### Here Admit Can only be previewed once the candidate status not 0 Then only we can generate or email there admit card -->





                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full">


<!-- ##### Payment Mesage will be shown here wheather or not payment is approved ##### -->
<?php
if (isset($_REQUEST['payment_success'])) {
    $paymentSuccess = $_REQUEST['payment_success'];
    echo '<div class="alert alert-success" id="flash-msg">' . $paymentSuccess . '</div>';
} else if (isset($_REQUEST['payment_failed'])) {
    $paymentFailed = $_REQUEST['payment_failed'];
    echo '<div class="alert alert-danger" id="flash-msg-fail">' . $paymentFailed . '</div>';
}
?>


<!-- ##### Payment Mesage will be shown here wheather or not payment is approved ##### -->


                         <div class="table-responsive">
                           <form action="update_pay_status.php" method="post" class="form-horizontal form-bordered" style="<?php if ($usr['email_sent'] == 1) {echo "display: none";
}?>">
                            <input type="hidden" name="id" value="<?php echo $usr['id']; ?>">
                                  <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-text-input">Payment Status</label>
                                            <div class="col-md-6">
                                                <select id="payment_status" name="payment_status" class="form-control" required>
                                                    <option value="">Please select</option>
                                                    <option value="success" <?php if ($usr['payment_status'] == 'success') {echo "selected";}?>>Success</option>
                                                    <option value="failure" <?php if ($usr['payment_status'] == 'failure') {echo "selected";}?>>Failure</option>
                                                </select>
                                            </div>
                                  </div>
                                 <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-text-input">Payment ID</label>
                                            <div class="col-md-6">
                                                <input type="text" value="<?php echo $usr['txnid']; ?>" id="txnid" name="txnid" class="form-control" placeholder="PaymentId" style="height: 36px" required>
                                            </div>
                                        </div>

                                  <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button type="submit" class="btn btn-effect-ripple btn-primary" >Save</button>
                                            </div>
                                        </div>

                              </form>

                              <?php // echo "<pre>"; print_r($user); ?>
                            <table class="table table-hover">
                              <tr>
                                <th colspan="2" style="text-align: center;font-size: 20px;background-color: #eee;">User Details</th>
                              </tr>
                              <tr>
                                <th>Payment Status</th>
                                <td><?php if ($usr['payment_status'] != null) {
    echo $usr['payment_status'];
} else {
    echo "Not Done";
}?>
                                 </td>
                                </td>
                              </tr>

                              <tr>
                                <th>Last Active time</th>
                                <td><?php if (!empty($user['updated_at'])) {
    echo date('d/m/y H:i:s', strtotime($usr['updated_at']));
} else {
    echo "Not active after registration";
}?></td>

                                </td>
                              </tr>
                              <tr>
                                <th>First Name</th>
                                <td>
                                  <a href="#" class="xfirst_name" data-type="text" data-name="first_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit First Name"><?php echo $usr['first_name']; ?></a>
                                </td>
                              </tr>
                              <tr>
                                <th>Middle Name</th>
                                <td>
                                  <a href="#" class="xmiddle_name" data-type="text" data-name="middle_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Middle Name">
                                  <?php
if (!empty($usr['middle_name'])) {
    echo $usr['middle_name'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Last Name</th>
                                <td>
                                  <a href="#" class="xlast_name" data-type="text" data-name="last_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Last Name">
                                  <?php
if (!empty($usr['last_name'])) {
    echo $usr['last_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Email</th>
                                <td>
                                  <a href="#" class="xemail" data-type="text" data-name="email" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Email">
                                  <?php
if (!empty($usr['email'])) {
    echo $usr['email'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Programme</th>
                                <td>
                                  <a href="#" class="xProgramme" data-type="select" data-name="Programme" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit Programme">
                                  <?php
if (!empty($user['Programme'])) {
    echo $user['Programme'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Date of Birth</th>
                                <td>
                                  <a href="#" class="xdob" data-type="date" data-viewformat="dd.mm.yyyy" data-name="dob" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit DOB">
                                  <?php
if (!empty($user['dob'])) {
    echo $user['dob'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Gender</th>
                                <td>
                                  <a href="#" class="xgender" data-type="select" data-name="gender" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit Gender"><?php echo $user['gender']; ?></a>
                                 </td>
                              </tr>
                              <tr>
                                <th>Marital Status</th>
                                <td>
                                 <?php
if ($user['marital_status'] == 'Married') {
    $mstatus = 'Married';
} else if ($user['marital_status'] == 'Single') {
    $mstatus = 'Single';
} else if ($user['marital_status'] == 'Separated') {
    $mstatus = 'Separated';
} else if ($user['marital_status'] == 'Widowed') {
    $mstatus = 'Widowed';
} else {
    $mstatus = 'NA';
}
?>
                                  <a href="#" class="xmstatus" data-type="select" data-name="marital_status" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit Marital Status"><?php echo $mstatus; ?></a>
                                 </td>
                                 </td>
                              </tr>
                              <tr>
                                <th>Category</th>
                                <td>
                                  <a href="#" class="xcategory" data-type="select" data-name="category" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit Category">
                                  <?php
if (!empty($user['category'])) {
    echo $user['category'];
} else {
    echo "NA";
}
?>

                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Nationality</th>
                                <td>
                                  <a href="#" class="xnationality" data-type="select" data-name="nationality" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit Nationality">
                                  <?php
if (!empty($user['nationality'])) {
    echo $user['nationality'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                              </tr>
                              <tr>
                                <th>Domicile Residence of Rajasthan State</th>
                                <td>
                                  <a href="#" class="xdomicile" data-type="select" data-name="domicile" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Domicile">
                                  <?php
if (!empty($user['domicile'])) {
    echo $user['domicile'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Mobile Number (Candidate)</th>
                                <td>
                                  <a href="#" class="xphone" data-type="text" data-name="phone" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit phone">
                                  <?php
if (!empty($user['phone'])) {
    echo $user['phone'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Fathers Name</th>
                                <td>
                                  <a href="#" class="xfathers_name" data-type="text" data-name="fathers_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Fathers Name">
                                  <?php
if (!empty($user['fathers_name'])) {
    echo $user['fathers_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                              </tr>
                              <tr>
                                <th>Mothers Name</th>
                                <td>
                                  <a href="#" class="xmothers_name" data-type="text" data-name="mothers_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Mothers Name">
                                  <?php
if (!empty($user['mothers_name'])) {
    echo $user['mothers_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                                </td>
                              </tr>
                              <!--###### New Fields Editable Section from Step 3 ######-->
                              <tr>
                                <th>Bhamashah</th>
                                <td>
                                  <a href="#" class="xbhamashah" data-type="text" data-name="bhamashah" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Bhamashah">
                                  <?php
if (!empty($user['bhamashah'])) {
    echo $user['bhamashah'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                                </td>
                              </tr>

                              <tr>
                                <th>Alternate Email</th>
                                <td>
                                  <a href="#" class="xalternate_email" data-type="text" data-name="alternate_email" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Alternate Email">
                                  <?php
if (!empty($user['alternate_email'])) {
    echo $user['alternate_email'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                                </td>
                              </tr>

                              <tr>
                                <th>Religion</th>
                                <td>
                                 <?php
if ($user['religion'] == 'Hindu') {
    $mreligion = 'Hindu';
} else if ($user['religion'] == 'Muslim') {
    $mreligion = 'Muslim';
} else if ($user['religion'] == 'Sikh') {
    $mreligion = 'Sikh';
} else if ($user['religion'] == 'Christian') {
    $mreligion = 'Christian';
} else if ($user['religion'] == 'Jain') {
    $mreligion = 'Jain';
} else if ($user['religion'] == 'Buddhist') {
    $mreligion = 'Buddhist';
} else if ($user['religion'] == 'Other') {
    $mreligion = 'Other';
} else {
    $mreligion = 'NA';
}
?>
                                  <a href="#" class="xreligion" data-type="select" data-name="religion" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select Religion"><?php echo $mreligion; ?></a>
                                 </td>
                                 </td>
                              </tr>



                               <tr>
                                <th>Other Religion.</th>
                                <td>
                                  <a href="#" class="xother_religion" data-type="text" data-name="other_religion" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Other Religion">
                                  <?php
if (!empty($user['other_religion'])) {
    echo $user['other_religion'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                                </td>
                              </tr>

                               <tr>
                                <th>Minority</th>
                                <td>
                                 <?php
if ($user['minority'] == 'Yes') {
    $minority = 'Yes';
} else if ($user['minority'] == 'No') {
    $minority = 'No';
} else {
    $minority = 'NA';
}
?>
                                  <a href="#" class="xminority" data-type="select" data-name="minority" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select Minority"><?php echo $minority; ?></a>
                                 </td>
                                 </td>
                              </tr>



                              <tr>
                                <th>Is student belongs to BPL/APL?  </th>
                                <td>
                                 <?php
if ($user['bpl_apl'] == 'Yes') {
    $bpl_apl = 'Yes';
} else if ($user['bpl_apl'] == 'No') {
    $bpl_apl = 'No';
} else {
    $bpl_apl = 'NA';
}
?>
                                  <a href="#" class="xbpl_apl" data-type="select" data-name="bpl_apl" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select if student belongs to BPL/APL?"><?php echo $bpl_apl; ?></a>
                                 </td>
                                 </td>
                              </tr>

                              <tr>
                                <th>Emergency Phone No.</th>
                                <td>
                                  <a href="#" class="xemergency_phone_no" data-type="text" data-name="emergency_phone_no" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Emergency Phone No.">
                                  <?php
if (!empty($user['emergency_phone_no'])) {
    echo $user['emergency_phone_no'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                                </td>
                              </tr>

                              <tr>
                                <th>Caste Category</th>
                                <td>
                                 <?php
if ($user['caste_category'] == 'General') {
    $caste_category = 'General';
} else if ($user['caste_category'] == 'SC') {
    $caste_category = 'SC';
} else if ($user['caste_category'] == 'ST') {
    $caste_category = 'ST';
} else if ($user['caste_category'] == 'BC- Non Creamy Layer') {
    $caste_category = 'BC- Non Creamy Layer';
} else if ($user['caste_category'] == 'MBC- Non Creamy Layer') {
    $caste_category = 'MBC- Non Creamy Layer';
} else {
    $caste_category = 'NA';
}
?>
                                  <a href="#" class="xcaste_category" data-type="select" data-name="caste_category" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select Caste Category"><?php echo $caste_category; ?></a>
                                 </td>
                                 </td>
                              </tr>


                              <tr>
                                <th>Other Category </th>
                                <td>
                                 <?php
if ($user['other_category'] == 'Kashmiri Migrants (KM)') {
    $other_category = 'Kashmiri Migrants (KM)';
} else if ($user['other_category'] == 'Differently/Specially Abled') {
    $other_category = 'Differently/Specially Abled';
} else if ($user['other_category'] == 'None') {
    $other_category = 'None';
} else {
    $other_category = 'NA';
}
?>
                                  <a href="#" class="xother_category" data-type="select" data-name="other_category" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select Other Category"><?php echo $other_category; ?></a>
                                 </td>
                                 </td>
                              </tr>

                              <tr>
                                <th>Student belongs to Area </th>
                                <td>
                                 <?php
if ($user['students_area'] == 'Urban') {
    $students_area = 'Urban';
} else if ($user['students_area'] == 'Rural') {
    $students_area = 'Rural';
} else {
    $students_area = 'NA';
}
?>
                                  <a href="#" class="xstudents_area" data-type="select" data-name="students_area" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select student belongs to Area?"><?php echo $students_area; ?></a>
                                 </td>
                                 </td>
                              </tr>

                              <!--###### New Fields Editable Section from Step 3 ######-->



                              <tr>
                                <th>Phone Father</th>
                                <td>
                                  <a href="#" class="xphone_father" data-type="text" data-name="phone_father" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Phone Father">
                                  <?php
if (!empty($user['phone_father'])) {
    echo $user['phone_father'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Email Father</th>
                                <td>
                                  <a href="#" class="xemail_father" data-type="text" data-name="email_father" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Email Father">
                                  <?php
if (!empty($user['email_father'])) {
    echo $user['email_father'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Phone Mother</th>
                                <td>
                                  <a href="#" class="xphone_mother" data-type="text" data-name="phone_mother" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Parents Phone no.">
                                    <?php
if (!empty($user['phone_mother'])) {
    echo $user['phone_mother'];
} else {
    echo "NA";
}
?>
                                    </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Email Mother</th>
                                <td>
                                  <a href="#" class="xemail_mother" data-type="text" data-name="email_mother" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Email Mother">
                                  <?php
if (!empty($user['email_mother'])) {
    echo $user['email_mother'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Local Guardians Name</th>
                                <td>
                                  <a href="#" class="xguardians_name" data-type="text" data-name="guardians_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Guardians Name">
                                  <?php
if (!empty($user['guardians_name'])) {
    echo $user['guardians_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Relation with Local Guardian.</th>
                                <td>
                                  <a href="#" class="xguardians_relation" data-type="text" data-name="guardians_relation" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Guardians relation">
                                  <?php
if (!empty($user['guardians_relation'])) {
    echo $user['guardians_relation'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                              </tr>
                              <tr>
                                <th>Local Guardian Phone</th>
                                <td>
                                  <a href="#" class="xphone_guardian" data-type="text" data-name="phone_guardian" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Guardians phone no.">
                                  <?php
if (!empty($user['phone_guardian'])) {
    echo $user['phone_guardian'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                              </tr>
                              <tr>
                                <th>Local Guardian Email</th>
                                <td>
                                  <a href="#" class="xemail_guardian" data-type="text" data-name="email_guardian" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Guardians email">
                                  <?php
if (!empty($user['email_guardian'])) {
    echo $user['email_guardian'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                                </td>
                              </tr>
                              <tr>
                                <th>Craft Relation</th>
                                <td>
                                  <a href="#" class="xcraft_relation" data-type="select" data-name="craft_relation" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select to Edit Craft Relation">
                                  <?php
if (!empty($user['craft_relation'])) {
    echo $user['craft_relation'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Craft Name</th>
                                <td>
                                  <a href="#" class="xcraft_name" data-type="text" data-name="craft_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Craft Name">
                                  <?php
if (!empty($user['craft_name'])) {
    echo $user['craft_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                 </td>
                              </tr>
                              <tr>
                                <th>Family Income</th>
                                <td>
                                  <a href="#" class="xfamily_income" data-type="select" data-name="family_income" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Family Income">
                                  <?php
if (!empty($user['family_income'])) {
    echo $user['family_income'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Medical / Health Information</th>
                                <td>
                                  <a href="#" class="xmedical_info" data-type="textarea" data-name="medical_info" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Medical Info">
                                  <?php
if (!empty($user['medical_info'])) {
    echo $user['medical_info'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th  >Address</th>
                                <td>
                                  <a href="#" class="xaddress_line1" data-type="text" data-name="address_line1" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit address line1">
                                  <?php
if (!empty($user['address_line1'])) {
    echo $user['address_line1'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>


                             <tr>
                                <th  >Village Name</th>
                                <td>
                                  <a href="#" class="xvillage_name" data-type="text" data-name="village_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit village name">
                                  <?php
if (!empty($user['village_name'])) {
    echo $user['village_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>

                               <tr>
                                <th  >Block or Tehsil</th>
                                <td>
                                  <a href="#" class="xblock_or_tehsil" data-type="text" data-name="block_or_tehsil" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Block/Tehsil">
                                  <?php
if (!empty($user['block_or_tehsil'])) {
    echo $user['block_or_tehsil'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>


                              <tr>
                                <th>Nationality</th>
                                <td>

                                  <?php
$cn = countryNameById($user['country']);
echo $cn;
?>
                                </td>
                              </tr>
                              <tr>
                                <th>State</th>
                                <td>

                                   <?php
$s = stateNameById($user['state']);
echo $s;
?>
                                </td>
                              </tr>
                              <tr>
                                <th>City</th>
                                <td>
                                   <?php
$c = cityNameById($user['city']);
echo $c;
?>
                                </td>
                              </tr>
                              <tr>
                                <th>Pin Code</th>
                                <td>
                                  <a href="#" class="xpin_code" data-type="text" data-name="pin_code" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Pin Code">
                                  <?php
if (!empty($user['pin_code'])) {
    echo $user['pin_code'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>





                                <tr>
                                <th>Select ID Type </th>
                                <td>
                                 <?php
if ($user['identity_card'] == 'UID Aadhar') {
    $identity_card = 'UID Aadhar';
} else if ($user['identity_card'] == 'Driver\'s License') {
    $identity_card = 'Driver\'s License';
} else if ($user['identity_card'] == 'Passport') {
    $identity_card = 'Passport';
} else if ($user['identity_card'] == 'Voter ID') {
    $identity_card = 'Voter ID';
} else {
    $identity_card = 'NA';
}
?>
                                  <a href="#" class="xidentity_card" data-type="select" data-name="identity_card" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Select ID Card Type"><?php echo $identity_card; ?></a>
                                 </td>
                                 </td>
                              </tr>


                                       <tr>
                                <th  >ID Card No.</th>
                                <td>
                                  <a href="#" class="xidentity_card_no" data-type="text" data-name="identity_card_no" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit ID Card No">
                                  <?php
if (!empty($user['identity_card_no'])) {
    echo $user['identity_card_no'];
} else {
    echo "NA";
}
?>
                                  </a>
                              </tr>






                              <tr>
                                <th  >Exam Center (First Choice)</th>
                                <td>
                                  <a href="#" class="xexam_center1" data-type="text" data-name="exam_center1" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit exam center1">
                                  <?php
if (!empty($user['exam_center1'])) {
    echo $user['exam_center1'];
} else {
    echo "NA";
}
?>
                                  </a>
                              </tr>




                              <tr>
                              <th  >Exam Center (Second Choice)</th>
                                <td>
                                  <a href="#" class="xexam_center2" data-type="text" data-name="exam_center2" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit exam center2">
                                  <?php
if (!empty($user['exam_center2'])) {
    echo $user['exam_center2'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                              <th  >Exam Center (Third Choice)</th>
                                <td>

                                  <a href="#" class="xexam_center3" data-type="text" data-name="exam_center3" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit exam center3">
                                  <?php
if (!empty($user['exam_center3'])) {
    echo $user['exam_center3'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft pass year</th>
                                <td>
                                  <a href="#" class="xtwelft_pass_year" data-type="text" data-name="twelft_pass_year" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft pass year">
                                  <?php
if (!empty($user['twelft_pass_year'])) {
    echo $user['twelft_pass_year'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft Stream</th>
                                <td>
                                  <a href="#" class="xtwelft_stream" data-type="select" data-name="twelft_stream" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft stream">
                                  <?php
if (!empty($user['twelft_stream'])) {
    echo $user['twelft_stream'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft other stream</th>
                                <td>
                                  <a href="#" class="xtwelft_other_stream" data-type="text" data-name="twelft_other_stream" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft other stream">
                                  <?php
if (!empty($user['twelft_other_stream'])) {
    echo $user['twelft_other_stream'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft board name</th>
                                <td>
                                  <a href="#" class="xtwelft_board_name" data-type="text" data-name="twelft_board_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft board name">
                                  <?php
if (!empty($user['twelft_board_name'])) {
    echo $user['twelft_board_name'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>

                                <tr>
                                <th>Twelft school name</th>
                                <td>
                                  <a href="#" class="xtwelft_school_name" data-type="text" data-name="twelft_school_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft school name">
                                  <?php
if (!empty($user['twelft_school_name'])) {
    echo $user['twelft_school_name'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                               <tr>
                                <th>Twelft school Address</th>
                                <td>
                                  <a href="#" class="xtwelft_school_address" data-type="text" data-name="twelft_school_address" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft school address">
                                  <?php
if (!empty($user['twelft_school_address'])) {
    echo $user['twelft_school_address'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Twelft Grade</th>
                                <td>
                                  <a href="#" class="xtwelft_grade" data-type="text" data-name="twelft_grade" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit twelft grade">
                                  <?php
if (!empty($user['twelft_grade'])) {
    echo $user['twelft_grade'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree pass year</th>
                                <td>
                                  <a href="#" class="xdegree_pass_year" data-type="text" data-name="degree_pass_year" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree pass year">
                                  <?php
if (!empty($user['degree_pass_year'])) {
    echo $user['degree_pass_year'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree stream</th>
                                <td>
                                  <a href="#" class="xdegree_stream" data-type="text" data-name="degree_stream" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree stream">
                                  <?php
if (!empty($user['degree_stream'])) {
    echo $user['degree_stream'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree other stream</th>
                                <td>
                                  <a href="#" class="xdegree_other_stream" data-type="text" data-name="degree_other_stream" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree other stream">
                                  <?php
if (!empty($user['degree_other_stream'])) {
    echo $user['degree_other_stream'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree  university name</th>
                                <td>
                                  <a href="#" class="xdegree_col_univ" data-type="text" data-name="degree_col_univ" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree university">
                                  <?php
if (!empty($user['degree_col_univ'])) {
    echo $user['degree_col_univ'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>

                              <tr>
                                <th>Degree college name</th>
                                <td>
                                  <a href="#" class="xdegree_col_name" data-type="text" data-name="degree_col_name" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree college">
                                  <?php
if (!empty($user['degree_col_name'])) {
    echo $user['degree_col_name'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree college address</th>
                                <td>
                                  <a href="#" class="xdegree_col_address" data-type="text" data-name="degree_col_address" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree college address">
                                  <?php
if (!empty($user['degree_col_address'])) {
    echo $user['degree_col_address'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Degree Grade</th>
                                <td>
                                  <a href="#" class="xdegree_grade" data-type="text" data-name="degree_grade" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit degree grade">
                                  <?php
if (!empty($user['degree_grade'])) {
    echo $user['degree_grade'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Language Hindi</th>
                                <td>
                                <?php
if (!empty($user['language_hindi'])) {
    echo $user['language_hindi'];
} else {
    echo "NA";
}
?>
                                  <a href="#" class="xlanguage_hindi" data-type="checklist" data-name="language_hindi"   data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Language Hindi"> edit </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Language English</th>
                                <td>
                                <?php
if (!empty($user['language_english'])) {
    echo $user['language_english'];
} else {
    echo "NA";
}
?>
                                  <a href="#" class="xlanguage_english" data-type="checklist" data-name="language_english" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit Language English"> edit </a>
                                </td>
                              </tr>
                              <tr>
                                <th>Language Other</th>
                                <td>
                                  <a href="#" class="xlanguage_other" data-type="text" data-name="language_other" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit language other">
                                  <?php
if (!empty($user['language_other'])) {
    echo $user['language_other'];
} else {
    echo "NA";
}
?>
                                   </a>
                                </td>
                              </tr>
                              <tr>
                                <th rowspan="3">Specialization Choice</th>
                                <td>
                                  <a href="#" class="xspecialization_choice1" data-type="text" data-name="specialization_choice1" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit specialization choice1">
                                  <?php
if (!empty($user['specialization_choice1'])) {
    echo $user['specialization_choice1'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <a href="#" class="xspecialization_choice2" data-type="text" data-name="specialization_choice2" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit specialization_choice2">
                                  <?php
if (!empty($user['specialization_choice2'])) {
    echo $user['specialization_choice2'];
} else {
    echo "NA";
}
?>
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <a href="#" class="xspecialization_choice3" data-type="text" data-name="specialization_choice3" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit specialization_choice3">
                                  <?php
if (!empty($user['specialization_choice3'])) {
    echo $user['specialization_choice3'];
} else {
    echo "NA";
}
?>
                                  </a>
                                  </td>
                              </tr>
                            <form action="" method="post" enctype="multipart/form-data">
                              <tr>
                                <th>Applicant Photo</th>
                                <td>
                                  <img src="../images/<?php echo $fetch['applicant_photo']; ?>" style="max-width:100px;">
                                  <input type="file" name="applicant_photo" required>
                                </td>
                              </tr>
                               <tr>
                                <td colspan="2">
                                  <button type="submit" name="submit" class="btn btn-primary" style="float: right;">Update File</button>
                                </td>
                              </tr>
                            </form>

                            <form action="" method="post" enctype="multipart/form-data">
                              <tr>
                                <th>Applicant Signature</th>
                                <td>
                                  <img src="../images/<?php echo $fetch['signatures']; ?>" style="max-width:100px;">
                                  <input type="file" name="signatures" required>
                                </td>
                              </tr>
                               <tr>
                                <td colspan="2">
                                  <button type="submit" name="submit" class="btn btn-primary" style="float: right;">Update File</button>
                                </td>
                              </tr>
                            </form>


                              <form action="" method="post" enctype="multipart/form-data">
                              <tr>
                                <th>ID Proof (Front)</th>
                                <td>
                                  <img src="../images/<?php echo $fetch['id_proof_front']; ?>" style="max-width:100px;">
                                  <input type="file" name="id_proof_front" required>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <button type="submit" name="submit" class="btn btn-primary" style="float: right;">Update File</button>
                                </td>
                              </tr>
                            </form>

                              <form action="" method="post" enctype="multipart/form-data">
                              <tr>
                                <th>ID Proof (Back)</th>
                                <td>
                                  <img src="../images/<?php echo $fetch['id_proof_back']; ?>" style="max-width:100px;">
                                  <input type="file" name="id_proof_back" required>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <button type="submit" name="submit" class="btn btn-primary" style="float: right;">Update File</button>
                                </td>
                              </tr>
                            </form>
                              <tr>
                                <th>Declaration</th>
                                <td>
                                  <?php
if ($user['declaration'] == 0) {
    $decl = 'No';
} else {
    $decl = 'Yes';
}
?>
                                  <a href="#" class="xdeclaration" data-type="select" data-name="declaration" data-pk="<?php echo $id; ?>" data-url="save_edit_user.php" data-title="Edit declaration"><?php echo $decl; ?></a>
                                 </td>
                              </tr>
                            </table>
                            </div>
                        </div>
                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <!-- <script src="js/vendor/bootstrap.min.js"></script> -->
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
    </body>
</html>

<script type="text/javascript">
    $( document ).ready(function() {


      <?php if (isset($paymentSuccess)) {?>
          $("#flash-msg").delay(5000).fadeOut("slow", function(){
              window.location.replace("http://www.partnerfortechnology.com/iicd/admin/view_userdetail.php?id=<?php echo $id; ?>");
          });
      <?php } else {?>
          $("#flash-msg-fail").delay(5000).fadeOut("slow", function(){
              window.location.replace("http://www.partnerfortechnology.com/iicd/admin/view_userdetail.php?id=<?php echo $id; ?>");
          });
      <?php }?>



      
        $('.detelebtn').click(function() {
            $('#modal-fade').fadeIn("fast");
            var id=$(this).attr('id');
            var username=$(this).attr('username');
            $('#modal-fade .showuser').text(username);
            $('#modal-fade .myprocesdelete').attr('href','deletevideo.php?id='+id+'&delete=1');
        });
    });
</script>

<script language="javascript" type="text/javascript">
        $(document).ready(function() {
          $('#country').change(function() {
            $("option:selected", $(this)).each(function () {
              var countryid = $(this).val();
              $.ajax({
                  type: "POST",
                  url: "showstate.php",
                  data: "countryid=" + countryid,
                  success: function (data) {
                          $('#divcountry').html(data);
                          $('#divcountry').slideDown('slow');
                          }
              });
          });
         });
      });
</script>

<script language="javascript" type="text/javascript">
      $(document).on('change', '#state', function() {
        //  $('#state').change(function() {
            $("option:selected", $(this)).each(function () {
              var stateid = $(this).val();
              $.ajax({
                  type: "POST",
                  url: "showcity.php",
                  data: "stateid=" + stateid,
                  success: function (data) {
                          $('#divstate').html(data);
                          $('#divstate').slideDown('slow');
                          }
              });
          });
         });

</script>

<script type="text/javascript">
    function UpdateRecord(id){
      jQuery.ajax({
       type: "POST",
       url: "update_user_status.php",
       data: 'id='+id,
       cache: false,
       success: function(response)
       {
         alert("Approved successfully");
         location.reload();
       }
     });
    }

</script>

<script type="text/javascript">
    function UpdatePay(id){
      var payment_status = $('#payment_status').val();
      var txnid = $('#txnid').val();
      jQuery.ajax({
       type: "POST",
       url: "update_pay_status.php",
       data:"id="+id+"&payment_status="+payment_status+"&txnid="+txnid,
       cache: false,
       success: function(response)
       {
         alert("Data saved successfully");
         location.reload();
       }
     });
    }

</script>

<script type="text/javascript">
    function SendEmail(id){
      //event.preventDefault();

        $("button[name='email_send']").text("wait...");
        $("button[name='email_send']").attr("disabled", "disabled");
      $.ajax({
       type: "POST",
       dataType:"json",
       url: "send_email.php",
       data:"id="+id,
       success: function(response)
       {
          alert('Email sent successfully');
              location.reload();
       }
     });
    }

</script>

<script type="text/javascript">
    function SendAdmit(id){
 //event.preventDefault();
        $("#sendadmit").val("sending...");
        $("#sendadmit").attr("disabled", "disabled");
      $.ajax({
       type: "POST",
       dataType:"json",
       url: "admit-cardnew.php",
       data:"id="+id,
       success: function(response)
       {
          alert('Admit card sent successfully');
              location.reload();
       }
     });
    }

</script>

<?php /*$DB -> close();*/?>