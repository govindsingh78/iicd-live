<?php
session_start();
include 'meekrodb.2.3.class.php';
if (!isset($_SESSION['user_id'])) {
    header('location: admission.php');
}
#########- Generate Enrollment ID Starts -#########
$userEnrolled = $_SESSION['user_id'];
generateEnrollmentId($userEnrolled);

#########- Function to Generate Enrollment ID Starts -#########
function generateEnrollmentId($userEnrolled)
{
    $sqlquery = "SELECT users.id AS UserID,users.status, user_details.Programme, user_details.exam_center1,exam_centers.city,exam_centers.center_code FROM users";
    $sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
    $sqlquery = $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city where users.id='" . $_SESSION['user_id'] . "'";
    $sqlquery = DB::queryFirstRow($sqlquery);
    $enrollmentExist = $sqlquery['enroll_id'];
    //if(empty($enrollmentExist)){
    ######- Comparing the Params Exist in Enrollment ID -#####
    $center_code = $sqlquery['center_code'];
    $programme = $sqlquery['Programme'];
    $id = $sqlquery['UserID'];
    $user_id = str_pad($id, 4, '0', STR_PAD_LEFT);
    $year = date("y");
    $likeEnroll = $programme . "" . $year . "" . $center_code;
    //$sql = "SELECT * FROM users where enroll_id like '%$likeEnroll%' and id!='$id'";
    //'be__in%'
    //$sql = "SELECT * FROM users where enroll_id like '$programme__$center_code%' and id!='$id'";

    
     $sql = "SELECT user_details.Programme, exam_centers.center_code  FROM user_details LEFT JOIN exam_centers ON  user_details.exam_center1 = exam_centers.city where user_details.Programme = '$programme' and exam_centers.center_code = '$center_code' and   user_details.user_id!='$id'";


    $sql = DB::query($sql);
    $countsql = DB::count($sql);
    $centerCodeVirtualId = $countsql + 1;
    $centerCodeVirtualId_Formatted = str_pad($centerCodeVirtualId, 3, '0', STR_PAD_LEFT);
    //$enrollmentPG = $programme.$year.$center_code.$user_id.'-'.$centerCodeVirtualId_Formatted;
    //New Enrollment ID format removing userid
    $enrollmentPG = $programme . $year . $center_code . '-' . $centerCodeVirtualId_Formatted;
    if ($enrollmentExist == "") {
        $update = DB::update('users', array('enroll_id' => $enrollmentPG), "id=%i", $id);
    }

}


$update_tracker['signup_tracking'] = 9;
$updateTracker = DB::update('user_details', $update_tracker, "user_id=%s", $_SESSION['user_id']);

?>


<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<style type="text/css">
  .title{
    text-align: center;
  }

table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    height: 50px;
}
h1 {
    text-align:center;
}
</style>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- Analytics Code -->
<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-75923092-1', 'auto');  ga('send', 'pageview'); </script>
  <!-- Facebook Pixel Code -->
  <script>
  ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
          n.callMethod ?
              n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
  }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1151196271591907');
  fbq('track', 'PageView');
  </script>
  <noscript>
      <img height="1" width="1" src="https://www.facebook.com/tr?id=1151196271591907&ev=PageView
&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->
<?php
//include 'meekrodb.2.3.class.php';
require_once "phpmailer/class.phpmailer.php";
$status = $_POST["status"];
$firstname = $_POST["firstname"];
$amount = $_POST["amount"];
//$amount=1;
$txnid = $_POST["txnid"];
$payuMoneyId = $_POST["payuMoneyId"];
$posted_hash = $_POST["hash"];
$key = $_POST["key"];
$productinfo = $_POST["productinfo"];
$email = $_POST["email"];
$salt = "DH32f67b";

if (isset($_POST["additionalCharges"])) {
    $additionalCharges = $_POST["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

} else {

    $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {
    $sqlfirst = "SELECT users.*  FROM users WHERE email = '" . $email . "'";
    $datafirst = DB::queryFirstRow($sqlfirst);

    if ($datafirst['email_sent'] != 1) {

        $update = DB::update('users', array('payment_status' => $status, 'txnid' => $_POST['payuMoneyId'], 'email_sent' => 1), "email=%s", $email);

        $sql = "SELECT users.*, user_details.* FROM users";
        $sql = $sql . " LEFT JOIN user_details ON user_details.user_id = users.id";
        $sql = $sql . " WHERE users.email = '" . $email . "'";

        $data = DB::queryFirstRow($sql);

        function countryNameById($countryid)
        {
            $sql = "SELECT country_name FROM country WHERE id = '" . $countryid . "'";
            $data = DB::queryFirstRow($sql);
            return $data['country_name'];
        }

        function cityNameById($cityid)
        {
            $sql = "SELECT city_name FROM city WHERE id = '" . $cityid . "'";
            $data = DB::queryFirstRow($sql);
            return $data['city_name'];
        }

        function stateNameById($stateid)
        {
            $sql = "SELECT state_name FROM state WHERE id = '" . $stateid . "'";
            $data = DB::queryFirstRow($sql);
            return $data['state_name'];
        }

        if ($data['Programme'] == 'PG') {
            $pname = "Master of Vocation in Crafts and Design";
        } elseif ($data['Programme'] == 'UG') {
            $pname = "4 Year Integrated Bachelors Programme (CFPD + B. VOC)";
        } else {
            $pname = "5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)";
        }
          
         if (!empty($data['religion'])) {
        $otherrel = $data['religion'];

        }

        if (!empty($data['other_religion'])) {
        $otherrel .= " - " . $data['other_religion'];
        }
        
        
        

        $message = "<body background='images/main-body-bg.png'>
            <div style='width:100%;' >
                <div style='width:600px; margin:0 auto; background-color:#fff; padding:15px; box-shadow: 0px 0px 10px 2px #ccc;'>

                        <h1 style='text-align: center;'><img src='images/company-logo1.png' alt='logo'></h1>

                          <h3 style='text-align: center;'>REMITTANCE PARTICULARS - Admission Processing Fee Details.</h3>

                            <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                              <tr>
                              <th style='border: 1px solid black;height: 50px'>Enrollment ID.</th>
                              <td style='border: 1px solid black'> " . $data['enroll_id'] . " </td>
                            </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Mode of payment</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>Online Mode</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Payment ID or Number</th>
                                  <td style='border: 1px solid black'> $payuMoneyId </td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Date of Transaction</th>
                                  <td style='border: 1px solid black'>" . date('d/m/Y') . "</td>
                                </tr>
                              </tbody>
                            </table>

                              <h3 style='text-align: center;'>PERSONAL DETAILS</h3>

                          <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                                    <tbody>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Name of Applicant</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Date Of Birth</th>
                                        <td style='border: 1px solid black'> " . date('d/m/y', strtotime($data['dob'])) . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Gender</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['gender'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Marital Status</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['marital_status'] . "</td>
                                      </tr>





                                   <tr>
                                   <th style='border: 1px solid black;height: 50px'>Alternate Email</th>
                                   <td style='border: 1px solid black;text-transform:uppercase'> " . $data['alternate_email'] . "</td>
                                 </tr>
                                 <tr>
                                 <th style='border: 1px solid black;height: 50px'>Religion</th>
                                 <td style='border: 1px solid black;text-transform:uppercase'> " . $otherrel . "</td>
                               </tr>
                                
                             <tr>
                             <th style='border: 1px solid black;height: 50px'>Minority</th>
                             <td style='border: 1px solid black;text-transform:uppercase'> " . $data['minority'] . "</td>
                           </tr>
                           <tr>
                           <th style='border: 1px solid black;height: 50px'>Is Student Belongs to APL/BPL?</th>
                           <td style='border: 1px solid black;text-transform:uppercase'> " . $data['bpl_apl'] . "</td>
                         </tr>
                         <tr>
                         <th style='border: 1px solid black;height: 50px'>Emergency Phone No.</th>
                         <td style='border: 1px solid black;text-transform:uppercase'> " . $data['emergency_phone_no'] . "</td>
                       </tr>
                       <tr>
                       <th style='border: 1px solid black;height: 50px'>Caste Category</th>
                       <td style='border: 1px solid black;text-transform:uppercase'> " . $data['caste_category'] . "</td>
                     </tr>
                     <tr>
                     <th style='border: 1px solid black;height: 50px'>Other Category</th>
                     <td style='border: 1px solid black;text-transform:uppercase'> " . $data['other_category'] . "</td>
                   </tr>
                   <tr>
                   <th style='border: 1px solid black;height: 50px'>Students Area</th>
                   <td style='border: 1px solid black;text-transform:uppercase'> " . $data['students_area'] . "</td>
                 </tr>



                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Nationality</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['nationality'] . "</td>
                                      </tr>

                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Candidate)</th>
                                        <td style='border: 1px solid black'> " . $data['phone'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Candidate)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Father`s Name</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'>" . $data['fathers_name'] . "</td>
                                      </tr>

                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Father)</th>
                                        <td style='border: 1px solid black'> " . $data['phone_father'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Father)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email_father'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mother`s Name</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['mothers_name'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Mother)</th>
                                        <td style='border: 1px solid black'> " . $data['phone_mother'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Mother)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email_mother'] . "</td>
                                      </tr>
                                       <tr>
                                        <th style='border: 1px solid black;height: 50px'>Local Guardians Name</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['guardians_name'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Local Guardian)</th>
                                        <td style='border: 1px solid black'> " . $data['phone_guardian'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Local Guardian)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email_guardian'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Relation with Local Guardians </th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['guardians_relation'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Family Income Per Annum (Rs)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['family_income'] . "</td>
                                      </tr>";

        if (!empty($data['craft_relation']) && $data['craft_relation'] == 'yes') {
            $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Craft Name </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['craft_name'] . "</td>
                                </tr>";
        }

        $message .= "
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Medical/Health Information</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'>" . $data['medical_info'] . "</td>
                                      </tr>
                                    </tbody>
                          </table>

                              <h3 style='text-align: center;'>PERMANENT ADDRESS</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                              <tr>
                              <th style='border: 1px solid black;height: 50px'>House No./Flat no.</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . $data['address_line1'] . "</td>
                            </tr>

                            <tr>
                            <th style='border: 1px solid black;height: 50px'>Village/Block/Tehsil Name</th>
                            <td style='border: 1px solid black;text-transform:uppercase'> " . $data['block_or_tehsil'] . "</td>
                          </tr>
                            <tr>
                              <th style='border: 1px solid black;height: 50px'>Pincode</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . $data['pin_code'] . "</td>
                            </tr>
                             <tr>
                              <th style='border: 1px solid black;height: 50px'>City</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . cityNameById($data['city']) . "</td>
                            </tr>
                            <tr>
                              <th style='border: 1px solid black;height: 50px'>State</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . stateNameById($data['state']) . "</td>
                            </tr>
                            </tbody>
                            </table>

                             <h3 style='text-align: center;'>Identity Detail</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                            <tr>
                            <th style='border: 1px solid black;height: 50px'>ID Card Type</th>
                            <td style='border: 1px solid black;text-transform:uppercase'>" . $data['identity_card'] . "</td>
                          </tr>
                          <tr>
                          <th style='border: 1px solid black;height: 50px'>ID Card No.</th>
                          <td style='border: 1px solid black;text-transform:uppercase'>" . $data['identity_card_no'] . "</td>
                        </tr>
                           

                              </tbody>
                            </table>

                            <h3 style='text-align: center;'>CORRESPONDENCE ADDRESS</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                              <tr>
                              <th style='border: 1px solid black;height: 50px'>House No./Flat no.</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . $data['caddress_line1'] . "</td>
                            </tr>

                          <tr>
                          <th style='border: 1px solid black;height: 50px'>Village/Block/Tehsil Name</th>
                          <td style='border: 1px solid black;text-transform:uppercase'> " . $data['cblock_or_tehsil'] . "</td>
                        </tr>
                            <tr>
                              <th style='border: 1px solid black;height: 50px'>Pincode</th>
                              <td style='border: 1px solid black'>" . $data['cpin_code'] . "</td>
                            </tr>
                            <tr>
                              <th style='border: 1px solid black;height: 50px'>City</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . cityNameById($data['ccity']) . "</td>
                            </tr>
                            <tr>
                              <th style='border: 1px solid black;height: 50px'>State</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . stateNameById($data['state']) . "</td>
                            </tr>

                              </tbody>
                            </table>

                              <h3 style='text-align: center;'>PREFERENCES FOR ENTRANCE TEST CENTER</h3>
                              <h4>IICD reserves the rights to cancel or change the Examination Center/Date</h4>
                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Ist Option</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['exam_center1'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IInd Option</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['exam_center2'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IIIrd Option</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['exam_center3'] . "</td>
                                </tr>
                              </tbody>
                            </table>

                              <h3 style='text-align: center;'>10+2 OR EQUIVALENT QUALIFICATIONS</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Year of Passing</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_pass_year'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Stream of Studies at 10+2 or equivalent</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['twelft_stream'] . "</td>
                                </tr>";

        if (!empty($data['twelft_other_stream'])) {
            $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Incase of other stream, pl. specify </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_other_stream'] . "</td>
                                </tr>";
        }

        $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the Board/School  </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_board_name'] . "</td>
                                </tr>

                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the School </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_school_name'] . "</td>
                                </tr>

                                 <tr>
                                  <th style='border: 1px solid black;height: 50px'>Address of School </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_school_address'] . "</td>
                                </tr>
                                  <th style='border: 1px solid black;height: 50px'>Grade/Class/Division/Appeared </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['twelft_grade'] . "</td>
                                </tr>
                              </tbody>
                            </table>";
        if (!empty($data['Programme']) && $data['Programme'] == 'PG') {

            $message .= "<h3 style='text-align: center;'>DEGREE</h3>


                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Year of Passing</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_pass_year'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Stream of Studies at Bachelors degree</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_stream'] . "</td>
                                </tr>";

            if (!empty($data['degree_other_stream'])) {
                $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Incase of other stream, pl. specify </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['degree_other_stream'] . "</td>
                                </tr>";
            }
            $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the Board/University </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_col_univ'] . "</td>
                                </tr>

                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the College </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_col_name'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Address of the College </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_col_address'] . "</td>
                                </tr>
                                  <th style='border: 1px solid black;height: 50px'>Grade/Class/Division/Appeared </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_grade'] . "</td>
                                </tr>
                              </tbody>
                            </table>";
        }

        $message .= "<h3 style='text-align: center;'>LANGUAGE KNOWN</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Hindi</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['language_hindi'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>English</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['language_english'] . "</td>
                                </tr>
                                ";

        if (!empty($data['language_other'])) {
            $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Other Language </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['language_other'] . "</td>
                                </tr>";
        }
        $message .= " </tbody>
                            </table>
                              <h3 style='text-align: center;'>PREFERENCES FOR SPECIALIZATION</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Ist Choice</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['specialization_choice1'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IInd Choice</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['specialization_choice2'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IIIrd Choice</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['specialization_choice3'] . "</td>
                                </tr>
                              </tbody>
                            </table>
                    </div>
                  </div>
                </body>";

        $subjectLine = DB::queryFirstRow("SELECT * FROM settings WHERE id = 16");
        //$bodyContent = DB::queryFirstRow("SELECT * FROM settings WHERE id = 17");

        $lineofSubject = strip_tags($subjectLine['value']);

        $ap = 'images/' . $data['applicant_photo'];
        $id_proof_front = 'images/' . $data['id_proof_front'];
        $id_proof_back = 'images/' . $data['id_proof_back'];
        $sig = 'images/' . $data['signatures'];

        // php mailer code starts
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // telling the class to use SMTP

        $mail->SMTPDebug = 0; // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->SMTPSecure = "ssl"; // sets the prefix to the servier
        $mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
        $mail->Port = 465; // set the SMTP port for the GMAIL server

        $mail->Username = 'admissions@iicd.ac.in';
        $mail->Password = 'admission915';

        $mail->SetFrom('admissions@iicd.ac.in', 'IICD');
        $mail->AddAddress($email);
        $mail->AddCC('admissions@iicd.ac.in');
        $mail->AddAttachment($ap);
        $mail->AddAttachment($sig);
        $mail->AddAttachment($id_proof_front);
        $mail->AddAttachment($id_proof_back);

        $mail->Subject = trim("$lineofSubject $pname");
        $mail->MsgHTML($message);

        try {
            $mail->send();

        } catch (Exception $ex) {
            $msg = $ex->getMessage();

        }
    }

    ?>
            <!DOCTYPE html>
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>IICD | Register Form</title>
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
            <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="css/font-awesome.css">

            <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
            <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>

            <body>
                <div class="admission-form">
                  <div class="container">
                     <div class="form-pay">
                          <div class="thanks-sec">
                              <div class="col-md-12">
                                 <h1><img src="images/company-logo1.png" alt="logo"></h1>
                                   <h2 class="thanx-h2">Thank You !</h2>
                                   <div class="thanks-body">
                                      <p>
                                        Congratulations!!! You have successfully submitted your form, you will get a separate email on candidate`s email id about the confirmation of acceptance within next 02 working days. In case you do not receive the confirmation email, please feel free to contact us: email: admissions@iicd.ac.in; WhatsApp: 9460673297. <br>
                                        Your transaction status is <?php echo $status; ?>
                                        Your Transaction ID for this transaction is <?php echo $txnid; ?> .
                                        </p>
                                        <span class="thnaks-subtxt">We have received a payment of Rs. <?php echo $amount; ?> </span>
                                        <div class="thanx-bk-btn ">
                                        </div>
                                   </div>
                                </div>
                            </div>
                         </div>
                  </div>
                </div>
            <script type="text/javascript" src="js/bootstrap.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/selecters.js"></script>
            </body>
            </html>
      <?php
}
?>