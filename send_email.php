<?php
require_once '../meekrodb.2.3.class.php';
require_once 'phpmailer/class.phpmailer.php';
$userid = $_POST['id'];

$sql = "SELECT users.*, user_details.* FROM users";
$sql = $sql . " LEFT JOIN user_details ON user_details.user_id = users.id";
$sql = $sql . " WHERE users.id = '" . $userid . "'";
$data = DB::queryFirstRow($sql);

function countryNameById($countryid)
{
    $sql = "SELECT country_name FROM country WHERE id = '" . $countryid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['country_name'];
}

function cityNameById($cityid)
{
    $sql = "SELECT city_name FROM city WHERE id = '" . $cityid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['city_name'];
}

function stateNameById($stateid)
{
    $sql = "SELECT state_name FROM state WHERE id = '" . $stateid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['state_name'];
}

if ($data['Programme'] == 'PG') {
    $pname = "Master of Vocation in Crafts and Design";
} elseif ($data['Programme'] == 'UG') {
    $pname = "4 Year Integrated Bachelors Programme (CFPD + B. VOC)";
} else {
    $pname = "5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)";
}

       if (!empty($data['religion'])) {
        $otherrel = $data['religion'];

        }

        if (!empty($data['other_religion'])) {
        $otherrel .= " - " . $data['other_religion'];
        }

$message = "<body background='images/main-body-bg.png'>
            <div style='width:100%;' >
                <div style='width:600px; margin:0 auto; background-color:#fff; padding:15px; box-shadow: 0px 0px 10px 2px #ccc;'>

                        <h1 style='text-align: center;'><img src='images/company-logo1.png' alt='logo'></h1>

                          <h3 style='text-align: center;'>REMITTANCE PARTICULARS - Admission Processing Fee Details.</h3>

                            <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                              <tr>
                              <th style='border: 1px solid black;height: 50px'>Enrollment ID.</th>
                              <td style='border: 1px solid black'> " . $data['enroll_id'] . " </td>
                            </tr>
                                <tr>

                                  <th style='border: 1px solid black;height: 50px'>Mode of payment</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>Online Mode</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Payment ID or Number</th>
                                  <td style='border: 1px solid black'> " . $data['txnid'] . " </td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Date of Transaction</th>
                                  <td style='border: 1px solid black'>" . $data['updated_at'] . "</td>
                                </tr>

                              </tbody>
                            </table>

                              <h3 style='text-align: center;'>PERSONAL DETAILS</h3>

                          <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                                    <tbody>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Name of Applicant</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Date Of Birth</th>
                                        <td style='border: 1px solid black'> " . date('d/m/y', strtotime($data['dob'])) . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Gender</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['gender'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Marital Status</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['marital_status'] . "</td>
                                      </tr>


                                   <tr>
                                   <th style='border: 1px solid black;height: 50px'>Alternate Email</th>
                                   <td style='border: 1px solid black;text-transform:uppercase'> " . $data['alternate_email'] . "</td>
                                 </tr>
                                 <tr>
                                 <th style='border: 1px solid black;height: 50px'>Religion</th>
                                 <td style='border: 1px solid black;text-transform:uppercase'> " . $otherrel . "</td>
                               </tr>
                                
                             <tr>
                             <th style='border: 1px solid black;height: 50px'>Minority</th>
                             <td style='border: 1px solid black;text-transform:uppercase'> " . $data['minority'] . "</td>
                           </tr>
                           <tr>
                           <th style='border: 1px solid black;height: 50px'>Is Student Belongs to APL/BPL?</th>
                           <td style='border: 1px solid black;text-transform:uppercase'> " . $data['bpl_apl'] . "</td>
                         </tr>
                         <tr>
                         <th style='border: 1px solid black;height: 50px'>Emergency Phone No.</th>
                         <td style='border: 1px solid black;text-transform:uppercase'> " . $data['emergency_phone_no'] . "</td>
                       </tr>
                       <tr>
                       <th style='border: 1px solid black;height: 50px'>Caste Category</th>
                       <td style='border: 1px solid black;text-transform:uppercase'> " . $data['caste_category'] . "</td>
                     </tr>
                     <tr>
                     <th style='border: 1px solid black;height: 50px'>Other Category</th>
                     <td style='border: 1px solid black;text-transform:uppercase'> " . $data['other_category'] . "</td>
                   </tr>
                   <tr>
                   <th style='border: 1px solid black;height: 50px'>Students Area</th>
                   <td style='border: 1px solid black;text-transform:uppercase'> " . $data['students_area'] . "</td>
                 </tr>


                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Nationality</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['nationality'] . "</td>
                                      </tr>

                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Candidate)</th>
                                        <td style='border: 1px solid black'> " . $data['phone'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Candidate)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Father`s Name</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'>" . $data['fathers_name'] . "</td>
                                      </tr>

                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Father)</th>
                                        <td style='border: 1px solid black'> " . $data['phone_father'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Father)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email_father'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mother`s Name</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['mothers_name'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Mother)</th>
                                        <td style='border: 1px solid black'> " . $data['phone_mother'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Mother)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email_mother'] . "</td>
                                      </tr>
                                       <tr>
                                        <th style='border: 1px solid black;height: 50px'>Local Guardians Name</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['guardians_name'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Mobile Number (Local Guardian)</th>
                                        <td style='border: 1px solid black'> " . $data['phone_guardian'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Email id (Local Guardian)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['email_guardian'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Relation with Local Guardians </th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['guardians_relation'] . "</td>
                                      </tr>
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Family Income Per Annum (Rs)</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'> " . $data['family_income'] . "</td>
                                      </tr>";

if (!empty($data['craft_relation']) && $data['craft_relation'] == 'yes') {
    $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Craft Name </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['craft_name'] . "</td>
                                </tr>";
}

$message .= "
                                      <tr>
                                        <th style='border: 1px solid black;height: 50px'>Medical/Health Information</th>
                                        <td style='border: 1px solid black;text-transform:uppercase'>" . $data['medical_info'] . "</td>
                                      </tr>
                                    </tbody>
                          </table>

                              <h3 style='text-align: center;'>PERMANENT ADDRESS</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                <th style='border: 1px solid black;height: 50px'>House No./Flat no.</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['address_line1'] . "</td>
                                </tr>

                                <tr>
                                <th style='border: 1px solid black;height: 50px'>Village/Block/Tehsil Name</th>
                                <td style='border: 1px solid black;text-transform:uppercase'> " . $data['block_or_tehsil'] . "</td>
                              </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Pincode</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['pin_code'] . "</td>
                                </tr>
                                    <tr>
                                  <th style='border: 1px solid black;height: 50px'>City</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . cityNameById($data['city']) . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>State</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . stateNameById($data['state']) . "</td>
                                </tr>
                                 </tbody>
                            </table>

                             <h3 style='text-align: center;'>Identity Detail</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                <th style='border: 1px solid black;height: 50px'>ID Card Type</th>
                                <td style='border: 1px solid black;text-transform:uppercase'>" . $data['identity_card'] . "</td>
                              </tr>
                              <tr>
                              <th style='border: 1px solid black;height: 50px'>ID Card No.</th>
                              <td style='border: 1px solid black;text-transform:uppercase'>" . $data['identity_card_no'] . "</td>
                            </tr>
                            

                              </tbody>
                            </table>

                            <h3 style='text-align: center;'>CORRESPONDENCE ADDRESS</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                <th style='border: 1px solid black;height: 50px'>House No./Flat no.</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['caddress_line1'] . "</td>
                                </tr>

                              <tr>
                              <th style='border: 1px solid black;height: 50px'>Village/Block/Tehsil Name</th>
                              <td style='border: 1px solid black;text-transform:uppercase'> " . $data['cblock_or_tehsil'] . "</td>
                            </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Pincode</th>
                                  <td style='border: 1px solid black'>" . $data['cpin_code'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>City</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . cityNameById($data['ccity']) . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>State</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . stateNameById($data['state']) . "</td>
                                </tr>

                              </tbody>
                            </table>

                              <h3 style='text-align: center;'>PREFERENCES FOR ENTRANCE TEST CENTER</h3>
                              <h4>IICD reserves the rights to cancel or change the Examination Center/Date</h4>
                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Ist Option</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['exam_center1'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IInd Option</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['exam_center2'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IIIrd Option</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['exam_center3'] . "</td>
                                </tr>
                              </tbody>
                            </table>

                              <h3 style='text-align: center;'>10+2 OR EQUIVALENT QUALIFICATIONS</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Year of Passing</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_pass_year'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Stream of Studies at 10+2 or equivalent</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['twelft_stream'] . "</td>
                                </tr>";

if (!empty($data['twelft_other_stream'])) {
    $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Incase of other stream, pl. specify </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_other_stream'] . "</td>
                                </tr>";
}

$message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the Board/School  </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_board_name'] . "</td>
                                </tr>

                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the School </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_school_name'] . "</td>
                                </tr>

                                 <tr>
                                  <th style='border: 1px solid black;height: 50px'>Address of School </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['twelft_school_address'] . "</td>
                                </tr>
                                  <th style='border: 1px solid black;height: 50px'>Grade/Class/Division/Appeared </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['twelft_grade'] . "</td>
                                </tr>
                              </tbody>
                            </table>";
if (!empty($data['Programme']) && $data['Programme'] == 'PG') {

    $message .= "<h3 style='text-align: center;'>DEGREE</h3>


                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Year of Passing</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_pass_year'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Stream of Studies at Bachelors degree</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_stream'] . "</td>
                                </tr>";

    if (!empty($data['degree_other_stream'])) {
        $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Incase of other stream, pl. specify </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['degree_other_stream'] . "</td>
                                </tr>";
    }
    $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the Board/University </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_col_univ'] . "</td>
                                </tr>

                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Name of the College </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_col_name'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Address of the College </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_col_address'] . "</td>
                                </tr>
                                  <th style='border: 1px solid black;height: 50px'>Grade/Class/Division/Appeared </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['degree_grade'] . "</td>
                                </tr>
                              </tbody>
                            </table>";
}

$message .= "<h3 style='text-align: center;'>LANGUAGE KNOWN</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Hindi</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'>" . $data['language_hindi'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>English</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['language_english'] . "</td>
                                </tr>
                                ";

if (!empty($data['language_other'])) {
    $message .= "<tr>
                                  <th style='border: 1px solid black;height: 50px'>Other Language </th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['language_other'] . "</td>
                                </tr>";
}
$message .= " </tbody>
                            </table>
                              <h3 style='text-align: center;'>PREFERENCES FOR SPECIALIZATION</h3>

                           <table class='table' style='border-collapse: collapse; border-collapse: collapse; width: 100%;'>
                              <tbody>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>Ist Choice</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['specialization_choice1'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IInd Choice</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['specialization_choice2'] . "</td>
                                </tr>
                                <tr>
                                  <th style='border: 1px solid black;height: 50px'>IIIrd Choice</th>
                                  <td style='border: 1px solid black;text-transform:uppercase'> " . $data['specialization_choice3'] . "</td>
                                </tr>
                              </tbody>
                            </table>
                    </div>
                  </div>
                </body>";

$subjectLine = DB::queryFirstRow("SELECT * FROM settings WHERE id = 16");
//$bodyContent = DB::queryFirstRow("SELECT * FROM settings WHERE id = 17");

$lineofSubject = strip_tags($subjectLine['value']);

$ap = '../images/' . $data['applicant_photo'];
$id_proof_front = '../images/' . $data['id_proof_front'];
$id_proof_back = '../images/' . $data['id_proof_back'];
$sig = '../images/' . $data['signatures'];

// php mailer code starts
$mail = new PHPMailer(true);
$mail->IsSMTP(); // telling the class to use SMTP

$mail->SMTPDebug = 0; // enables SMTP debug information (for testing)
$mail->SMTPAuth = true; // enable SMTP authentication
$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
$mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
$mail->Port = 465; // set the SMTP port for the GMAIL server

$mail->Username = 'admissions@iicd.ac.in';
$mail->Password = 'admission915';

$mail->SetFrom('admissions@iicd.ac.in', 'IICD');
$mail->AddAddress($data['email']); //$data['email']
$mail->AddCC('admissions@iicd.ac.in');
if (!empty($data['applicant_photo'])) {
    $mail->AddAttachment($ap);
}
if (!empty($data['signatures'])) {
    $mail->AddAttachment($sig);
}
if (!empty($data['id_proof_front'])) {
    $mail->AddAttachment($id_proof_front);
}
if (!empty($data['id_proof_back'])) {
    $mail->AddAttachment($id_proof_back);
}

$mail->Subject = trim("$lineofSubject $pname");
$mail->MsgHTML($message);
try {
    $mail->send();
    $update = DB::update('users', array('email_sent' => 1), "id=%s", $userid);
    $result['success'] = 'success';
    $result['message'] = 'Email sent successfully';
    echo json_encode($result);

} catch (Exception $ex) {
    $msg = $ex->getMessage();

}
