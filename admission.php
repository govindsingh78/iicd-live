<?php
session_start();
require_once 'meekrodb.2.3.class.php';
?>

<!DOCTYPE html>
<html>
<head>
<link rel="apple-touch-icon" sizes="57x57" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://www.iicd.ac.in/wp-content/themes/reverie-child-master/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:title" content="IICD Admission Open 2019-20" />
<meta property="og:type" content="IICD Admission" />
<!-- <meta property="og:url" content="https://www.iicd.ac.in" /> -->
<meta property="og:image" content="https://www.iicd.ac.in/wp-content/uploads/2018/11/admission-open-at-iicd-2019-20.jpg" />
<meta property="og:site_name" content="Admission for UG and PG degree programmes in Crafts &amp; Design 2019-20 " />

<title>IICD</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Analytics Code -->
<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-75923092-1', 'auto');  ga('send', 'pageview'); </script>
  <!-- Facebook Pixel Code -->
  <script>
  ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
          n.callMethod ?
              n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
  }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1151196271591907');
  fbq('track', 'PageView');
  </script>
  <noscript>
      <img height="1" width="1" src="https://www.facebook.com/tr?id=1151196271591907&ev=PageView
&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->
</head>
<body>
    <div class="admission-form">
        <div class="mrg-btm">
          <div class="col-md-12 text-center">
           <div class="company-logo">
                  <a href="#">
                      <img src="images/company-logo1.png" alt="">
                  </a>
              </div>

                 <!-- Code to make title dynamic which can be change from admin panel setting tab -->
                 <?php
$headline1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 12");
$headline2 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 13");
$headline3 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 14");
$headline4 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 15");
?>
                      <h2> <?php echo strip_tags($headline1['value']); ?> </h2>
                      <div class="form-sub-title">
                      <span> <?php echo strip_tags($headline2['value']); ?>  </span><br>
                      <span> <?php echo strip_tags($headline3['value']); ?>  </span><br>
                      <span> <?php echo strip_tags($headline4['value']); ?>  </span>
                      </div>
                      <!-- Code to make title dynamic which can be change from admin panel setting tab -->


          </div>
        </div>

        <div class="col-sm-8 col-sm-offset-2 flash-message"></div>

        <div class="container">
            <div class="col-md-6">
              <div class="register-form-container">
                    <div class="firstid" id="fomr-2">
                      <div class="form-section">
                          <h3 class="text-left"> Registration For Entrance
                            <img class="pull-right" src="images/register-icn.png" alt=""> </h3>
                            <div class="col-md-12"> <span class="sub-heading"> Please fill the details below:</span>
                              <form id="RegistrationForm" action="" method="POST" class="form-horizontal">
                                <div class="col-md-12">
                                    <div class="my-input-bx">
                                      <input type="text" class="form-control" id="first_name" name="first_name" required title="First name is required" placeholder="First Name*">
                                       <span class="bar"></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                   <div class="my-input-bx">
                                      <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" >
                                       <span class="bar"></span>
                                   </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="my-input-bx">
                                      <input type="text" class="form-control" id="last_name" name="last_name"   placeholder="Last Name">
                                       <span class="bar"></span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="my-input-bx">
                                      <input type="text" class="form-control" id="email" name="email" required
                                      title="Email is required" placeholder="Email*">
                                      <span class="bar"></span>
                                    </div>
                                </div>

                                 <div class="col-md-12">
                                   <div class="my-input-bx ">
                                      <input type="password" class="form-control" id="password" name="password" required title="Password is required" placeholder="Password*">
                                      <span class="bar"></span>
                                   </div>
                                </div>

                               <!--   <div class="col-md-12">
                                   <div class="my-input-bx ">
                                      <div class="g-recaptcha" data-sitekey="6Lfp-D4UAAAAAG1ISkJTH7oJEeMAq4ZPXNujeG5y"></div>
                                      <div id="recaptcha-error" class="error-show" style="display: block;"></div>
                                   </div>
                                </div> -->

                                   <div class="col-md-12">
                                   <div class="my-input-bx ">
                                       <img src="captcha.php?rand=<?php echo rand(); ?>" id='captchaimg'><br>

                                          Can't read the image? click <a  href='javascript: refreshCaptcha();'>here</a> to refresh.
                                   </div>
                                </div>

                                <div class="col-md-12">
                                   <div class="my-input-bx ">
                                         <input id="captcha_code" name="captcha_code" type="text" required placeholder="Enter the code above here*">
                                           <span class="bar"></span>
                                           <div id="recaptcha-error" class="error-show" style="display: block;"></div>
                                   </div>
                                </div>

                                <div class="col-md-6 col-md-offset-3">
                                    <button id="btn_submit" type="submit" name="reg_submit" class="ripple" style="margin-bottom:30px;">Submit</button>
                                </div>
                              </form>

                              <div class="container">
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
                                  <div class="modal-dialog thanks-bnr-mdl">
                                    <!-- Modal content-->
                                    <div class="modal-content thank-popup">
                                      <div class="modal-body">
                                        <div class="thanks-bnr">
                                            <h3 class="mdl-thanks-h3"> Thank You ! </h3>
                                            <div class="col-md-12">
                                              <div class="messg-icon">
                                                <img src="images/chat.png" alt="">
                                              </div>
                                            </div>
                                            <div class="col-md-12">
                                              <div class="verfy-nbr">
                                                <h3>Verify Your Email</h3>
                                                <p>OTP has been sent to  your  Email Address. <br>Please enter it below and confirm.</p>
<p>Please also check  spam folder.</p>
                                                <form id="OtpForm" action="" method="POST" class="form-horizontal">
                                                  <div class="form-group">
                                                    <input type="text" id="otp_input" name="otp" class="form-control" placeholder="Enter OTP*">
                                                    <div id="otp-error" class="error-show"></div>
                                                  </div>
                                                </form>
                                              </div>
                                            </div>
                                              <div class="clearfix"></div>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <div class="col-md-12">
                                        <button type="button" id="btn_confirm" name="otp_cnfrm" class="ripple">confirm</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                      </div>
                    </div>
              </div>
            </div>

            <div class="col-md-6">
                <div class="register-form-container">
                    <div class="firstid" id="fomr-2">
                        <div class="form-section">
                            <h3 class="text-left"> Login For Entrance
                            <img class="pull-right" src="images/login.png" alt=""> </h3>
                            <div class="col-md-12"> <span class="sub-heading"> Please fill the details below:</span>
                              <form id="LoginForm" action="" method="POST" class="form-horizontal">
                                <div class="col-md-12">
                                   <div class="my-input-bx">
                                      <input type="text" name="email" class="form-control" required title="Enter Your Email Id" placeholder="Email Id*">
                                      <span class="bar"></span>
                                   </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="my-input-bx ">
                                      <input type="password" name="password" class="form-control" required title="Enter Your Confirm Password " placeholder="Password*">
                                      <span class="bar"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3">
                                <button id="btn_submit_login" type="submit" name="submit_login" class="ripple login-btn" style="margin-bottom:30px;">Log In</button>
                                </div>
                                <div class="forget-pas">
                                  <a href="forgot_password.php">Forgot Password ?</a>
                                </div>
                              </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/jquery.validate.min.js"></script>

</body>
</html>

<script>
  $(document).ready(function() {
    $("#flash-msg").delay(5000).fadeOut("slow");

    var user_otp = '';
    $("#btn_submit").click(function(event) {
      if(!$('#RegistrationForm').valid()){
          return false;
      }
      else{
        event.preventDefault();
        $("button[name='reg_submit']").text("wait...");
        $("button[name='reg_submit']").attr("disabled", "disabled");
        $.ajax({
            url: "regProccess.php",
            type: "post",
            data: $('#RegistrationForm').serialize(),
            success: function(res) {
              res = JSON.parse(res);
              $("button[name='reg_submit']").text("Submit");
              $("button[name='reg_submit']").removeAttr("disabled");

              // console.log(res);
              if(res['success']){
                if(res['data']['otp'] && res['data']['uemail']){
                  $("#myModal").modal("show");
                  user_otp = res['data']['otp'];
                  console.log(user_otp);
                }
              }
              else if (res['email_err']) {
                $("#email-error").remove();
                $("#emailError").remove();
                $("#email").after('<div id="emailError" class="error-show">'+res['email_err']['message']+'</div>');
              }
              else if (res['recaptcha']){
                $('#recaptcha-error').css("display","block");
                $('#recaptcha-error').text(res['recaptcha']['message']);
              }
              else {
                $('.flash-message').html('<div class="alert alert-danger" id="flash-msg">'+res['message']+'</div>');
                $("#flash-msg").delay(5000).fadeOut("slow");
              }
            },
            error: function (request, error) {
              $("button[name='reg_submit']").text("Submit");
              $("button[name='reg_submit']").removeAttr("disabled");
              // alert(" Can't do because: " + error);
              $('.flash-message').html('<div class="alert alert-danger" id="flash-msg">'+error+'</div>');
              $("#flash-msg").delay(5000).fadeOut("slow");
            }
        });
        return true;
      }
    });

    $('#RegistrationForm').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "first_name": {
            required: true,
            customvalidation: true

          },

          "email": {
            required: true,
            email :true
          },
          "password": {
             required: true,
             minlength: 5
          },
          "captcha_code": {
             required: true
          }
        },
        messages:
        {
         "first_name": {
            required: "First Name is required",
            customvalidation: "Sorry, no special characters and number allowed"
          },

          "email": {
             required: "Email is required",
             email: "Please enter a valid email address"
          },
          "password": {
             required: "Password is required",
             minlength: "Your password must be at least 5 characters long"
          },
          "captcha_code": {
             required: "Captcha is required"
          }
        }
    });
     $.validator.addMethod("customvalidation",
           function(value, element) {
                   return /^[a-zA-Z]+$/.test(value);
           },
        "Sorry, no special characters and number allowed"
   );

    $("#btn_submit_login").click(function(event) {
      if(!$('#LoginForm').valid()){
          return false;
      }
      else{
        event.preventDefault();
        $("button[name='submit_login']").text("wait...");
        $("button[name='submit_login']").attr("disabled", "disabled");
        $.ajax({
            url: "loginProccess.php",
            type: "post",
            data: $('#LoginForm').serialize(),
            success: function(res) {
              res = JSON.parse(res);
              if(!res['error']){
                $("button[name='submit_login']").text("Log In");
                $("button[name='submit_login']").removeAttr("disabled");
                if (res.payment_status == "success") {
                   window.location.href = 'thankyou.php';
                }
                else{
                  /*Analytics Login track*/
                  ga('send', 'event', 'form', 'submit', 'login');
                  /*/Analytics Login track*/
                  window.location.href = 'admission-form.php';
                }
                // window.location.href = 'valid.php';
              }
              else{
                $("button[name='submit_login']").text("Log In");
                $("button[name='submit_login']").removeAttr("disabled");
                $('.flash-message').html('<div class="alert alert-danger" id="flash-msg">'+res['message']+'</div>');
                $("#flash-msg").delay(5000).fadeOut("slow");
              }
            },
            error: function (request, error) {
              $("button[name='submit_login']").text("Log In");
              $("button[name='submit_login']").removeAttr("disabled");
              alert(" Can't do because: " + error);
            }
        });
        return true;
      }
    });

    $('#LoginForm').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "email": {
            required: true,
            email :true
          },
          "password": {
             required: true,
             minlength: 5
          }
        },
        messages:
        {
          "email": {
             required: "Email is required",
             email: "Please enter a valid email address"
          },
          "password": {
             required: "Password is required",
             minlength: "Your password must be at least 5 characters long"
          }
        }
    });

    $("#btn_confirm").click(function(event) {
      var otp = $('#otp_input').val();

      if(otp != user_otp){
        $('#otp-error').css("display","block");
        $('#otp-error').text("Please enter valid otp");
        // alert('Please enter valid otp');
        // $('#OtpForm .form-group').append('<div class="error-show">Please enter valid otp.</div>');
        return false;
      }
      else{
        event.preventDefault();
        $("button[name='otp_cnfrm']").text("wait...");
        $("button[name='otp_cnfrm']").attr("disabled", "disabled");
        $.ajax({
            url: "authLogin.php",
            type: "post",
            data: $('#OtpForm').serialize(),
            success: function(res) {
              /*Analytics code*/
              ga('send', 'event', 'form', 'submit', 'submit');
              /*/Analytics code*/
              $("button[name='otp_cnfrm']").text("confirm");
              $("button[name='otp_cnfrm']").removeAttr("disabled");
              $("#myModal").modal("hide");
              $('#RegistrationForm')[0].reset();
              $('#OtpForm')[0].reset();
              // location.reload();
              $('.flash-message').html('<div class="alert alert-success" id="flash-msg">Registered Successfully, Now proceed with registration form submission by login with your verified login details below.</div>');
              $("#flash-msg").delay(5000).fadeOut("slow");
              // window.location.href = 'check-session.php'
            },
            error: function (request, error) {
              $("button[name='otp_cnfrm']").text("confirm");
              $("button[name='otp_cnfrm']").removeAttr("disabled");
              $('.flash-message').html('<div class="alert alert-danger" id="flash-msg">Something went wrong, please try again</div>');
              $("#flash-msg").delay(5000).fadeOut("slow");
              // alert(" Can't do because: " + error);
            }
        });
        return true;
      }
    });

  });
</script>


<script> //material-btn
(function (window, $) {

  $(function() {


    $('.ripple').on('click', function (event) {
      event.preventDefault();

      var $div = $('<div/>'),
          btnOffset = $(this).offset(),
          xPos = event.pageX - btnOffset.left,
          yPos = event.pageY - btnOffset.top;



      $div.addClass('ripple-effect');
      var $ripple = $(".ripple-effect");

      $ripple.css("height", $(this).height());
      $ripple.css("width", $(this).height());
      $div
        .css({
          top: yPos - ($ripple.height()/2),
          left: xPos - ($ripple.width()/2),
          background: $(this).data("ripple-color")
        })
        .appendTo($(this));

      window.setTimeout(function(){
        $div.remove();
      }, 2000);
    });

  });

})(window, jQuery);
</script>
<script type='text/javascript'>
function refreshCaptcha(){
  var img = document.images['captchaimg'];
  img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>