<?php
session_start();
include 'meekrodb.2.3.class.php';

$query = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";
$row = DB::queryFirstRow($query);

$hindi_spk = '';
$hindi_read = '';
$hindi_write = '';

$eng_spk = '';
$eng_read = '';
$eng_write = '';

if ($row['language_hindi'] != '') {
    $hindi = explode(',', $row['language_hindi']);
    for ($i = 0; $i < sizeof($hindi); $i++) {
        if ($hindi[$i] == 'speak') {
            $hindi_spk = 'checked="checked"';
        }
        if ($hindi[$i] == 'read') {
            $hindi_read = 'checked="checked"';
        }
        if ($hindi[$i] == 'write') {
            $hindi_write = 'checked="checked"';
        }
    }
}

if ($row['language_english'] != '') {
    $english = explode(',', $row['language_english']);
    for ($i = 0; $i < sizeof($english); $i++) {
        if ($english[$i] == 'speak') {
            $eng_spk = 'checked="checked"';
        }
        if ($english[$i] == 'read') {
            $eng_read = 'checked="checked"';
        }
        if ($english[$i] == 'write') {
            $eng_write = 'checked="checked"';
        }
    }
}

?>
<form id="form_language" name="form_language">
<div class="my-dtl-feed">
  <div class="col-md-12">
<div class="group">
      <div class="col-md-6">
              <div class="my-input-bx field required-field">
                  <label class="my-label">Hindi
                  </label>
                   <span class="bar"></span>
                  <div class="chk-bx">
                     <div class="checkbox">
                          <input id="language_hindi" type="checkbox" value="speak" name="language_hindi[]" <?=$hindi_spk?>> Speak
                     </div>
                     <div class="checkbox">
                          <input id="language_hindi" type="checkbox" value="read" name="language_hindi[]" <?=$hindi_read?>> Read
                     </div>
                     <div class="checkbox">
                          <input id="language_hindi" type="checkbox" value="write" name="language_hindi[]" <?=$hindi_write?>> Write
                     </div>
                  </div>
                  <input type="hidden" name="chk_hindi" id="chk_hindi" value="">
              </div>
      </div>

      <div class="col-md-6">
              <div class="my-input-bx field required-field">
               <label class="my-label">English
                  </label>
                   <span class="bar"></span>
                  <div class="chk-bx">
                     <div class="checkbox">
                          <input id="language_english" type="checkbox" value="speak" name="language_english[]" <?=$eng_spk?>> Speak
                     </div>
                     <div class="checkbox">
                          <input id="language_english" type="checkbox" value="read" name="language_english[]" <?=$eng_read?>> Read
                     </div>
                     <div class="checkbox">
                          <input id="language_english" type="checkbox" value="write" name="language_english[]" <?=$eng_write?>> Write
                     </div>
                     <input type="hidden" name="chk_eng" id="chk_eng" value="">
                  </div>
              </div>
      </div>
      </div>
<div class="group">
  <div class="col-md-6">
     <div class="my-input-bx">
        <input class="form-control" id="language_other" name="language_other"   type="text" value="<?=$row['language_other']?>">
        <span class="bar"></span>
        <label>Other Languages, If Any</label>
     </div>
  </div>


  <nav class="form-section-nav">
    <input type="hidden" name="action" id="action" value="save_lang">
      <span id="btn_back_lang" class="btn-secondary form-nav-prev"> <img src="images/left-arrow.jpg" alt="left">  Prev</span>
      <div class="loader" style="position: fixed; top: 35%; left: 48%;"></div><span id="btn_next_lang" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
  </nav>
</div>
</div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function(){

    $("#btn_back_lang").unbind().click(function() {

      if("<?=$row['Programme'] == 'PG'?>"){
        $('#degree_container').load('form_degree.php',function(e){
           $("#language_container" ).slideUp( "slow");
           $('#language_container').html('');
          // $("#degree_container" ).slideDown( "slow");
           $("#degree_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,600);
          });
        });
      }else{
          $('#hs_container').load('form_hs.php',function(e){
            $("#language_container" ).slideUp( "slow");
            $('#language_container').html('');
            //$("#hs_container" ).slideDown( "slow");
            $("#hs_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,600);
          });
          });
      }
    });

    $("#btn_next_lang").unbind().click(function() {

       var count_checked = $("[name='language_hindi[]']:checked").length;
       var count_checked1 = $("[name='language_english[]']:checked").length;

       $('#chk_hindi').val('1');
       $('#chk_eng').val('1');

       if(count_checked == 0){
        $('#chk_hindi').val('');
       }

       if(count_checked1 == 0){
        $('#chk_eng').val('');
       }


        if(!$('#form_language').valid()){
          return false;
        }

      //  var formData = new FormData($('form#form_language')[0]);
var formData = $('form#form_language').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            beforeSend: function() {
                    $('.loader').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
            },
            success: function(response) {
              if(response.status == 1){
                $('#specialization_container').load('form_specialization.php',function(e){
                  $("#language_container" ).slideUp( "slow");
                  $('#language_container').html('');
                 // $("#specialization_container" ).slideDown( "slow");
                  $("#specialization_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,1100);
                     });
                });
              }
            }
        });

    });

    $('#form_language').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "chk_hindi": {
            required: true
          },
          "chk_eng": {
            required: true
          },
        },
        messages:
        {
         "chk_hindi": {
            required: "Please select one option."
          },
          "chk_eng": {
            required: "Please select one option."
          },
        }
  });
});
</script>