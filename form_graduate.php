<?php
session_start();
include 'meekrodb.2.3.class.php';

$query = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";

$row = DB::queryFirstRow($query);

$ug_checked = 'checked';
$pg_checked = '';
$int_checked = '';

if (!empty($row)) {
    if ($row['Programme'] != '' && $row['Programme'] == 'PG') {
        $ug_checked = '';
        $int_checked = '';
        $pg_checked = 'checked';
    } elseif ($row['Programme'] != '' && $row['Programme'] == 'INTG') {
        $ug_checked = '';
        $int_checked = 'checked';
        $pg_checked = '';
    }
}
?>
<form id="form_graduate" name="form_graduate" method="post">
<div class="my-radio-bnt">
  <div class="col-md-12">
     <div class="chk-rdo field required-field" >
         <div class="col-md-4">
            <input type="radio" id="radio_ug" name="Programme" <?=$ug_checked?> value="UG"> 4 Year Integrated Bachelors Programme (CFPD + B. VOC)
         </div>
         <div class="col-md-4">
            <input type="radio" id="radio_pg" name="Programme" <?=$int_checked?> value="INTG"> 5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)
         </div>
         <div class="col-md-4">
            <input type="radio" id="radio_pg" name="Programme" <?=$pg_checked?> value="PG"> Master of Vocation in Crafts and Design
         </div>

     </div>
  </div>
</div>
<nav class="form-section-nav">
    <input type="hidden" name="action" id="action" value="save_graduate">
    <span id="btn_next_graduate" name="btn_save_next" class="btn-std form-nav-next" > Save & Next <img src="images/right-arrow.jpg" alt="left"></span><div class="loader" style="position: fixed; top: 35%; left: 48%;"></div>
</nav>
</form>

<script type="text/javascript">
$(document).ready(function(){

    $('#degree_section').removeClass('displayblock').addClass('displaynone');
    if("<?=$pg_checked != ''?>"){
      $('#degree_section').removeClass('displaynone').addClass('displayblock');
      $('#lbl_deg').text('6');
      $('#lbl_lng').text('7');
      $('#lbl_spec').text('8');
      $('#lbl_attach').text('9');
    }else{
      $('#lbl_lng').text('6');
      $('#lbl_spec').text('7');
      $('#lbl_attach').text('8');
    }

    $("input[name='Programme']").change(function(){
        if($(this).val() !='' && $(this).val()=='PG'){
          $('#degree_section').removeClass('displaynone').addClass('displayblock');
          $('#lbl_deg').text('6');
          $('#lbl_lng').text('7');
          $('#lbl_spec').text('8');
          $('#lbl_attach').text('9');
        }else{
          $('#degree_section').removeClass('displayblock').addClass('displaynone');
          $('#lbl_lng').text('6');
          $('#lbl_spec').text('7');
          $('#lbl_attach').text('8');
        }
    });

    $("#btn_next_graduate").unbind().click(function() {

      console.log('hdjsdjksdh');

        if(!$('#form_graduate').valid()){
          return false;
        }

        //var formData = new FormData($('form#form_graduate')[0]);
var formData = $('form#form_graduate').serialize();
//var formData1 = $( "form#form_graduate input, form#form_graduate textarea, form#form_graduate select" ).serialize();
//alert($( "form#form_graduate input[type='radio']").val());
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            beforeSend: function() {
                    $('.loader').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
            },
            success: function(response) {
                if(response.status == 1){

                    // $('#personal_container').load('form_personal.php',function(e){

                    // $("#graduate_container" ).slideUp( "slow");
                    // $('#graduate_container').html('');
                    // $("#personal_container" ).slideDown( "slow", function(e) {
                    // window.scrollTo(0,600);
                    // });

                    // });


                  $('#personal_container').load('form_personal.php',function(e){
                    $('#graduate_container').html('');

                    console.log('loading the file');

                      $("#graduate_container" ).slideUp( "slow", function() {

                        $("#personal_container" ).slideDown( "slow");
                      });

                  });
                }
            }
        });

    });

     $('#form_graduate').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "Programme": {
            required: true
          }
        },
        messages:
        {
         "Programme": {
            required: "Programme is required"
          }
        }
  });

});
</script>