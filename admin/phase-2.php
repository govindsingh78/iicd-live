<?php
session_start();
require_once '../meekrodb.2.3.class.php';

$years = array('2001' => '2001', '2002' => '2002', '2003' => '2003', '2004' => '2004', '2005' => '2005', '2006' => '2006', '2007' => '2007', '2008' => '2008', '2009' => '2009', '2010' => '2010', '2011' => '2011', '2012' => '2012', '2013' => '2013', '2014' => '2014', '2015' => '2015', '2016' => '2016', '2017' => '2017', '2018' => '2018');

$specialisations = array('Fired Material Design' => 'Fired Material Design', 'Soft Material Design' => 'Soft Material Design', 'Hard Material Design' => 'Hard Material Design', 'Fashion Design' => 'Fashion Design', 'Fired Material Specialization' => 'Fired Material Specialization', 'Hard Material Specialization' => 'Hard Material Specialization', 'Soft Material Specialization' => 'Soft Material Specialization');
############  Filter by Year Students Enrolled ###########
$previoustopreviousyr = date("Y", strtotime("-2 year")) . "-" . date("Y", strtotime("-1 year"));
$previousyr = date("Y", strtotime("-1 year")) . "-" . date("Y");
$currentyr = date("Y") . "-" . date("Y", strtotime("+1 year"));

?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
            <script src="http://code.jquery.com/jquery-1.9.1.js"></script>

            <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        <!-- END Stylesheets -->
        <style type="text/css">
            .block .block {
                height: 400px;
                border: 1px solid #dae0e8;
                -webkit-box-shadow: none;
                box-shadow: none;
            }
            input{
                    padding-left: 6px;
            }
            .displayBlock{
                display: block;
            }
            .displayNone{
                display: none;
            }
        </style>

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once 'header.php';?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full">
                            <div class="row">

                                 <div class="row">
                            <div class="col-sm-12">
                                <!-- Block -->
                                <div class="block" style="height: auto; overflow: hidden;">

                                    <!-- Block Title -->
                                    <div class="block-title">
                                        <h2>Students Phase II</h2>

                                    </div>
                                    <!-- END Block Title -->

                                    <!-- Block Content -->

                                   <form method="get"   id="preview_form" name="preview_form">

                                   <!--###### Fetching All the Columns from Users & User_Details ########-->
                                   <div class="form-group col-md-12">
                                         <select id="sfn" name="sfn" class="form-control"  onchange="useAjaxtoFilterResults(this.value)">
                                            <option value="">Select Field Name</option>
                                            <?php
$columns_users = DB::columnList('users');
$columns_userdetails = DB::columnList('user_details');
$columnsArray = array_merge($columns_users, $columns_userdetails);
$newArray = array("created_at");
//'first_name','middle_name','last_name','email','enroll_id','created_at','updated_at','Programme','dob','gender','phone','marital_status','category','nationality','domicile' ,'fathers_name','mothers_name','phone_mother','email_mother','craft_name','phone_father','email_father',' guardians_name' ,'guardians_relation' ,'phone_guardian' ,'email_guardian' ,'craft_relation' ,'family_income' ,'medical_info' ,'address_line1' ,'address_line2' ,'city' ,'state' ,'country' ,'pin_code' ,'caddress_line1' ,'caddress_line2' ,'ccity' ,'cstate' ,'ccountry' ,'cpin_code' ,'exam_center1' ,'exam_center2' ,'exam_center3'<option value="twelft_pass_year">twelft_pass_year' ,'twelft_stream' ,'twelft_other_stream' ,'twelft_board_name' ,'twelft_school_name' ,'twelft_school_address' ,'degree_col_name' ,'degree_col_address' ,'twelft_grade' ,'degree_pass_year' ,'degree_stream' ,'degree_other_stream' ,'degree_col_univ' ,'degree_grade' ,'language_hindi' ,'language_english' ,'language_other' ,'specialization_choice1' ,'specialization_choice2' ,'specialization_choice3' ,'applicant_photo' ,'dob_certificate' ,'id_proof' ,'declaration' ,'signatures','created_at','updated_at'
$columnsnewArray = array_diff($columnsArray, array('id', 'password', 'otp', 'user_id', 'email_verified', 'phone_verified', 'email_sent', 'admit_card', 'pdf_sent', 'created_at', 'updated_at', 'ccity', 'cstate', 'ccountry', 'declaration', 'updated_at', 'phase1_attendence', 'phase1_result', 'phase3_attendence', 'phase3_result', 'address_line2', 'caddress_line2', 'addresscheck', 'id_proof', 'category', 'domicile', 'village_name', 'cvillage_name', 'mail_count', 'disclaimer_check'));
//formattedValue($val)
$columns = array_merge($columnsnewArray, $newArray);

foreach ($columns as $val) {
    if ($val == "created_at") {
        $formattedVal = "Year of Enrollment";
    } else {
        $formattedVal = formattedValue($val);
    }
    echo '<option value="' . $val . '" ' . $selected . '>' . $formattedVal . '</option>';
}
?>
                                          </select>
                                    </div>
                                    <!--###### Fetching All the Columns from Users & User_Details ########-->

                                   <?php //var_dump($columns);?>

                                    <!--###### Field Changes With Respect to Column Selected Using Ajax ########-->
                                    <div id="replaceAcctoColumn">

                                    </div>
                                     <!--###### Field Changes With Respect to Column Selected Using Ajax ########-->



                                 <!--###### 17112018 Function to Convert a string as Name Format Starts ########-->
<?php
function formattedValue($val)
{
    $nameFormatted = str_replace('_', ' ', trim($val));
    $formattedValue = ucwords($nameFormatted);

    if ($val == "txnid") {
        return "Transaction ID";
    } elseif ($val == "address_line1") {
        return "House No./Flat No./Building Name";
    } elseif ($val == "caddress_line1") {
        return "Correspondence House No./Flat No./Building Name";
    } elseif ($val == "block_or_tehsil") {
        return "Village/Block/Tehsil/Locality/Landmark";
    } elseif ($val == "bhamashah") {
        return "Bhamashah ID No.";
    } elseif ($val == "cblock_or_tehsil") {
        return "Correspondence Village/Block/Tehsil/Locality/Landmark";
    } elseif ($val == "ccity") {
        return "Correspondence City";
    } elseif ($val == "cstate") {
        return "Correspondence State";
    } elseif ($val == "ccountry") {
        return "Correspondence Country";
    } elseif ($val == "cpin_code") {
        return "Correspondence Pin Code";
    } else {
        return $formattedValue;
    }
}
?>
<!--###### Function to Convert a string as Name Format Starts ########-->





                                            <div class="form-group col-md-12">
                                            <div class="pull-right">
                                            <a class="btn btn-primary"  onclick="displayRecords(50, 1)"><i class="fa fa-filter"></i> Apply </a>

                                            <a class="btn btn-primary" href="multilavel_field_data_search.php"><i class="fa fa-filter"></i> Reset</a>

                                            <a class="btn btn-primary" onclick="exportRecords()"><i class="fa fa-download"></i> Download</a>
                                            </div>
                                            </div>



                                    </form>


                                    <!-- END Block Content -->
                                </div>
                                <!-- END Block -->
                            </div>

                            <form action="user-export.php" method="post" class="myexcel">
                                <input type="hidden" name="search_first_name" value="<?php if (isset($_POST['first_name'])) {echo $_POST['first_name'];} else {echo '';}?>">

                                <input type="hidden" name="search_email" value="<?php if (isset($_POST['email'])) {echo $_POST['email'];} else {echo '';}?>">

                                <input type="hidden" name="search_phone" value="<?php if (isset($_POST['phone'])) {echo $_POST['phone'];} else {echo '';}?>">

                                <input type="hidden" name="search_payment" value="<?php if (isset($_POST['payment_status'])) {echo $_POST['payment_status'];} else {echo '';}?>">

                                <input type="hidden" name="search_status"  value="<?php if (isset($_POST['status'])) {echo $_POST['status'];} else {echo '';}?>" >

                                <input type="hidden" name="search_from_date"  value="<?php if (isset($_POST['from_date'])) {echo $_POST['from_date'];} else {echo '';}?>" >

                                <input type="hidden" name="search_to_date"  value="<?php if (isset($_POST['to_date'])) {echo $_POST['to_date'];} else {echo '';}?>" >

                                <input type="hidden" name="search_Programme"  value="<?php if (isset($_POST['Programme'])) {echo $_POST['Programme'];} else {echo '';}?>" >

                                <input type="hidden" name="search_dob"  value="<?php if (isset($_POST['dob'])) {echo $_POST['dob'];} else {echo '';}?>" >

                                <input type="hidden" name="search_gender"  value="<?php if (isset($_POST['gender'])) {echo $_POST['gender'];} else {echo '';}?>" >

                                <input type="hidden" name="search_marital_status"  value="<?php if (isset($_POST['marital_status'])) {echo $_POST['marital_status'];} else {echo '';}?>" >

                                <input type="hidden" name="search_category"  value="<?php if (isset($_POST['category'])) {echo $_POST['category'];} else {echo '';}?>" >

                                <input type="hidden" name="search_nationality"  value="<?php if (isset($_POST['nationality'])) {echo $_POST['nationality'];} else {echo '';}?>" >

                                <input type="hidden" name="search_domicile"  value="<?php if (isset($_POST['domicile'])) {echo $_POST['domicile'];} else {echo '';}?>" >

                                <input type="hidden" name="search_exam_center1"  value="<?php if (isset($_POST['exam_center1'])) {echo $_POST['exam_center1'];} else {echo '';}?>" >

                                <input type="hidden" name="search_exam_center2"  value="<?php if (isset($_POST['exam_center2'])) {echo $_POST['exam_center2'];} else {echo '';}?>" >

                                <input type="hidden" name="search_exam_center3"  value="<?php if (isset($_POST['exam_center3'])) {echo $_POST['exam_center3'];} else {echo '';}?>" >

                                <input type="hidden" name="search_twelft_pass_year"  value="<?php if (isset($_POST['twelft_pass_year'])) {echo $_POST['twelft_pass_year'];} else {echo '';}?>" >

                                <input type="hidden" name="search_specialization_choice1"  value="<?php if (isset($_POST['specialization_choice1'])) {echo $_POST['specialization_choice1'];} else {echo '';}?>" >

                                <input type="hidden" name="search_specialization_choice2"  value="<?php if (isset($_POST['specialization_choice2'])) {echo $_POST['specialization_choice2'];} else {echo '';}?>" >

                                <input type="hidden" name="search_specialization_choice3"  value="<?php if (isset($_POST['specialization_choice3'])) {echo $_POST['specialization_choice3'];} else {echo '';}?>" >

                                <!-- Added Extra Input Box for Year of Enrollment -->
                                <input type="hidden" name="year_of_enrollment"  value="<?php if (isset($_POST['year_of_enrollment'])) {echo $_POST['year_of_enrollment'];} else {echo '';}?>" >

                            </form>




                        </div>
                            </div>

                             <!-- Checkbox option for Preview Multiple Admit Cards Starts -->
                             <div class="row" style="margin-bottom: 5px;">
                                <div class="col-md-12">
                                <div class="pull-right">
                                <input type='button' id='previewAdmitCards' class="btn btn-primary" value='Preview Multiple Admit Cards' onclick='previewDiv();'>
                                </div>
                                <div class="pull-left">
                                <input type="checkbox" id="ckbCheckAll" /> <span style="margin: 3px; position: absolute; margin-left: 5px;">Check/Uncheck All</span>
                                </div>
                                </div>
                                </div>
                                <!-- Checkbox option for Preview Multiple Admit Cards Ends -->


                            <div class="row">
                                     <div class="loader"></div>
                                    <!-- <div id="resultsExport"></div> -->
                                     <!-- Ajax Based Table Rendering Process -->

                                     <div id="results"></div>

                                     <!-- Ajax Based Table Rendering Process -->





                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>


        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
        <script src="js/pages/formsComponents.js"></script>
        <script>$(function(){ FormsComponents.init(); });</script>
    </body>
</html>
<script type="text/javascript">
	$( document ).ready(function() {
		$('.detelebtn').click(function() {
			$('#modal-fade').fadeIn("fast");
			var id=$(this).attr('id');
			var username=$(this).attr('username');
			$('#modal-fade .showuser').text(username);
			$('#modal-fade .myprocesdelete').attr('href','deletevideo.php?id='+id+'&delete=1');
		});
	});
</script>


<script>
	function pagination(elem){
        var datastring = $("#preview_form").serialize();
     	console.log(datastring);
			$.ajax({
				type: "POST",
				url: "ajax.php",
		        data:datastring,
				success: function (result)
					{
						var aee = result.split('*');
		   				var a1 = aee[0];
				   		var a2 = aee[1];
				   		$('.mytabledata').html(a1);
				   		$('.mypagidataul').html('');
				   		$('.mypagidataul').html(a2);
						//$(".make-switch.init").removeClass('init').bootstrapSwitch();
					}
			});
	}







        //####### Function used for updating attendence of students starts

        function attendenceUpdate(valId) {
            var datastring = $("#attendenceForm"+valId).serialize();
            console.log(datastring);
            $.ajax({
                type: "GET",
                url: "user-attendence.php",
                data: datastring+"&pagename=phase2",
                cache: false,
                beforeSend: function() {
                    $('.loader').html('<img src="images/spinner.gif" alt="" width="45" height="45">');
                },
                success: function(html) {
                    console.log("True"+html);
                     //$("#messageAttendence"+valId).html(html);
                     $('.loader').html('');
                     $('#'+html).html('');
                     $('#'+html).html('<i class="fa fa-check-circle-o pull-right" aria-hidden="true" style="margin-top: 10px"></i>');
                     $('#'+html).fadeIn().delay(2000).fadeOut();
                    //###18102018 From Here will have to disable the radio button for pass , fail, not declared in case of student absent or not marked

                    var resultIdStatus = html.split('phase2attendencemessage');
                    var wordtobeUsed = resultIdStatus[1];
                    var radioValue = wordtobeUsed[wordtobeUsed.length -1];
                    var resultIdExp = wordtobeUsed.slice(0,-1);
                    var resultId = resultIdExp[0];
                    console.log(resultIdExp);
                    console.log(radioValue);
                    var nameofresultRadio = "result"+resultIdExp;
                    if(radioValue != 1){
                    $('input[name="'+nameofresultRadio+'"]').attr('disabled', true);
                    $('input[name="'+nameofresultRadio+'"]').attr('checked', true).val([0]);
                    }else{
                    $('input[name="'+nameofresultRadio+'"]').attr('disabled', false);
                    }

                 }
              });
            }

    //####### Function used for updating attendence of students ends

      //####### Function used for updating results of students starts

        function resultUpdate(valId) {
            var datastring = $("#resultForm"+valId).serialize();
            console.log(datastring);
            $.ajax({
                type: "GET",
                url: "user-results.php",
                data: datastring+"&pagename=phase2",
                cache: false,
                beforeSend: function() {
                    $('.loader').html('<img src="images/spinner.gif" alt="" width="45" height="45">');
                },
                success: function(html) {
                    console.log("False"+html);
                    $('.loader').html('');
                    $('#'+html).html('');
                    $('#'+html).html('<i class="fa fa-check-circle-o pull-right" aria-hidden="true" style="margin-top: 10px"></i>');
                    $('#'+html).fadeIn().delay(2000).fadeOut();



                    //$("#messageResults"+valId).html(html);
                    // $('.loader').html('');
                 }
              });
            }

    //####### Function used for updating results of students ends



    //####### Function used for download excel spreadsheet from user-export.php starts

        function exportRecords() {
            var datastring = $("#preview_form").serialize();
            console.log(datastring);
            $.ajax({
                type: "GET",
                url: "user-export-phase-2.php",
                data: datastring,
                cache: false,
                beforeSend: function() {
                    $('.loader').html('<img src="images/spinner.gif" alt="" width="45" height="45">');
                },
                success: function(html) {
                    $('.loader').html('');
                    console.log("True");
                     location.replace('Phase-2-Data.xls');
                     //$("#resultsExport").html(html);
                    // $('.loader').html('');
                 }
              });
            }

    //####### Function used for download excel spreadsheet from user-export.php ends




            // #####- Function to Make Table Based on Ajax Call without reloading the whole page -######

            function displayRecords(numRecords, pageNum) {
            var datastring = $("#preview_form").serialize();
            console.log(datastring);
            $.ajax({
                type: "GET",
                url: "getrecordsphase2.php",
                data: datastring+"&show=" + numRecords + "&pagenum=" + pageNum,
                cache: false,
                beforeSend: function() {
                    $('.loader').html('<img src="images/spinner.gif" alt="" width="45" height="45">');
                },
                success: function(html) {
                    console.log(datastring+numRecords+pageNum);
                    $("#results").html(html);
                    $('.loader').html('');
                    //$('#demo-save-report').hide('');
                    //$('#demo-filter-result').hide('');
                    $(".collapse").collapse('hide');
                }
            });
            }
            // used when user change row limit
            function changeDisplayRowCount(numRecords) {
            displayRecords(numRecords, 1);
            }
            $(document).ready(function() {
            displayRecords(50, 1);
            });

            // #####- Function to Make Table Based on Ajax Call without reloading the whole page -######



</script>
<script>
function MyReset(elem){
//alert(1);
    $('.myreset').submit();
}
function Myexcel(elem){
    $('.myexcel').submit();
}
</script>
<script type="text/javascript">



//###### Function to Clean Up Text Starts

        function formatText(fieldName){
            var string = fieldName.replace(/[^a-z0-9\s]/gi, ' ');
            var placeHolderText = string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
            });

            if(placeHolderText == "Address Line1"){
                return "House No./Flat No./Building Name";
            }else if(placeHolderText == "Caddress Line1"){
                return "Correspondence House No./Flat No./Building Name";
            }else if(placeHolderText == "Block Or Tehsil"){
                return "Village/Block/Tehsil/Locality/Landmark";
            }else if(placeHolderText == "Cblock Or Tehsil"){
                return "Correspondence Village/Block/Tehsil/Locality/Landmark";
            }else{
                return placeHolderText;
            }
        }

//###### Function to Clean Up Text Starts

// #####- Function to Call Ajax Based on The Selected Field -######

function useAjaxtoFilterResults(fieldName){
        console.log(fieldName);
        //write html using jquery append applicant_photo

        if(fieldName == "Programme"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Select Programme-</option><option value="UG">UG</option><option value="PG">PG</option><option value="INTG">INTG</option></select></div></div></div>');
        }else if(fieldName == "gender"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Select Gender-</option><option value="male">Male</option><option value="female">Female</option></select></div></div></div>');
        }else if(fieldName == "payment_status"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Payment Status-</option><option value="success">Success</option><option value="failure">Failure</option><option value="NULL">Pending</option></select></div></div></div>');
        }
        else if(fieldName == "status"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Select Status-</option><option value="1">Approved</option><option value="0">Pending</option></select></div></div></div>');
        }else if(fieldName == "marital_status"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Marital Status-</option><option value="Single">Single</option><option value="Married">Married</option><option value="Separated">Separated</option><option value="Widowed">Widowed</option><option value="Divorced">Divorced</option></select></div></div></div>');
        }else if(fieldName == "domicile"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Domicile Status-</option><option value="yes">with Domicile</option><option value="no">without Domicile</option></select></div></div></div>');
        }else if(fieldName == "specialization_choice1"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Specialisation Choice #1-</option><option value="Soft Material">Soft Material Specialization</option><option value="Hard Material">Hard Material Specialization</option><option value="Fired Material">Fired Material Specialization</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "specialization_choice2"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Specialisation Choice #2-</option><option value="Soft Material">Soft Material Specialization</option><option value="Hard Material">Hard Material Specialization</option><option value="Fired Material">Fired Material Specialization</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "specialization_choice3"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Specialisation Choice #3-</option><option value="Soft Material">Soft Material Specialization</option><option value="Hard Material">Hard Material Specialization</option><option value="Fired Material">Fired Material Specialization</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "applicant_photo"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Applicant Photo-</option><option value="jpg">Available</option><option value="NULL">Not Available</option></select></div></div></div>');
        }else if(fieldName == "signatures"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Signatures-</option><option value="jpg">Available</option><option value="NULL">Not Available</option></select></div></div></div>');
        }else if(fieldName == "id_proof"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-ID Proof-</option><option value="jpg">Available</option><option value="NULL">Not Available</option></select></div></div></div>');
        }else if(fieldName == "dob_certificate"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-DOB Certificate-</option><option value="jpg">Available</option><option value="NULL">Not Available</option></select></div></div></div>');
        }else if(fieldName == "language_english"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">Can</option><option value="not">Can\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-English Language-</option><option value="read">Read</option><option value="write">Write</option><option value="speak">Speak</option></select></div></div></div>');
        }else if(fieldName == "language_hindi"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">Can</option><option value="not">Can\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Hindi Language-</option><option value="read">Read</option><option value="write">Write</option><option value="speak">Speak</option></select></div></div></div>');
        }else if(fieldName == "craft_relation"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Craft Relation-</option><option value="yes">Yes</option><option value="no">No</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "family_income"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Family Income-</option><option value="Upto Rs.5,00,000">Upto Rs.5,00,000</option><option value="Rs.5,00,001 to Rs.10,00,000">Rs.5,00,001 to Rs.10,00,000</option><option value="Above Rs.10,00,000">Above Rs.10,00,000</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "exam_center1"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Exam Center (1st Choice)-</option><option value="Jaipur">Jaipur</option><option value="Kolkata">Kolkata</option><option value="Raipur">Raipur</option><option value="Bhopal">Bhopal</option><option value="Patna">Patna</option><option value="Lucknow">Lucknow</option><option value="New Delhi">New Delhi</option><option value="Jammu">Jammu</option><option value="Mumbai">Mumbai</option><option value="Udaipur">Udaipur</option><option value="Hyderabad">Hyderabad</option><option value="Chennai">Chennai</option><option value="Chandigarh">Chandigarh</option><option value="Guwahati">Guwahati</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "exam_center2"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Exam Center (2nd Choice)-</option><option value="Jaipur">Jaipur</option><option value="Kolkata">Kolkata</option><option value="Raipur">Raipur</option><option value="Bhopal">Bhopal</option><option value="Patna">Patna</option><option value="Lucknow">Lucknow</option><option value="New Delhi">New Delhi</option><option value="Jammu">Jammu</option><option value="Mumbai">Mumbai</option><option value="Udaipur">Udaipur</option><option value="Hyderabad">Hyderabad</option><option value="Chennai">Chennai</option><option value="Chandigarh">Chandigarh</option><option value="Guwahati">Guwahati</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "exam_center3"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Exam Center (3rd Choice)-</option><option value="Jaipur">Jaipur</option><option value="Kolkata">Kolkata</option><option value="Raipur">Raipur</option><option value="Bhopal">Bhopal</option><option value="Patna">Patna</option><option value="Lucknow">Lucknow</option><option value="New Delhi">New Delhi</option><option value="Jammu">Jammu</option><option value="Mumbai">Mumbai</option><option value="Udaipur">Udaipur</option><option value="Hyderabad">Hyderabad</option><option value="Chennai">Chennai</option><option value="Chandigarh">Chandigarh</option><option value="Guwahati">Guwahati</option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "twelft_stream"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Stream of Studies (12th or Equivalent)-</option><option value="Science">Science</option><option value="Arts">Arts</option><option value="Commerce">Commerce</option><option value="Engineering">Engineering</option><option value="Other">Other</option<option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "degree_stream"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Stream of Degree-</option><option value="Science">Science</option><option value="Arts">Arts</option><option value="Commerce">Commerce</option><option value="Engineering">Engineering</option><option value="Other">Other</option<option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "dob"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">contains</option><option value="not">doesn\'t contain</option></select> &nbsp; <input class="form-control" placeholder=" Date of Birth" id="txt_'+fieldName+'" name="txt_'+fieldName+'" type="date" value="" style="width: 48%; float: right"></div></div></div>');
        }else if(fieldName == "created_at"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Year of  Enrollment-</option><option value="<?php echo $previoustopreviousyr; ?>"><?php echo $previoustopreviousyr; ?></option><option value="<?php echo $previousyr; ?>"><?php echo $previousyr; ?></option><option value="<?php echo $currentyr; ?>"><?php echo $currentyr; ?></option><option value="NULL">Not Provided</option></select></div></div></div>');
        }else if(fieldName == "city"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; &nbsp;<select id="txt_'+fieldName+'_1" name="txt_'+fieldName+'_1" class="form-control" style="width: 16%; float: right"><option value="">-City-</option></select>&nbsp;<select id="txt_state_32" name="txt_state_32" class="form-control" style="width: 16%; float: right" onchange="loadAllCity(this.value)"><option value="">-State-</option></select>&nbsp;<select id="txt_country_31" name="txt_country_31" class="form-control" style="width: 16%; float: right" onchange="loadAllState(this.value)"></select></div></div></div>');
            $( "#txt_country_31" ).load( "country-list.php", function( response, status, xhr ) {
            console.log("Govind"+status);
            });
        }else if(fieldName == "state"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_state_1" name="txt_state_1" class="form-control" style="width: 24%; float: right"><option value="">-State-</option></select><select id="txt_country_21" name="txt_country_21" class="form-control" style="width: 24%; float: right" onchange="loadAllState(this.value)"></select>&nbsp;</div></div></div>');
            $( "#txt_country_21" ).load( "country-list.php", function( response, status, xhr ) {
            console.log("Govind"+status);
            });
        }else if(fieldName == "country"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'_1" name="txt_'+fieldName+'_1" class="form-control" style="width: 48%; float: right"></select></div></div></div>');
            $( "#txt_country_1" ).load( "country-list.php", function( response, status, xhr ) {
            console.log("Govind"+status);
            });
        }else if(fieldName == "phase2_attendence"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Attendence-</option><option value="1">Present</option><option value="2">Absent</option><option value="0">Not Marked</option></select></div></div></div>');
        }else if(fieldName == "phase2_result"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">isn\'t</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-Results-</option><option value="1">Pass</option><option value="2">Fail</option><option value="0">Not Declared</option></select></div></div></div>');
        }
        //###### 17112018 Here All New 19(10 fiels are automatically false in else + 9 select box created) fields will be added as per their new condition

        else if(fieldName == "id_proof_front"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-ID Card (Front View)-</option><option value="jpg">Available</option><option value="NULL">Not Available</option></select></div></div></div>');
        }else if(fieldName == "id_proof_back"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"><option value="">-ID Card (Back View)-</option><option value="jpg">Available</option><option value="NULL">Not Available</option></select></div></div></div>');
        }else if(fieldName == "religion"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">Select Religion</option> <option value="Hindu">Hindu</option><option value="Muslim">Muslim</option><option value="Sikh">Sikh</option><option value="Christian">Christian</option><option value="Jain">Jain</option><option value="Buddhist">Buddhist</option><option value="Other">Other</option> </select> </div></div></div>');
        }else if(fieldName == "minority"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp;<select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">Select Minority</option> <option value="Yes">Yes</option><option value="No">No</option> </select> </div></div></div>');
        } else if(fieldName == "bpl_apl"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp;  <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right" class="form-control"> <option value="">Select BPL/APL Category</option> <option value="Yes">Yes</option><option value="No">No</option> </select></div></div></div>');
        } else if(fieldName == "caste_category"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select  id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">Select Caste Category</option> <option value="General">General</option><option value="SC">SC</option><option value="ST">ST</option><option value="BC- Non Creamy Layer">BC- Non Creamy Layer</option><option value="MBC- Non Creamy Layer">MBC- Non Creamy Layer</option> </select></div></div></div>');
        } else if(fieldName == "other_category"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">Select Other Category</option> <option value="Kashmiri Migrants (KM)">Kashmiri Migrants (KM)</option><option value="Differently/Specially Abled">Differently/Specially Abled</option><option value="None">None</option> </select></div></div></div>');
        } else if(fieldName == "students_area"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">Select Students belong to Area</option> <option value="Urban">Urban</option><option value="Rural">Rural</option> </select></div></div></div>');
        } else if(fieldName == "identity_card"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">Select ID Cart Type</option> <option value="UID Aadhar">UID Aadhar</option><option value="Driver\'s License">Driver\'s License</option><option value="Passport">Passport</option><option value="Voter ID">Voter ID</option> </select></div></div></div>');
        }else if(fieldName == "signup_tracking"){
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">is</option><option value="not">is not</option></select> &nbsp; <select id="txt_'+fieldName+'" name="txt_'+fieldName+'" class="form-control" style="width: 48%; float: right"> <option value="">-Sign UP Tracking-</option> <option value="0">Not Started Yet</option><option value="1">Completed Step 1</option><option value="2">Completed Step 2</option><option value="3">Completed Step 3</option><option value="4">Completed Step 4</option><option value="5">Completed Step 5</option><option value="6">Completed Step 6</option><option value="7">Completed Step 7</option><option value="8">Completed Step 8</option><option value="9">Completed Step 9</option> </select></div></div></div>');
        }

        else{
            $('#replaceAcctoColumn').append('<div id="innerDiv_'+fieldName+'" class="col-md-12" style="margin: 0px; padding: 0px;"><div class="form-group col-md-1"><input type="checkbox" id="chk_'+fieldName+'" name="chk_'+fieldName+'" value="'+fieldName+'" onclick="displayBox(this.value)" checked="checked"/></div><div class="form-group col-md-3"><span class="pull-left">'+formatText(fieldName)+'</span></div><div class="form-group col-md-8"><div id="showhideDiv_'+fieldName+'"><select id="sel_'+fieldName+'" name="sel_'+fieldName+'" class="form-control" style="width: 48%; float: left"><option value="">contains</option><option value="not">doesn\'t contain</option></select> &nbsp; <input class="form-control" placeholder="'+formatText(fieldName)+'" id="txt_'+fieldName+'" name="txt_'+fieldName+'" type="text" value="" style="width: 48%; float: right"></div></div></div>');
        }
        $("#sfn").val($("#sfn option:first").val());
        $("#sfn option[value="+fieldName+"]").prop('disabled','disabled');
}

// #####- Function to Call Ajax Based on The Selected Field -######

function loadAllCity(validState) {
    var stateid = validState;
    console.log(stateid);
    if(stateid !=''){
      $.ajax({
          type: "POST",
          url: "../showValidCity.php",
          dataType: "json",
          data: "stateid=" + stateid,
          success: function (response) {
            $('#txt_city_1').html(response.data);

          }
      });
    }
 }

  function loadAllState(validCountry) {
    var countryid = validCountry;
    console.log(countryid);
    if(countryid !=''){
      $.ajax({
          type: "POST",
          url: "../showValidState.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#txt_state_1').html(response.data);
            $('#txt_state_32').html(response.data);
           console.log("In");
          }
      });
    }
 }

//##### Jquery to Detect Changes in State of Checkbox specialization_choice1 #####

function displayBox(valueChk){
           console.log(valueChk);

            if($("#chk_"+valueChk).is(":checked")) {
                console.log("Govind Singh");
                $("#showhideDiv_"+valueChk).css('display', "block");
            }
            if(!$("#chk_"+valueChk).is(":checked")) {
                console.log("Govind Singh !!");
                $("#showhideDiv_"+valueChk).css('display', "none");
                if(valueChk == "Programme" || valueChk == "gender" || valueChk == "payment_status" || valueChk == "status" || valueChk == "marital_status" || valueChk == "domicile" || valueChk == "specialization_choice1" || valueChk == "specialization_choice2" || valueChk == "specialization_choice3" || valueChk == "applicant_photo" || valueChk == "dob_certificate" || valueChk == "id_proof" || valueChk == "signatures" || valueChk == "language_english" || valueChk == "language_hindi" || valueChk == "craft_relation" || valueChk == "family_income" || valueChk == "exam_center1" || valueChk == "exam_center2" || valueChk == "exam_center3" || valueChk == "twelft_stream"  || valueChk == "degree_stream" || valueChk == "country" || valueChk == "state" || valueChk == "city" || valueChk == "phase2_attendence" || valueChk == "phase2_result"){
                    $("#txt_"+valueChk).val($("#txt_"+valueChk+" option:first").val());

                    $("#txt_"+valueChk+"_1").val($("#txt_"+valueChk+"_1 option:first").val());


                }else{
                    console.log("yes");
                    $('#txt_'+valueChk).val('');
                }

            }
        }

 //##### Jquery to Detect Changes in State of Checkbox #####





 //########## Function to get all the checked ID(s) for Preview Admit Cards starts

     function previewDiv(){
           //Checking if none is check alert the user to check a single checkbox at least
        var countCheck = ($("input[name='chk[]']:checked").length);
        console.log("Selected Count : "+countCheck);
        if(countCheck == 0){
        alert("Please check at least one checkbox in order to Preview Admit Card(s) !!");
        }else{
        var previewData = [];
                $.each($("input[name='chk[]']:checked"), function(){

                    console.log($(this).val());
                    previewData.push($(this).val());

                });
                var previewId = previewData.join(",");
                console.log(previewId);
                window.open("https://www.iicd.ac.in/admin/admit-card-preview.php?id="+previewId);

    }
}

    //########## Function to get all the checked ID(s) for Preview Admit Cards ends


//########## Check Uncheck All Checkbox Jquery ##########//
$(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
            if (!$(this).prop("checked")){
                $("#ckbCheckAll").prop("checked",false);
            }else{

            }
        });
    });
    //########## Check Uncheck All Checkbox Jquery ##########//

function Myexcel(elem){
    $('.myexcel').submit();
}

    function UpdateRecord(id){
      jQuery.ajax({
       type: "POST",
       url: "update_user_status.php",
       data: 'id='+id,
       cache: false,
       success: function(response)
       {
         alert("Successfully updated");
         location.reload();
       }
     });
    }


</script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$.datepicker.setDefaults({
showOn: "button",
buttonImage: "datepicker.png",
buttonText: "Date Picker",
buttonImageOnly: true,
dateFormat: 'yy-mm-dd'
});
$(function() {
$("#from").datepicker();
$("#to").datepicker();
$("#dob").datepicker();
});
</script>
<?php /*$DB -> close();*/?>