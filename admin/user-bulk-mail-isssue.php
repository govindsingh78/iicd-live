<?php require_once 'rightusercheck.php';?>
<?php
require_once '../meekrodb.2.3.class.php';
//include('../phpmailer/PHPMailerAutoload.php');
require_once 'phpmailer/class.phpmailer.php';
$path = "https://www.iicd.ac.in/";

$mailingidbulk = $_GET['mailingidbulk'];
$templateidbulk = $_GET['templateidbulk'];
$dataArray = array();
if ($templateidbulk == '') {
    $result['error'] = 'validation error';
    $result['message'] = '<span   class="btn btn-effect-ripple btn-xs btn-danger clearfix" style="overflow: hidden; position: relative; padding: 10px;">Please Select at least one template type to send to Candiate !!!</span>';
    //echo json_encode($result);exit;
    $dataArray[] = $result;
} else {

    $uniqueIds = explode(",", $mailingidbulk);
    foreach ($uniqueIds as $id) {
        $id = base64_decode($id);

        $hrEmail = 'govindsingh78@gmail.com';

        $sqlquery = "SELECT users.id AS UserID, users.*, user_details.Programme,user_details.fathers_name,user_details.dob,user_details.phone,user_details.exam_center1,user_details.applicant_photo,user_details.dob_certificate,user_details.id_proof,user_details.signatures,user_details.mail_count,exam_centers.center_name,exam_centers.city  FROM users";
        $sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
        $sqlquery = $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city";
        $sqlquery = $sqlquery . " WHERE users.id = '" . $id . "'";
        $result = DB::queryFirstRow($sqlquery);

        $first_name = $result['first_name'];
        $middle_name = $result['middle_name'];
        $last_name = $result['last_name'];
        $fullname = $first_name . ' ' . $middle_name . ' ' . $last_name;
        $fathers_name = $result['fathers_name'];
        $Programme = $result['Programme'];
        $enroll_id = $result['enroll_id'];
        $mcount = $result['mail_count'];
        $mailcount = $mcount + 1;

        if (isset($templateidbulk) && $templateidbulk == 18) {
            $appsub = DB::queryFirstRow("SELECT * FROM settings WHERE id = 18");
            $apptitle = DB::queryFirstRow("SELECT * FROM settings WHERE id = 21");

        } else if (isset($templateidbulk) && $templateidbulk == 19) {
            $appsub = DB::queryFirstRow("SELECT * FROM settings WHERE id = 19");
            $apptitle = DB::queryFirstRow("SELECT * FROM settings WHERE id = 22");

        } else if (isset($templateidbulk) && $templateidbulk == 20) {
            $appsub = DB::queryFirstRow("SELECT * FROM settings WHERE id = 20");
            $apptitle = DB::queryFirstRow("SELECT * FROM settings WHERE id = 23");

        }

        $appsubline = $appsub['value'];
        $apptitleline = $apptitle['value'];

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = 'admissions@iicd.ac.in';
        $mail->Password = 'admission915';
        $mail->setFrom($hrEmail, 'IICD');
        $mail->addReplyTo($hrEmail, 'IICD');
        $mail->addAddress($result['email']); //tl mail $result['email']
        $mail->AddCC('admissions@iicd.ac.in');
        //$mail->addAttachment($rootpath);
        $mail->Subject = " $apptitleline ";
        $mail->msgHTML($appsubline);
        if (!$mail->send()) {
            $result['failed'] = 'failed';
            $result['message'] = 'Failed';
            $dataArray[] = $result;
            // echo json_encode($result);

        } else {
            $update = DB::update('user_details', array('mail_count' => $mailcount), "user_id=%i", $id);
            $result['success'] = 'success';
            $result['message'] = '<span   class="btn btn-effect-ripple btn-xs btn-success clearfix" style="overflow: hidden; position: relative; padding: 10px;">Mail Sent Successfully to ' . $fullname . '</span>';
            //echo json_encode($result);
            $dataArray[] = $result;
        }

    }

}

echo json_encode($dataArray);
