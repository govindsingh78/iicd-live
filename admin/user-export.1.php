<?php
//error_reporting(0);
ini_set('max_execution_time', 7200000);
require_once '../meekrodb.2.3.class.php';
?>
<?php

function countryNameById($countryid)
{
    $sql = "SELECT country_name FROM country WHERE id = '" . $countryid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['country_name'];
}

function cityNameById($cityid)
{
    $sql = "SELECT city_name FROM city WHERE id = '" . $cityid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['city_name'];
}

function stateNameById($stateid)
{
    $sql = "SELECT state_name FROM state WHERE id = '" . $stateid . "'";
    $data = DB::queryFirstRow($sql);
    return $data['state_name'];
}
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/excel/Classes/PHPExcel.php';

// Create new PHPExcel object
//echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();
$objCalc = PHPExcel_Calculation::getInstance();
//print_r($objCalc->listFunctionNames());
// Set document properties
//echo date('H:i:s') , " Set properties" , EOL;
$objPHPExcel->getProperties()->setCreator("IICD")
    ->setLastModifiedBy("IICD")
    ->setTitle("IICD")
    ->setSubject("IICD")
    ->setDescription("IICD")
    ->setKeywords("IICD")
    ->setCategory("IICD");
foreach (range('A', 'CC') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(false);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth(30);
    //##### New Fields Width Set
    $objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth(30);

    //##### New Fields Width Set as on 17112018

    $objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth(30);

}

###### 17112018 Function to Convert a string as Name Format Starts ########

function formattedValue($val)
{
    $nameFormatted = str_replace('_', ' ', trim($val));
    $formattedValue = ucwords($nameFormatted);

    if ($val == "txnid") {
        return "Transaction ID";
    } elseif ($val == "address_line1") {
        return "House No./Flat No./Building Name";
    } elseif ($val == "caddress_line1") {
        return "Correspondence House No./Flat No./Building Name";
    } elseif ($val == "block_or_tehsil") {
        return "Village/Block/Tehsil/Locality/Landmark";
    } elseif ($val == "bhamashah") {
        return "Bhamashah ID No.";
    } elseif ($val == "cblock_or_tehsil") {
        return "Correspondence Village/Block/Tehsil/Locality/Landmark";
    } elseif ($val == "ccity") {
        return "Correspondence City";
    } elseif ($val == "cstate") {
        return "Correspondence State";
    } elseif ($val == "ccountry") {
        return "Correspondence Country";
    } elseif ($val == "cpin_code") {
        return "Correspondence Pin Code";
    } else {
        return $formattedValue;
    }
}

###### Function to Convert a string as Name Format Starts ########

// Create a first sheet
//echo date('H:i:s') , " Add data" , EOL;
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', "Sr.No");
$objPHPExcel->getActiveSheet()->setCellValue('B1', "First Name");
$objPHPExcel->getActiveSheet()->setCellValue('C1', "Middle Name");
$objPHPExcel->getActiveSheet()->setCellValue('D1', "Last Name");
$objPHPExcel->getActiveSheet()->setCellValue('E1', "Email");
$objPHPExcel->getActiveSheet()->setCellValue('F1', "Phone");
$objPHPExcel->getActiveSheet()->setCellValue('G1', formattedValue("Enroll no."));
$objPHPExcel->getActiveSheet()->setCellValue('H1', formattedValue("email_verified"));
$objPHPExcel->getActiveSheet()->setCellValue('I1', formattedValue("payment_status"));
$objPHPExcel->getActiveSheet()->setCellValue('J1', formattedValue("txnid"));
$objPHPExcel->getActiveSheet()->setCellValue('K1', formattedValue("Programme"));
$objPHPExcel->getActiveSheet()->setCellValue('L1', formattedValue("dob"));
$objPHPExcel->getActiveSheet()->setCellValue('M1', formattedValue("gender"));
$objPHPExcel->getActiveSheet()->setCellValue('N1', formattedValue("marital_status"));

$objPHPExcel->getActiveSheet()->setCellValue('O1', formattedValue("nationality"));

$objPHPExcel->getActiveSheet()->setCellValue('P1', formattedValue("fathers_name"));
$objPHPExcel->getActiveSheet()->setCellValue('Q1', formattedValue("mothers_name"));
$objPHPExcel->getActiveSheet()->setCellValue('R1', formattedValue("phone_father"));
$objPHPExcel->getActiveSheet()->setCellValue('S1', formattedValue("email_father"));
$objPHPExcel->getActiveSheet()->setCellValue('T1', formattedValue("guardians_name"));
$objPHPExcel->getActiveSheet()->setCellValue('U1', formattedValue("guardians_relation"));
$objPHPExcel->getActiveSheet()->setCellValue('V1', formattedValue("phone_guardian"));
$objPHPExcel->getActiveSheet()->setCellValue('W1', formattedValue("email_guardian"));
$objPHPExcel->getActiveSheet()->setCellValue('X1', formattedValue("craft_relation"));
$objPHPExcel->getActiveSheet()->setCellValue('Y1', formattedValue("family_income"));
$objPHPExcel->getActiveSheet()->setCellValue('Z1', formattedValue("medical_info"));
$objPHPExcel->getActiveSheet()->setCellValue('AA1', formattedValue("address_line1"));
//####### New Fields Added After Recent Changes  17112018

$objPHPExcel->getActiveSheet()->setCellValue('AB1', formattedValue("block_or_tehsil"));

$objPHPExcel->getActiveSheet()->setCellValue('AC1', formattedValue("city"));
$objPHPExcel->getActiveSheet()->setCellValue('AD1', formattedValue("state"));
$objPHPExcel->getActiveSheet()->setCellValue('AE1', formattedValue("country"));
$objPHPExcel->getActiveSheet()->setCellValue('AF1', formattedValue("pin_code"));
$objPHPExcel->getActiveSheet()->setCellValue('AG1', formattedValue("exam_center1"));
$objPHPExcel->getActiveSheet()->setCellValue('AH1', formattedValue("exam_center2"));
$objPHPExcel->getActiveSheet()->setCellValue('AI1', formattedValue("exam_center3"));
$objPHPExcel->getActiveSheet()->setCellValue('AJ1', formattedValue("twelft_pass_year"));
$objPHPExcel->getActiveSheet()->setCellValue('AK1', formattedValue("twelft_stream"));
$objPHPExcel->getActiveSheet()->setCellValue('AL1', formattedValue("twelft_other_stream"));
$objPHPExcel->getActiveSheet()->setCellValue('AM1', formattedValue("twelft_board_name"));
$objPHPExcel->getActiveSheet()->setCellValue('AN1', formattedValue("twelft_grade"));
$objPHPExcel->getActiveSheet()->setCellValue('AO1', formattedValue("degree_pass_year"));
$objPHPExcel->getActiveSheet()->setCellValue('AP1', formattedValue("degree_stream"));
$objPHPExcel->getActiveSheet()->setCellValue('AQ1', formattedValue("degree_other_stream"));
$objPHPExcel->getActiveSheet()->setCellValue('AR1', formattedValue("degree_col_univ"));
$objPHPExcel->getActiveSheet()->setCellValue('AS1', formattedValue("degree_grade"));
$objPHPExcel->getActiveSheet()->setCellValue('AT1', formattedValue("language_hindi"));
$objPHPExcel->getActiveSheet()->setCellValue('AU1', formattedValue("language_english"));
$objPHPExcel->getActiveSheet()->setCellValue('AV1', formattedValue("language_other"));
$objPHPExcel->getActiveSheet()->setCellValue('AW1', formattedValue("specialization_choice1"));
$objPHPExcel->getActiveSheet()->setCellValue('AX1', formattedValue("specialization_choice2"));
$objPHPExcel->getActiveSheet()->setCellValue('AY1', formattedValue("specialization_choice3"));
$objPHPExcel->getActiveSheet()->setCellValue('AZ1', formattedValue("applicant_photo"));
//####### New Fields Added After Recent Changes  17112018
$objPHPExcel->getActiveSheet()->setCellValue('BA1', formattedValue("identity_card"));
$objPHPExcel->getActiveSheet()->setCellValue('BB1', formattedValue("identity_card_no"));
$objPHPExcel->getActiveSheet()->setCellValue('BC1', formattedValue("id_proof_front"));
$objPHPExcel->getActiveSheet()->setCellValue('BD1', formattedValue("id_proof_back"));

$objPHPExcel->getActiveSheet()->setCellValue('BE1', formattedValue("declaration"));
$objPHPExcel->getActiveSheet()->setCellValue('BF1', formattedValue("payment_status"));
$objPHPExcel->getActiveSheet()->setCellValue('BG1', formattedValue("updated_at"));
$objPHPExcel->getActiveSheet()->setCellValue('BH1', formattedValue("status"));

//####### New Fields Added After Recent Changes
$objPHPExcel->getActiveSheet()->setCellValue('BI1', formattedValue("created_at"));
$objPHPExcel->getActiveSheet()->setCellValue('BJ1', formattedValue("signatures"));
$objPHPExcel->getActiveSheet()->setCellValue('BK1', formattedValue("caddress_line1"));
//####### New Fields Added After Recent Changes  17112018

$objPHPExcel->getActiveSheet()->setCellValue('BL1', formattedValue("cblock_or_tehsil"));

$objPHPExcel->getActiveSheet()->setCellValue('BM1', formattedValue("ccity"));
$objPHPExcel->getActiveSheet()->setCellValue('BN1', formattedValue("cstate"));
$objPHPExcel->getActiveSheet()->setCellValue('BO1', formattedValue("ccountry"));
$objPHPExcel->getActiveSheet()->setCellValue('BP1', formattedValue("cpin_code"));
$objPHPExcel->getActiveSheet()->setCellValue('BQ1', formattedValue("email_mother"));
$objPHPExcel->getActiveSheet()->setCellValue('BR1', formattedValue("phone_mother"));
$objPHPExcel->getActiveSheet()->setCellValue('BS1', formattedValue("twelft_school_address"));

//####### New Fields Added After Recent Changes 17112018
$objPHPExcel->getActiveSheet()->setCellValue('BT1', formattedValue("bhamashah"));
$objPHPExcel->getActiveSheet()->setCellValue('BU1', formattedValue("alternate_email"));
$objPHPExcel->getActiveSheet()->setCellValue('BV1', formattedValue("religion"));
$objPHPExcel->getActiveSheet()->setCellValue('BW1', formattedValue("other_religion"));
$objPHPExcel->getActiveSheet()->setCellValue('BX1', formattedValue("minority"));
$objPHPExcel->getActiveSheet()->setCellValue('BY1', formattedValue("bpl_apl"));
$objPHPExcel->getActiveSheet()->setCellValue('BZ1', formattedValue("emergency_phone_no"));
$objPHPExcel->getActiveSheet()->setCellValue('CA1', formattedValue("caste_category"));
$objPHPExcel->getActiveSheet()->setCellValue('CB1', formattedValue("other_category"));
$objPHPExcel->getActiveSheet()->setCellValue('CC1', formattedValue("students_area"));

//$objPHPExcel->getActiveSheet()->getStyle('A1:W1')->getFont()->setBold(true);

// Rows to repeat at top
//echo date('H:i:s') , " Rows to repeat at top" , EOL;
$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);
$border_style = array('borders' => array('right' => array('style' =>
    PHPExcel_Style_Border::BORDER_THICK, 'color' => array('argb' => '000000'))));
$objPHPExcel->getActiveSheet()->getStyle("A1:CC1")->applyFromArray($border_style);

$style = array(
    'alignment' => array(
        //     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
);

$objPHPExcel->getActiveSheet()->getStyle("A1:CC1")->applyFromArray($style);
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);

$styleArray = array(
    'font' => array(
        'bold' => true,
        'color' => array('rgb' => 'FF0000'),
        'size' => 15,
        'name' => 'Verdana',
    ));

$objPHPExcel->getActiveSheet()
    ->getStyle('A1:CC1')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '66CC99'),
            ),
        )
    );

$objPHPExcel->getDefaultStyle()
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$i = 1;
// //Start Search post value
// $search_f_name    =    $_POST['search_first_name'];
// $search_email    =    $_POST['search_email'];
// $search_phone    =    $_POST['search_phone'];
// $search_payment    =    $_POST['search_payment'];
// $search_status    =    $_POST['search_status'];
// $search_from_date    =    $_POST['search_from_date'];
// $search_to_date =   $_POST['search_to_date'];
// $search_Programme =   $_POST['search_Programme'];
// $search_dob =   $_POST['search_dob'];
// $search_gender =   $_POST['search_gender'];
// $search_marital_status =   $_POST['search_marital_status'];
// $search_category =   $_POST['search_category'];
// $search_nationality =   $_POST['search_nationality'];
// $search_domicile =   $_POST['search_domicile'];
// $search_exam_center1    =   $_POST['search_exam_center1'];
// $search_exam_center2    =   $_POST['search_exam_center2'];
// $search_exam_center3    =   $_POST['search_exam_center3'];
// $search_twelft_pass_year    =   $_POST['search_twelft_pass_year'];
// $search_specialization_choice1    =   $_POST['search_specialization_choice1'];
// $search_specialization_choice2    =   $_POST['search_specialization_choice2'];
// $search_specialization_choice3    =    $_POST['search_specialization_choice3'];
// //End Search post value

// $sql    = "SELECT users.*, user_details.* FROM users";
// $sql   =    $sql . " LEFT JOIN user_details ON user_details.user_id = users.id";
// $sql   =    $sql . " WHERE users.email_verified = 1";
// if(isset($search_f_name) && $search_f_name != "")
// {
//     $sql   =    $sql . " AND users.first_name LIKE '%".$search_f_name."%' ";
// }
// if(isset($search_email) && $search_email  != "")
// {
//     $sql   =    $sql . " AND users.email = '".$search_email."' ";
// }
// if(isset($search_phone) && $search_phone != "")
// {
//     $sql   =    $sql . " AND user_details.phone = '".$search_phone."' ";
// }
// if(isset($search_payment) && $search_payment  != "" )
// {
//     if ($search_payment  == "notdone")
//     {
//          $sql   =    created_atayment_status IS NULL";
//     }else
//     {
//     $sql   =    $sql created_att_status = '".$search_payment."'";
//     }
// }
// if(isset($search_statcreated_ats != "")
// {
//     $sql   =    $sql created_at = '".$search_status."' ";
// }
// if($search_from_date != "" && $search_to_date != "" )
// {
//     $from = date('Y-m-d H:i:s',strtotime($search_from_date));
//     $to = date('Y-m-d H:i:s',strtotime($search_to_date));
//     $sql   =    $sql . " AND users.updated_at BETWEEN '".$from."' AND '".$to."'";
// }
// if( $search_Programme != "")
// {
//     $sql   =    $sql . " AND user_details.Programme = '".$search_Programme."' ";
// }

// if($search_dob != "")
// {
//     $dob = date('Y-m-d H:i:s',strtotime($search_dob));
//     $sql   =    $sql . " AND users.dob LIKE '".$dob."' ";
// }

// if( $search_gender != "")
// {
//     $sql   =    $sql . " AND user_details.gender = '".$search_gender."' ";
// }
// if( $search_marital_status != "")
// {
//     $sql   =    $sql . " AND user_details.marital_status = '".$search_marital_status."' ";
// }

// if($search_category != "")
// {
//     $sql   =    $sql . " AND user_details.category = '".$search_category."' ";
// }
// if( $search_nationality != "")
// {
//     $sql   =    $sql . " AND user_details.nationality = '".$search_nationality."' ";
// }
// if( $search_domicile != "")
// {
//     $sql   =    $sql . " AND user_details.domicile = '".$search_domicile."' ";
// }
// if( $search_exam_center1 != "")
// {
//     $sql   =    $sql . " AND user_details.exam_center1 = '".$search_exam_center1."' ";
// }
// if( $search_exam_center2 != "")
// {
//     $sql   =    $sql . " AND user_details.exam_center2 = '".$search_exam_center2."' ";
// }
// if( $search_exam_center3 != "")
// {
//     $sql   =    $sql . " AND user_details.exam_center3 = '".$search_exam_center3."' ";
// }
// if( $search_twelft_pass_year != "")
// {
//     $sql   =    $sql . " AND user_details.twelft_pass_year = '".$search_twelft_pass_year."' ";
// }
// if($search_specialization_choice1 != "")
// {
//     $sql   =    $sql . " AND user_details.specialization_choice1 = '".$search_specialization_choice1."' ";
// }

// if($search_specialization_choice2 != "")
// {
//     $sql   =    $sql . " AND user_details.specialization_choice2 = '".$search_specialization_choice2."' ";
// }
// if($search_specialization_choice3 != "")
// {
//     $sql   =    $sql . " AND user_details.specialization_choice3 = '".$search_specialization_choice3."' ";
// }

####### Condition in Query Added for Dynamic Multilavel Filter #######
$columns_users = DB::columnList('users');
$columns_userdetails = DB::columnList('user_details');
$columnsArray = array_merge($columns_users, $columns_userdetails);
$newArray = array("created_at");
//'first_name','middle_name','last_name','email','enroll_id','created_at','updated_at','Programme','dob','gender','phone','marital_status','category','nationality','domicile' ,'fathers_name','mothers_name','phone_mother','email_mother','craft_name','phone_father','email_father',' guardians_name' ,'guardians_relation' ,'phone_guardian' ,'email_guardian' ,'craft_relation' ,'family_income' ,'medical_info' ,'address_line1' ,'address_line2' ,'city' ,'state' ,'country' ,'pin_code' ,'caddress_line1' ,'caddress_line2' ,'ccity' ,'cstate' ,'ccountry' ,'cpin_code' ,'exam_center1' ,'exam_center2' ,'exam_center3'<option value="twelft_pass_year">twelft_pass_year' ,'twelft_stream' ,'twelft_other_stream' ,'twelft_board_name' ,'twelft_school_name' ,'twelft_school_address' ,'degree_col_name' ,'degree_col_address' ,'twelft_grade' ,'degree_pass_year' ,'degree_stream' ,'degree_other_stream' ,'degree_col_univ' ,'degree_grade' ,'language_hindi' ,'language_english' ,'language_other' ,'specialization_choice1' ,'specialization_choice2' ,'specialization_choice3' ,'applicant_photo' ,'dob_certificate' ,'id_proof' ,'declaration' ,'signatures','created_at','updated_at'
$columnsnewArray = array_diff($columnsArray, array('id', 'password', 'otp', 'user_id', 'email_verified', 'phone_verified', 'email_sent', 'admit_card', 'pdf_sent', 'created_at', 'updated_at', 'ccity', 'cstate', 'ccountry', 'declaration', 'updated_at'));
//formattedValue($val)
$columns = array_merge($columnsnewArray, $newArray);

//####### New Customised Query Dynamic Starts

$sqlquery = "SELECT users.id AS UserID, users.*, user_details.* FROM users";

$sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

$sqlquery = $sqlquery . " WHERE email_verified = 1";

foreach ($columns as $val) {
    $coreVal = 'txt_' . $val;
    $selVal = 'sel_' . $val;

    $coreValOther = 'txt_' . $val . "_1";

    if (isset($_GET[$coreVal]) != "" || isset($_GET[$coreValOther]) != "") {$coreVal;
        $selVal;
        $trimmedValue = trim($_GET[$coreVal]);

        if ($coreVal == "txt_created_at") {
            $trimmedValueNew = trim($_GET[$coreVal]);
            $explodetrimmedValue = explode("-", $trimmedValueNew);
            $trimmedValue = $explodetrimmedValue[0];

        }
        if ($coreValOther == 'txt_country_1' || $coreValOther == 'txt_state_1' || $coreValOther == 'txt_city_1') {
            $trimmedValue = trim($_GET[$coreValOther]);

        }

        $trimmedselValue = $_GET[$selVal];
        $colCheck = DB::columnList('users');
        if ($trimmedValue == "NULL") {
            if ($trimmedselValue == "") {
                $trimmedselValue = " is NULL";
            } else {
                $trimmedselValue = " is NOT NULL";
            }
            $specialVar = $trimmedselValue;

        } else {

            $specialVar = $trimmedselValue . " like   '%" . $trimmedValue . "%'";
        }

        //Now check if field exist in users array else from user_details change the query accordingly
        if (in_array($val, $colCheck)) {
            $sqlquery = $sqlquery . " AND users.`$val` " . $specialVar;
        } else {
            //$sqlquery   =    $sqlquery . " AND user_details.`$val` ".$trimmedselValue." like   '%".$trimmedValue."%' ";
            $sqlquery = $sqlquery . " AND user_details.`$val` " . $specialVar;
        }

    }
}

####### Condition in Query Added for Years Filter #######

$sqlquery = $sqlquery . " ORDER BY users.id DESC";

// echo $sqlquery; die;

$rsdata = DB::query($sqlquery);

//   var_dump($rsdata);  die;

//$rsdata = DB::query($sql);
$x = 2;
foreach ($rsdata as $key => $row) {

    $first_name = strtoupper($row['first_name']);
    $middle_name = strtoupper($row['middle_name']);
    $last_name = strtoupper($row['last_name']);
    $email = strtoupper($row['email']);
    $phone = strtoupper($row['phone']);
    $enroll_id = $row['enroll_id'];
    $email_verified = $row['email_verified'];
    $payment_status = strtoupper($row['payment_status']);
    $txnid = $row['txnid'];
    $Programme = strtoupper($row['Programme']);
    $dob = $row['dob'];
    $gender = strtoupper($row['gender']);
    $marital_status = strtoupper($row['marital_status']);

    $nationality = strtoupper($row['nationality']);

    $fathers_name = strtoupper($row['fathers_name']);
    $mothers_name = strtoupper($row['mothers_name']);
    $phone_father = strtoupper($row['phone_father']);
    $email_father = strtoupper($row['email_father']);
    $guardians_name = strtoupper($row['guardians_name']);
    $guardians_relation = strtoupper($row['guardians_relation']);
    $phone_guardian = strtoupper($row['phone_guardian']);
    $email_guardian = strtoupper($row['email_guardian']);
    $craft_relation = strtoupper($row['craft_relation']);
    $family_income = strtoupper($row['family_income']);
    $medical_info = strtoupper($row['medical_info']);
    $address_line1 = strtoupper($row['address_line1']);
    $address_line2 = strtoupper($row['address_line2']);
    $city = strtoupper(cityNameById($row['city']));
    $state = strtoupper(stateNameById($row['state']));
    $country = strtoupper(countryNameById($row['country']));
    $pin_code = strtoupper($row['pin_code']);
    $exam_center1 = strtoupper($row['exam_center1']);
    $exam_center2 = strtoupper($row['exam_center2']);
    $exam_center3 = strtoupper($row['exam_center3']);
    $twelft_pass_year = strtoupper($row['twelft_pass_year']);
    $twelft_stream = strtoupper($row['twelft_stream']);
    $twelft_other_stream = strtoupper($row['twelft_other_stream']);
    $twelft_board_name = strtoupper($row['twelft_board_name']);
    $twelft_grade = strtoupper($row['twelft_grade']);
    $degree_pass_year = strtoupper($row['degree_pass_year']);
    $degree_stream = strtoupper($row['degree_stream']);
    $degree_other_stream = strtoupper($row['degree_other_stream']);
    $degree_col_univ = strtoupper($row['degree_col_univ']);
    $degree_grade = strtoupper($row['degree_grade']);
    $language_hindi = strtoupper($row['language_hindi']);
    $language_english = strtoupper($row['language_english']);
    $language_other = strtoupper($row['language_other']);
    $specialization_choice1 = strtoupper($row['specialization_choice1']);
    $specialization_choice2 = strtoupper($row['specialization_choice2']);
    $specialization_choice3 = strtoupper($row['specialization_choice3']);
    $applicant_photo = $row['applicant_photo'];
    $dob_certificate = $row['dob_certificate'];
    $id_proof = $row['id_proof'];
    $declaration = strtoupper($row['declaration']);
    $payment_status = !empty($row['payment_status']) ? $row['payment_status'] : 'NOT DONE';
    $updated_at = $row['updated_at'];
    $status = $row['status'] == 1 ? 'APPROVED' : 'PENDING';

    //####### New Fields Added After Recent Changes    @@17112018
    $created_at = $row['created_at'];
    $signatures = $row['signatures'];
    $caddress_line1 = $row['caddress_line1'];
    $caddress_line2 = $row['caddress_line2'];

    $ccity = strtoupper(cityNameById($row['ccity']));
    $cstate = strtoupper(stateNameById($row['cstate']));
    $ccountry = strtoupper(countryNameById($row['ccountry']));

    $cpin_code = $row['cpin_code'];
    $email_mother = $row['email_mother'];
    $phone_mother = $row['phone_mother'];
    $twelft_school_address = $row['twelft_school_address'];

    //####### New Fields Added After Recent Changes 17112018
    $bhamashah = $row['bhamashah'];
    $alternate_email = $row['alternate_email'];
    $religion = $row['religion'];
    $other_religion = $row['other_religion'];
    $minority = $row['minority'];
    $bpl_apl = $row['bpl_apl'];
    $emergency_phone_no = $row['emergency_phone_no'];
    $caste_category = $row['caste_category'];
    $other_category = $row['other_category'];
    $students_area = $row['students_area'];

    $cblock_or_tehsil = $row['cblock_or_tehsil'];
    $identity_card = $row['identity_card'];
    $identity_card_no = $row['identity_card_no'];
    $id_proof_front = $row['id_proof_front'];
    $id_proof_back = $row['id_proof_back'];

    $block_or_tehsil = $row['block_or_tehsil'];

    $objPHPExcel->getActiveSheet()->getRowDimension($key + 2)->setRowHeight(40);

    $objPHPExcel->getActiveSheet()
        ->getStyle('A' . $x . ':CC' . $x)
        ->getAlignment()
        ->setWrapText(true);

    $objPHPExcel->getActiveSheet()->setCellValue('A' . $x, "$i")
        ->setCellValue('B' . $x, "$first_name")
        ->setCellValue('C' . $x, "$middle_name")
        ->setCellValue('D' . $x, "$last_name")
        ->setCellValue('E' . $x, "$email")
        ->setCellValue('F' . $x, "$phone")
        ->setCellValue('G' . $x, "$enroll_id")
        ->setCellValue('H' . $x, "$email_verified")
        ->setCellValue('I' . $x, "$payment_status")
        ->setCellValue('J' . $x, "$txnid")
        ->setCellValue('K' . $x, "$Programme")
        ->setCellValue('L' . $x, "$dob")
        ->setCellValue('M' . $x, "$gender")
        ->setCellValue('N' . $x, "$marital_status")

        ->setCellValue('O' . $x, "$nationality")

        ->setCellValue('P' . $x, "$fathers_name")
        ->setCellValue('Q' . $x, "$mothers_name")
        ->setCellValue('R' . $x, "$phone_father")
        ->setCellValue('S' . $x, "$email_father")
        ->setCellValue('T' . $x, "$guardians_name")
        ->setCellValue('U' . $x, "$guardians_relation")
        ->setCellValue('V' . $x, "$phone_guardian")
        ->setCellValue('W' . $x, "$email_guardian")
        ->setCellValue('X' . $x, "$craft_relation")
        ->setCellValue('Y' . $x, "$family_income")
        ->setCellValue('Z' . $x, "$medical_info")
        ->setCellValue('AA' . $x, "$address_line1")

        ->setCellValue('AB' . $x, "$block_or_tehsil")

        ->setCellValue('AC' . $x, "$city")
        ->setCellValue('AD' . $x, "$state")
        ->setCellValue('AE' . $x, "$country")
        ->setCellValue('AF' . $x, "$pin_code")
        ->setCellValue('AG' . $x, "$exam_center1")
        ->setCellValue('AH' . $x, "$exam_center2")
        ->setCellValue('AI' . $x, "$exam_center3")
        ->setCellValue('AJ' . $x, "$twelft_pass_year")
        ->setCellValue('AK' . $x, "$twelft_stream")
        ->setCellValue('AL' . $x, "$twelft_other_stream")
        ->setCellValue('AM' . $x, "$twelft_board_name")
        ->setCellValue('AN' . $x, "$twelft_grade")
        ->setCellValue('AO' . $x, "$degree_pass_year")
        ->setCellValue('AP' . $x, "$degree_stream")
        ->setCellValue('AQ' . $x, "$degree_other_stream")
        ->setCellValue('AR' . $x, "$degree_col_univ")
        ->setCellValue('AS' . $x, "$degree_grade")
        ->setCellValue('AT' . $x, "$language_hindi")
        ->setCellValue('AU' . $x, "$language_english")
        ->setCellValue('AV' . $x, "$language_other")
        ->setCellValue('AW' . $x, "$specialization_choice1")
        ->setCellValue('AX' . $x, "$specialization_choice2")
        ->setCellValue('AY' . $x, "$specialization_choice3")
        ->setCellValue('AZ' . $x, "$applicant_photo")
        ->setCellValue('BA' . $x, "$identity_card")
        ->setCellValue('BB' . $x, "$identity_card_no")
        ->setCellValue('BC' . $x, "$id_proof_front")
        ->setCellValue('BD' . $x, "$id_proof_back")

        ->setCellValue('BE' . $x, "$declaration")
        ->setCellValue('BF' . $x, "$payment_status")
        ->setCellValue('BG' . $x, "$updated_at")
        ->setCellValue('BH' . $x, "$status")

    //#### New Fields Values
        ->setCellValue('BI' . $x, "$created_at")
        ->setCellValue('BJ' . $x, "$signatures")
        ->setCellValue('BK' . $x, "$caddress_line1")

        ->setCellValue('BL' . $x, "$cblock_or_tehsil")

        ->setCellValue('BM' . $x, "$ccity")
        ->setCellValue('BN' . $x, "$cstate")
        ->setCellValue('BO' . $x, "$ccountry")
        ->setCellValue('BP' . $x, "$cpin_address")
        ->setCellValue('BQ' . $x, "$email_mother")
        ->setCellValue('BR' . $x, "$phone_mother")
        ->setCellValue('BS' . $x, "$twelft_school_address")
    //####### New Fields Added After Recent Changes 17112018
        ->setCellValue('BT' . $x, "$bhamashah")
        ->setCellValue('BU' . $x, "$alternate_email")
        ->setCellValue('BV' . $x, "$religion")
        ->setCellValue('BW' . $x, "$other_religion")
        ->setCellValue('BX' . $x, "$minority")
        ->setCellValue('BY' . $x, "$bpl_apl")
        ->setCellValue('BZ' . $x, "$emergency_phone_no")
        ->setCellValue('CA' . $x, "$caste_category")
        ->setCellValue('CB' . $x, "$other_category")
        ->setCellValue('CC' . $x, "$students_area");

    $i++;

    $x++;
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Save Excel 2007 file
//cho date('H:i:s') , " Write to Excel2007 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('userdata.xls');

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=userdata.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
ob_clean();
$objWriter->save('php://output');
