<?php
require_once '../meekrodb.2.3.class.php';

if (!(isset($_GET['pagenum']))) {
    $pagenum = 1;
} else {
    $pagenum = intval($_GET['pagenum']);
}
$page_limit = ($_GET["show"] != "" && is_numeric($_GET["show"])) ? intval($_GET["show"]) : 5;

$sqlquery = "SELECT users.id AS UserID, users.*, user_details.* FROM users";

$sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

$sqlquery = $sqlquery . " WHERE email_verified = 1 and signup_tracking = 9 and payment_status='success'";

####### Condition in Query Added for Dynamic Multilavel Filter #######
$columns_users = DB::columnList('users');
$columns_userdetails = DB::columnList('user_details');
$columnsArray = array_merge($columns_users, $columns_userdetails);
$newArray = array("created_at");
//'first_name','middle_name','last_name','email','enroll_id','created_at','updated_at','Programme','dob','gender','phone','marital_status','category','nationality','domicile' ,'fathers_name','mothers_name','phone_mother','email_mother','craft_name','phone_father','email_father',' guardians_name' ,'guardians_relation' ,'phone_guardian' ,'email_guardian' ,'craft_relation' ,'family_income' ,'medical_info' ,'address_line1' ,'address_line2' ,'city' ,'state' ,'country' ,'pin_code' ,'caddress_line1' ,'caddress_line2' ,'ccity' ,'cstate' ,'ccountry' ,'cpin_code' ,'exam_center1' ,'exam_center2' ,'exam_center3'<option value="twelft_pass_year">twelft_pass_year' ,'twelft_stream' ,'twelft_other_stream' ,'twelft_board_name' ,'twelft_school_name' ,'twelft_school_address' ,'degree_col_name' ,'degree_col_address' ,'twelft_grade' ,'degree_pass_year' ,'degree_stream' ,'degree_other_stream' ,'degree_col_univ' ,'degree_grade' ,'language_hindi' ,'language_english' ,'language_other' ,'specialization_choice1' ,'specialization_choice2' ,'specialization_choice3' ,'applicant_photo' ,'dob_certificate' ,'id_proof' ,'declaration' ,'signatures','created_at','updated_at'
$columnsnewArray = array_diff($columnsArray, array('id', 'password', 'otp', 'user_id', 'email_verified', 'phone_verified', 'email_sent', 'admit_card', 'pdf_sent', 'created_at', 'updated_at', 'ccity', 'cstate', 'ccountry', 'declaration', 'updated_at', 'phase1_attendence', 'phase1_result', 'phase2_attendence', 'phase2_result', 'phase3_attendence', 'phase3_result', 'address_line2', 'caddress_line2', 'addresscheck', 'id_proof', 'category', 'domicile', 'village_name', 'cvillage_name', 'mail_count', 'disclaimer_check'));
//formattedValue($val)
$columns = array_merge($columnsnewArray, $newArray);

foreach ($columns as $val) {
    $coreVal = 'txt_' . $val;
    $selVal = 'sel_' . $val;

    $coreValOther = 'txt_' . $val . "_1";

    if ($_GET[$coreVal] != "" || $_GET[$coreValOther] != "") {$coreVal;
        $selVal;
        $trimmedValue = trim($_GET[$coreVal]);
        // echo $trimmedValue; die;
        if ($coreVal == "txt_created_at") {
            $trimmedValueNew = trim($_GET[$coreVal]);
            $explodetrimmedValue = explode("-", $trimmedValueNew);
            $trimmedValue = $explodetrimmedValue[0];
            // echo $trimmedValue; die;
        }
        if ($coreValOther == 'txt_country_1' || $coreValOther == 'txt_state_1' || $coreValOther == 'txt_city_1') {
            $trimmedValue = trim($_GET[$coreValOther]);
            //echo $trimmedValue; die;
        }

        $trimmedselValue = $_GET[$selVal];
        $colCheck = DB::columnList('users');
        if ($trimmedValue == "NULL") {
            if ($trimmedselValue == "") {
                $trimmedselValue = " is NULL";
            } else {
                $trimmedselValue = " is NOT NULL";
            }
            $specialVar = $trimmedselValue;

        } else {
            //New Changes after Gender issues coming putting again in if elese
            if (strtolower($trimmedValue) == "male" || strtolower($trimmedValue) == "female") {
                $specialVar = $trimmedselValue . " = '" . $trimmedValue . "'";
            } else {
                $specialVar = $trimmedselValue . " like   '%" . $trimmedValue . "%'";
            }
            //New Changes after Gender issues coming putting again in if elese
        }

        //Now check if field exist in users array else from user_details change the query accordingly
        if (in_array($val, $colCheck)) {
            $sqlquery = $sqlquery . " AND users.`$val` " . $specialVar;
        } else {
            //$sqlquery   =    $sqlquery . " AND user_details.`$val` ".$trimmedselValue." like   '%".$trimmedValue."%' ";
            $sqlquery = $sqlquery . " AND user_details.`$val` " . $specialVar;
        }

    }
}

####### Condition in Query Added for Years Filter #######

$sqlquery = $sqlquery . " ORDER BY users.id DESC";

$sqlquery = DB::query($sqlquery);

$cntNew = DB::count();

$last = ceil($cntNew / $page_limit);
if ($pagenum < 1) {
    $pagenum = 1;
} elseif ($pagenum > $last) {
    $pagenum = $last;
}

if ($pagenum == 0) {
    $pagenum = 1;
}
global $pagenum;
$lower_limit = ($pagenum - 1) * $page_limit;

$sqlquery = "SELECT users.id AS UserID, users.*, user_details.* FROM users";

$sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";

//currently removing needs discussion  from following query

$sqlquery = $sqlquery . " WHERE email_verified = 1 and  signup_tracking = 9 and payment_status='success'";

foreach ($columns as $val) {
    $coreVal = 'txt_' . $val;
    $selVal = 'sel_' . $val;

    $coreValOther = 'txt_' . $val . "_1";

    if ($_GET[$coreVal] != "" || $_GET[$coreValOther] != "") {$coreVal;
        $selVal;
        $trimmedValue = trim($_GET[$coreVal]);
        // echo $trimmedValue; die;
        if ($coreVal == "txt_created_at") {
            $trimmedValueNew = trim($_GET[$coreVal]);
            $explodetrimmedValue = explode("-", $trimmedValueNew);
            $trimmedValue = $explodetrimmedValue[0];
            // echo $trimmedValue; die;
        }
        if ($coreValOther == 'txt_country_1' || $coreValOther == 'txt_state_1' || $coreValOther == 'txt_city_1') {
            $trimmedValue = trim($_GET[$coreValOther]);
            //echo $trimmedValue; die;
        }

        $trimmedselValue = $_GET[$selVal];
        $colCheck = DB::columnList('users');
        if ($trimmedValue == "NULL") {
            if ($trimmedselValue == "") {
                $trimmedselValue = " is NULL";
            } else {
                $trimmedselValue = " is NOT NULL";
            }
            $specialVar = $trimmedselValue;

        } else {

            //New Changes after Gender issues coming putting again in if elese
            if (strtolower($trimmedValue) == "male" || strtolower($trimmedValue) == "female") {
                $specialVar = $trimmedselValue . " = '" . $trimmedValue . "'";
            } else {
                $specialVar = $trimmedselValue . " like   '%" . $trimmedValue . "%'";
            }
            //New Changes after Gender issues coming putting again in if elese

        }

        //Now check if field exist in users array else from user_details change the query accordingly
        if (in_array($val, $colCheck)) {
            $sqlquery = $sqlquery . " AND users.`$val` " . $specialVar;
        } else {
            //$sqlquery   =    $sqlquery . " AND user_details.`$val` ".$trimmedselValue." like   '%".$trimmedValue."%' ";
            $sqlquery = $sqlquery . " AND user_details.`$val` " . $specialVar;
        }

    }
}

####### Condition in Query Added for Years Filter #######

$sqlquery = $sqlquery . " ORDER BY users.id DESC LIMIT $lower_limit, $page_limit";

// echo $sqlquery;

$sqlquery = DB::query($sqlquery);

$cnt = DB::count();

$cnt;
//limit ". ($lower_limit)." ,  ". ($page_limit). "
if ($cntNew == 0) {
    ?>
<table class="table table-striped table-bordered table-vcenter">
   <tr>
      <td align="center">No Record Found</td>
   </tr>
</table>
<?php } else {

    ?>
    <h4>Total Records : <?=$cntNew;?></h4>
<table class="table table-striped table-bordered table-vcenter">
   <thead>
      <tr>
         <th class="text-center" style="width: 50px;"></th>
         <th class="text-center" style="width: 50px;">S.no</th>
         <th>Name</th>
         <th>Email</th>
         <th>Photo</th>
         <th>Singnature</th>
         <th colspan="2" style="text-align: center">ID Proof</th>

         <th>Mail (Count)</th>

         <th class="text-center" style="width: 100px;"><i class="fa fa-flash"></i></th>
      </tr>
   </thead>
   <tbody class="mytabledata">
      <?php
$intcnt = $lower_limit;
    foreach ($sqlquery as $value) {
        $intcnt = $intcnt + 1;
        $idtoPreview = $value['UserID'];

        $identity_card = $value['identity_card'];
        $identity_card_no = $value['identity_card_no'];
        ?>
      <tr>
        <td class="text-center">

            <input type="checkbox" name="chk[]" class="checkBoxClass" id="checkbox<?=$idtoPreview;?>" value="<?php echo base64_encode($idtoPreview); ?>"/>


        </td>
         <td class="text-center"><?php echo $intcnt; ?></td>
         <td><?php echo ucwords(strtolower(($value['first_name'] . ' ' . $value['middle_name'] . ' ' . $value['last_name']))); ?></td>
         <td><?php echo $value['email']; ?></td>
         <td><img src="../images/<?php echo $value['applicant_photo']; ?>" width="100" height="100" title="Applicant Photo" alt="Applicant Photo" /></td>
         <td><img src="../images/<?php echo $value['signatures']; ?>" width="100" height="100" title="Applicant Signature" alt="Applicant Signature" /> </td>
         <td><img src="../images/<?php echo $value['id_proof_front']; ?>" width="100" height="100" title="ID Proof Front" alt="ID Proof Front" />
        <?php

        echo '<span title="Active Exam Center" class="btn btn-effect-ripple btn-xs btn-info clearfix" style="overflow: hidden; position: relative;">' . $identity_card . '<br/> Front View</span>';

        ?>

         </td>

         <td><img src="../images/<?php echo $value['id_proof_back']; ?>" width="100" height="100" title="ID Proof Back" alt="ID Proof Back" />
        <?php

        echo '<span title="Active Exam Center" class="btn btn-effect-ripple btn-xs btn-info clearfix" style="overflow: hidden; position: relative;">' . $identity_card . '<br/> Back View</span>';

        ?>
        </td>
        <td><?php $mailcount = $value['mail_count'];if ($mailcount > 0) {echo '<span title="Mail Sent Status" class="btn btn-effect-ripple btn-xs btn-success clearfix" style="overflow: hidden; position: relative;">Mail Sent </span> <span title="No. of Times Mail Sent" class="btn btn-effect-ripple btn-xs btn-info clearfix" style="overflow: hidden; position: relative;"> ' . $mailcount . '</span>';} else {echo '<span title="Mail Sent Status" class="btn btn-effect-ripple btn-xs btn-danger clearfix" style="overflow: hidden; position: relative;">No Mail Sent - ' . $mailcount . '</span>';}?></td>
         <td class="text-center">

            <a data-toggle="modal" data-target="#myModal<?=$idtoPreview;?>" title="Send Mail" class="btn btn-effect-ripple btn-xs btn-default"><i class="fa fa-eye"></i></a>
            <form method="get" id="myform<?=$idtoPreview;?>" name="myform<?=$idtoPreview;?>">
            <!-- Modal -->
            <div id="myModal<?=$idtoPreview;?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
            <input type="hidden" name="mailingid" value="<?=$idtoPreview;?>"/>
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Email - Student <?php echo ucwords($value['first_name'] . ' ' . $value['middle_name'] . ' ' . $value['last_name']); ?></h4>
            </div>
            <div class="modal-body">

            <div class="col-md-12" id="resultSent<?=$idtoPreview;?>">

            </div>

            <div class="col-md-12">
            <div class="my-input-bx field required-field">
            <div class="selectContainer">
            <label class="my-label"> Select Template Type </label><span class="bar"></span>
            <select name="templateid" class="form-control" required="required">
            <option value="">Select Issue</option>
            <option value="18">Applicant Photo Issue</option>
            <option value="19">Applicant Signature Issue</option>
            <option value="20">Applicant ID Proof (Front/Back) Issue</option>
            </select>
            </div>
            </div>
            </div>
            </div>
            <div class="modal-footer" style="margin-top: 20px; overflow: hidden">
            <button type="button" class="btn btn-primary" onclick="getidofstudenttomail(<?=$idtoPreview;?>)">Send</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>

            </div>
            </div>
            </form>
         </td>
      </tr>
                <!--##### Jquery to Detect Changes in State of Checkbox #####-->
                <script>
                $('#checkbox<?=$idtoPreview;?>').change(function() {
                    if($(this).is(":checked")) {

                    }
                    if (!$(this).prop("checked")){
                    $("#ckbCheckAll").prop("checked",false);
                    }
                })
                </script>
                <!--##### Jquery to Detect Changes in State of Checkbox #####-->
      <?php
}
    ?>
   </tbody>
</table>
<div class="height30"></div>
<div class="col-md-6">
   <div  class=" panel-heading pull-left">


         <select name="show" onChange="changeDisplayRowCount(this.value);" class="form-control">
            <option value="50" <?php if ($_GET["show"] == 50 || $_GET["show"] == "") {echo ' selected="selected"';}?> >50 (Rows to Show)</option>
            <option value="100" <?php if ($_GET["show"] == 100) {echo ' selected="selected"';}?> >100 (Rows to Show)</option>
            <option value="150" <?php if ($_GET["show"] == 150) {echo ' selected="selected"';}?> >150 (Rows to Show)</option>
         </select>

   </div>
   <div class=" panel-heading pull-left">
      <h3   style="font-size: 15px; margin: 10px">Page <?php echo $pagenum; ?> of <?php echo $last; ?></h3>
   </div>
</div>
</div>
<ul class="pagination pull-right" >
   <?php
$pagenum; //current page
    $page_limit; //data to be displayed from select box
    $last; //total no of pages
    $startnew = $pagenum - 5; //for loop will start
    if ($startnew > 1) {
        $start = $startnew;
    }
    if ($startnew <= 1) {
        $start = 1;
    }

    $endnew = $pagenum + 3; // for loop will be  less than or equal to

    if (($last - $endnew) > 1) {
        $end = $endnew;
    }
    if (($last - $endnew) <= 1) {
        $end = $last;
    }

    if (($pagenum - 1) > 0) {
        ?>
   <li><a href="javascript:void(0);" class="links" onclick="displayRecords('<?php echo $page_limit; ?>', '<?php echo 1; ?>');">|<</a></li>
   <li><a href="javascript:void(0);" class="links"  onclick="displayRecords('<?php echo $page_limit; ?>', '<?php echo $pagenum - 1; ?>');"><<</a></li>
   <?php
}

    //here the dots will appear

    if (($pagenum - 3) > 1) {
        ?>
   <li><a href="javascript:void(0);">...</a></li>
   <?php
}

    //Show page links
    for ($i = $start; $i <= $end; $i++) {
        if ($i == $pagenum) {
            ?>
   <li><a href="javascript:void(0);" class="selected active" style="background: blue; color: white;"><?php echo $i ?></a></li>
   <?php
} else {
            ?>
   <li><a href="javascript:void(0);" class="links"  onclick="displayRecords('<?php echo $page_limit; ?>', '<?php echo $i; ?>');" ><?php echo $i ?></a></li>
   <?php
}
    }

    //if pages exists after loop's upper limit

    if (($last - ($pagenum + 5)) > 1) {
        ?>
   <li><a href="javascript:void(0);">...</a></li>
   <?php
}

    if (($pagenum + 1) <= $last) {
        ?>
   <li><a href="javascript:void(0);" onclick="displayRecords('<?php echo $page_limit; ?>', '<?php echo $pagenum + 1; ?>');" class="links">>></a></li>
   <?php }if (($pagenum) != $last) {?>
   <li><a href="javascript:void(0);" onclick="displayRecords('<?php echo $page_limit; ?>', '<?php echo $last; ?>');" class="links" >>|</a></li>
   <?php
}
    ?>
</ul>
</div>
</div>
</div>
</div>
<?php }?>

