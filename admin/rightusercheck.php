<?php
    session_start();
    include '../meekrodb.2.3.class.php';
    ini_set("session.gc_maxlifetime",3*60*60);
    ini_set("session.gc_probability",1);
    ini_set("session.gc_divisor",1);
    date_default_timezone_set('Asia/Kolkata');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');  
       
    if (!empty($_SESSION['adminyncrights']))
    {
        $_SESSION['adminyncvalid']    		= true;
        $_SESSION['adminyncusername'] 		= $_SESSION['adminyncusername'];      
    }
    else
    {
        $_SESSION['adminyncvalid'] = false;
        session_unset();
        session_destroy();
        header("Location: login.php");
    }
?>