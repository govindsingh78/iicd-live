<?php
	require_once('../meekrodb.2.3.class.php');
	require_once ('phpmailer/class.phpmailer.php');
	  $centers = DB::query("select * from exam_centers");
      //  PG18030012-001
      //  PG18031220-002
      //  UG18032113-001
      //  UG18030213-002
      //INTG18030203-001
      //INTG18030253-002

      /********************for UG******************/

		 $sqlquery   =    "SELECT users.id AS UserID,users.status, user_details.Programme, user_details.exam_center1,exam_centers.city,exam_centers.center_code FROM users";
		 $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
		 $sqlquery   =    $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city where user_details.Programme = 'UG' order by exam_centers.center_code";
		 //$sqlquery   =   $sqlquery . "where user_details.Programme = 'UG'";
 		 $sqlquery = DB::query($sqlquery);
 		 $counter = DB::count();

 		 $centerCodeSeq = '';
 		 $centerCodeVirtualId = 1;

 		 if ($counter > 0) 		 	
 		 foreach ($sqlquery as $key => $value) {
	 		 	if ($value['status'] == 1) {
	 		 		if( $centerCodeSeq != $value['center_code'] ){
	 		 			$centerCodeVirtualId = 1;
	 		 			$centerCodeSeq = $value['center_code'];
	 		 		}
	 		 		    $center_code =  $value['center_code'];
			 		 	$programme =  $value['Programme'];
			 		 	$id =  $value['UserID'];
			 		 	$user_id= str_pad($id, 4, '0', STR_PAD_LEFT);
			 		 	$year = '18';
			 		 	$centerCodeVirtualId_Formatted = str_pad($centerCodeVirtualId, 3, '0', STR_PAD_LEFT);
	 		 	 	    $enrollmentUG = $programme.$year.$center_code.$user_id.'-'.$centerCodeVirtualId_Formatted; //echo "<br>";
			 		    $update = DB::update('users', array('enroll_id' => $enrollmentUG), "id=%i", $id);

	 		 	    $centerCodeVirtualId++;
	 		 }
 		 }

 		 //echo '<pre>';
 		 //print_r($enrollmentUG);

 		 /********************for PG******************/

 		 $sqlquery   =    "SELECT users.id AS UserID,users.status, user_details.Programme, user_details.exam_center1,exam_centers.city,exam_centers.center_code FROM users";
		 $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
		 $sqlquery   =    $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city where user_details.Programme = 'PG' order by exam_centers.center_code";
		 //$sqlquery   =   $sqlquery . "where user_details.Programme = 'UG'";
 		 $sqlquery = DB::query($sqlquery);
 		 $counter = DB::count();

 		 $centerCodeSeq = '';
 		 $centerCodeVirtualId = 1;

 		 if ($counter > 0) 		 	
 		 foreach ($sqlquery as $key => $value) {
	 		 	if ($value['status'] == 1) {
	 		 		if( $centerCodeSeq != $value['center_code'] ){
	 		 			$centerCodeVirtualId = 1;
	 		 			$centerCodeSeq = $value['center_code'];
	 		 		}
	 		 		    $center_code =  $value['center_code'];
			 		 	$programme =  $value['Programme'];
			 		 	$id =  $value['UserID'];
			 		 	$user_id= str_pad($id, 4, '0', STR_PAD_LEFT);
			 		 	$year = '18';
			 		 	$centerCodeVirtualId_Formatted = str_pad($centerCodeVirtualId, 3, '0', STR_PAD_LEFT);
	 		 	 	    $enrollmentPG = $programme.$year.$center_code.$user_id.'-'.$centerCodeVirtualId_Formatted; 
	 		 	 	    //echo "<br>";
			 		    $update = DB::update('users', array('enroll_id' => $enrollmentPG), "id=%i", $id);

	 		 	    $centerCodeVirtualId++;
	 		 }
 		 }

 		 //echo '<pre>';
 		 //print_r($enrollmentPG);

 		 /********************for INTG******************/

 		 $sqlquery   =    "SELECT users.id AS UserID,users.status, user_details.Programme, user_details.exam_center1,exam_centers.city,exam_centers.center_code FROM users";
		 $sqlquery   =    $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
		 $sqlquery   =    $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city where user_details.Programme = 'INTG' order by exam_centers.center_code";
		 //$sqlquery   =   $sqlquery . "where user_details.Programme = 'UG'";
 		 $sqlquery = DB::query($sqlquery);
 		 $counter = DB::count();

 		 $centerCodeSeq = '';
 		 $centerCodeVirtualId = 1;

 		 if ($counter > 0) 		 	
 		 foreach ($sqlquery as $key => $value) {
	 		 	if ($value['status'] == 1) {
	 		 		if( $centerCodeSeq != $value['center_code'] ){
	 		 			$centerCodeVirtualId = 1;
	 		 			$centerCodeSeq = $value['center_code'];
	 		 		}
	 		 		    $center_code =  $value['center_code'];
			 		 	$programme =  $value['Programme'];
			 		 	$id =  $value['UserID'];
			 		 	$user_id= str_pad($id, 4, '0', STR_PAD_LEFT);
			 		 	$year = '18';
			 		 	$centerCodeVirtualId_Formatted = str_pad($centerCodeVirtualId, 3, '0', STR_PAD_LEFT);
	 		 	 	    $enrollmentINTG = $programme.$year.$center_code.$user_id.'-'.$centerCodeVirtualId_Formatted; //echo "<br>";
			 		    $update = DB::update('users', array('enroll_id' => $enrollmentINTG), "id=%i", $id);

	 		 	    $centerCodeVirtualId++;
	 		 }
 		 }

 		 //echo '<pre>';
 		 //print_r($enrollmentINTG);

 		// $enrollment = array_merge($enrollmentUG, $enrollmentPG, $enrollmentINTG);

 		// echo '<pre>';
 		// print_r($enrollment);
		
 		  

?>



