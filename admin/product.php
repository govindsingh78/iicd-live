<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('main.php');
    $DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); 
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
               	<?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Validation Header -->
                        <div class="content-header">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="header-section">
                                        <h1>Product Name</h1>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs">
                                    <div class="header-section">
                                        <ul class="breadcrumb breadcrumb-top">
                                            <li>Home</li>
                                            <li><a href="">Product Name</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Validation Header -->

                        <!-- Form Validation Content -->
                        <?php
			           		if(!empty($_GET['id'])) 
			                {
			                    $edit     	= (int)$_GET['edit'];
			                    $id       	= (int)$_GET['id'];
			                    $sqlquery = "SELECT tblproducts.* FROM tblproducts WHERE tblproducts.id = ".$id." order by id";
			                    $rsdata   = $DB ->getdata($sqlquery);   
			                    if (mysql_num_rows($rsdata) > 0) 
			                    {            
			                      while($rowdata = mysql_fetch_array($rsdata))
			                      {
			                        $catid		  	=   trim($rowdata['catid']);
			                        $producttype  	=   trim($rowdata['producttype']);
			                        $productname  	=   trim($rowdata['productname']);
			                        $productprice   =   (float)$rowdata['productprice'];
			                        $instock     	=   trim($rowdata['instock']);
			                        $description  	=   trim($rowdata['description']);
			                        $status     	=   trim($rowdata['status']);
			                        $sortorder  	=   trim($rowdata['sortorder']);
			                      }
			                    }
			                }
			                else
			                {
			                		$edit           =   0;
			                        $id             =   0;
			                        $catid			=   0;
			                        $producttype    =   1;
			                        $productname   	=   null;
			                        $productprice	=	null;
			                        $instock		=	null;
			                        $description	=	null;
			                        $status         =   707;
			                        $sortorder      =   1;
			                }
			        	?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <!-- Form Validation Block -->
                                <div class="block">
                                    <!-- Form Validation Title -->
                                    <div class="block-title">
                                        <h2>Product Name</h2>
                                    </div>
                                    <!-- END Form Validation Title -->

                                    <!-- Form Validation Form -->
                                    <form id="form-validation" action="productsave.php" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                                    	<input type="hidden" name="edit" id="edit" value="<?php echo $edit;?>" />
                                    	<input type="hidden" name="id" id="id" value="<?php echo $id;?>" /> 
                                    	<div class="form-group">
                                            <label class="col-md-3 control-label" for="categoryname">Category Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                            	<select name="catid" id="catid" class="form-control required">
                                            		<option value="">Please Select Category</option>
                                            		<?php
                                            			$sqlquery 	= 	"SELECT tblcategory.* FROM tblcategory WHERE status = 707 order by categoryname";
									                    $rscatdata	= 	$DB ->getdata($sqlquery);   
									                    if (mysql_num_rows($rscatdata) > 0) 
									                    {            
									                      while($rowcatdata = mysql_fetch_array($rscatdata))
									                      {
                                            		?>
                                            		<option value="<?php echo $rowcatdata['id']; ?>" <?php if((int)$rowcatdata['id'] == (int)$catid) { echo "selected='selected'"; } ?>><?php echo $rowcatdata['categoryname']; ?></option>
                                            		<?php
                                            				}
                                            			}
                                            		?>
                                            	</select>
                                            </div>
                                      	</div>
                                      	<!--<div class="form-group">
                                            <label class="col-md-3 control-label" for="subcategoryname">Subcategory Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6" id="divsubcat">
                                            	<select name="subcatid" id="subcatid" class="form-control required">
                                            		<option value="">Please Select Subcategory</option>
                                            		<?php
                                            			if (!empty($catid)) {
                                            			$sqlquery 	= 	"SELECT tblsubcategory.* FROM tblsubcategory";
                                            			$sqlquery 	=	$sqlquery ." WHERE tblsubcategory.catid = ".$catid." AND tblsubcategory.status = 707 order by subcategoryname";
									                    $rssubcatdata	= 	$DB ->getdata($sqlquery);   
									                    if (mysql_num_rows($rssubcatdata) > 0) 
									                    {            
									                      while($rowsubcatdata = mysql_fetch_array($rssubcatdata))
									                      {
                                            		?>
                                            		<option value="<?php echo $rowsubcatdata['id']; ?>" <?php if((int)$rowsubcatdata['id'] == (int)$subcatid) { echo "selected='selected'"; } ?>><?php echo $rowsubcatdata['subcategoryname']; ?></option>
                                            		<?php
                                            				}
                                            			}
                                            			}
                                            		?>
                                            	</select>
                                            </div>
                                      	</div>-->
                                      	<div class="form-group">
                                            <label class="col-md-3 control-label" for="productname">Product Type <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="radio" id="producttype" name="producttype" value="1" class="required" <?php if($producttype == 1){ echo 'checked'; }else{ echo ''; } ?>> Veg&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="radio" id="producttype" name="producttype" value="2" class="required" <?php if($producttype == 2){ echo 'checked'; }else{ echo ''; } ?>> Non-Veg
                                            </div>
                                        </div>
                                      	<div class="form-group">
                                            <label class="col-md-3 control-label" for="productname">Product Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="productname" name="productname" value="<?php echo $productname; ?>" class="form-control required" placeholder="Enter product name..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="productprice">Product Price <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="productprice" name="productprice" value="<?php echo $productprice; ?>" class="form-control required" placeholder="Enter product price..">
                                                <!--<span style="color: #ff0000;">(Price per piece, per kg, per grams, per liter, per ml)</span>-->
                                            </div>
                                        </div>
                                      	<div class="form-group">
                                            <label class="col-md-3 control-label" for="instock">Instock <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" id="instock" name="instock" value="<?php echo $instock; ?>" class="form-control required" placeholder="Enter instock product.." style="width: 65%; display: inline !important;margin-right: 2%;">
                                          	</div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="instock">Product Upload Image <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
      	                                          <input type="file" id="bigimage" name="bigimage" class="form-control" style="padding-top: 0 !important;">
      	                                          <span style="color: #ff0000;">(Best upload size 1000px * 1000px )</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
	                                        <label class="col-md-3 control-label">Description</label>	
	                                        <div class="col-md-9">
	                                            <textarea id="textarea-ckeditor" name="description" class="ckeditor"><?php echo $description; ?></textarea>
	                                        </div>
                                    	</div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="val-skill">Status <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <select id="val-skill" name="status" class="form-control">
                                                    <option value="707" <?php if ((int) $status == 707) { echo "selected"; } ?>>Enable</option>
				                            		<option value="505" <?php if ((int) $status == 505) { echo "selected"; } ?>>Disable</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Sort Order</label>
                                            <div class="col-md-6">
                                                <input type="text" id="sortorder" name="sortorder" value="<?php if (isset($sortorder)) { echo $sortorder; } else { echo "1"; } ?>" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-3">
                                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Form -->
                                </div>
                                <!-- END Form Validation Block -->
                            </div>
                        </div>
                        <!-- END Form Validation Content -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
		<script src="js/plugins/ckeditor/ckeditor.js"></script>
        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/formsValidation.js"></script>
        <script>$(function(){ FormsValidation.init(); });</script>
        
	
    </body>
</html>
<?php $DB -> close(); ?>