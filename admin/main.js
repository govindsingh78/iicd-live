$(document).ready(function() {
  //toggle `popup` / `inline` mode
  $.fn.editable.defaults.mode = "popup";

  //make username editable
  $(".xfirst_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "First name is required";
      }
    }
  });

  $(".xmiddle_name").editable();

  $(".xlast_name").editable();

  $(".xemail").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Email is required";
      }
      //var regex = /^[0-9]+$/;
      var regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      if (!regex.test(value)) {
        return "Email only!";
      }
    }
  });

  $(".xProgramme").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Programme" },
      { value: "UG", text: "UG" },
      { value: "PG", text: "PG" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Programme is required";
      }
    }
  });

  $(".xdob").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "DOB is required";
      }
    }
  });

  $(".xgender").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select gender" },
      { value: "Male", text: "Male" },
      { value: "Female", text: "Female" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Gender is required";
      }
    }
  });

  $(".xmstatus").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Marital Status" },
      { value: "Married", text: "Married" },
      { value: "Single", text: "Single" },
      { value: "Separated", text: "Separated" },
      { value: "Widowed", text: "Widowed" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Marital Status is required";
      }
    }
  });

  $(".xcategory").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Category" },
      { value: "Genral", text: "Genral" },
      { value: "OBC", text: "OBC" },
      { value: "SCST", text: "SC / ST" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Category is required";
      }
    }
  });

  $(".xnationality").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Nationality" },
      { value: "Indian", text: "Indian" },
      { value: "Other", text: "Other" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Nationality is required";
      }
    }
  });

  $(".xdomicile").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select one" },
      { value: "Yes", text: "Yes" },
      { value: "No", text: "No" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Domicile is required";
      }
    }
  });

  $(".xphone").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Phone is required";
      }
      var regex = /^\d{10}$/;
      if (!regex.test(value)) {
        return "Please insert a valid phone number !";
      }
    }
  });

  $(".xfathers_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Fathers Name is required";
      }
    }
  });

  $(".xmothers_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Mothers Name is required";
      }
    }
  });

  // ###### -   New Field Validation from Editable - ######

  $(".xbhamashah").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Bhamashah ID is required";
      }

      var regex = /^[a-zA-Z0-9]+$/;
      if (!regex.test(value)) {
        return "Please insert a valid Bhamashah ID !";
      }
    }
  });

  $(".xalternate_email").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Alternate email ID is required";
      }

      var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!regex.test(value)) {
        return "Please insert a valid Email!";
      }
    }
  });

  $(".xreligion").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Religion" },
      { value: "Hindu", text: "Hindu" },
      { value: "Muslim", text: "Muslim" },
      { value: "Sikh", text: "Sikh" },
      { value: "Jain", text: "Jain" },
      { value: "Other", text: "Other" }
    ],

    validate: function(value) {
      if ($.trim(value) == "") {
        return "Religion is required";
      }
    }
  });

  $(".xother_religion").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Provide Other Religion if Apllicable";
      }
    }
  });

  $(".xminority").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Minority" },
      { value: "Yes", text: "Yes" },
      { value: "No", text: "No" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Minority is required";
      }
    }
  });

  $(".xbpl_apl").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select if student belongs to APL/BPL" },
      { value: "Yes", text: "Yes" },
      { value: "No", text: "No" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Select if student belongs to APL/BPL";
      }
    }
  });

  $(".xemergency_phone_no").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Emergency Phone No. is required";
      }
    }
  });

  $(".xcaste_category").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Caste Category" },
      { value: "General", text: "General" },
      { value: "SC", text: "SC" },
      { value: "ST", text: "ST" },
      { value: "BC- Non Creamy Layer", text: "BC- Non Creamy Layer" },
      { value: "MBC- Non Creamy Layer", text: "MBC- Non Creamy Layer" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Caste Category is required";
      }
    }
  });

  $(".xother_category").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Other Category" },
      { value: "Kashmiri Migrants (KM)", text: "Kashmiri Migrants (KM)" },
      {
        value: "Differently/Specially Abled",
        text: "Differently/Specially Abled"
      },
      { value: "None", text: "None" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Other Category is required";
      }
    }
  });

  $(".xstudents_area").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Students belong to Area" },
      { value: "Urban", text: "Urban" },
      { value: "Rural", text: "Rural" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Students belongs to Area is required";
      }
    }
  });

  // ###### -   New Field Validation from Editable - ######

  $(".xphone_father").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Father Phone no. is required";
      }
      var regex = /^\d{10}$/;
      if (!regex.test(value)) {
        return "Please insert a valid phone number !";
      }
    }
  });

  $(".xemail_father").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Father Email is required";
      }
      var regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      if (!regex.test(value)) {
        return "Email only!";
      }
    }
  });

  $(".xphone_mother").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Mother Phone no. is required";
      }
      var regex = /^\d{10}$/;
      if (!regex.test(value)) {
        return "Please insert a valid phone number !";
      }
    }
  });

  $(".xemail_mother").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Mother Email is required";
      }
      var regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      if (!regex.test(value)) {
        return "Email only!";
      }
    }
  });

  $(".xcraft_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Craft Name is required";
      }
    }
  });

  $(".xguardians_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Guardian name is required";
      }
    }
  });

  $(".xguardians_relation").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Guardian relation field is required";
      }
    }
  });

  $(".xphone_guardian").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Guardian phone no. is required";
      }
      var regex = /^\d{10}$/;
      if (!regex.test(value)) {
        return "Please insert a valid phone number !";
      }
    }
  });

  $(".xemail_guardian").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Guardian email is required";
      }
      var regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
      if (!regex.test(value)) {
        return "Email only!";
      }
    }
  });

  $(".xcraft_relation").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select one" },
      { value: "No", text: "No" },
      { value: "Yes", text: "Yes" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Craft relation field is required";
      }
    }
  });

  $(".xfamily_income").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select Family Income" },
      { value: "Upto Rs.5,00,000", text: "Upto Rs.5,00,000" },
      {
        value: "Rs.5,00,001 to Rs.10,00,000",
        text: "Rs.5,00,001 to Rs.10,00,000"
      },
      { value: "Above Rs.10,00,000", text: "Above Rs.10,00,000" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Family Income is required";
      }
    }
  });

  $(".xmedical_info").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Medical Info is required";
      }
    }
  });

  $(".xaddress_line1").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Address line1 is required";
      }
    }
  });

  $(".xvillage_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Village Name is required";
      }
    }
  });
  $(".xblock_or_tehsil").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Block/Tehsil is required";
      }
    }
  });

  $(".xcountry").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Country name is required";
      }
    }
  });

  $(".xstate").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "State name is required";
      }
    }
  });

  $(".xcity").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "City name is required";
      }
    }
  });

  $(".xpin_code").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Pin code is required";
      }
      var regex = /^[0-9]+$/;
      if (!regex.test(value)) {
        return "Please Insert a valid PIN no.!";
      }
    }
  });

  $(".xidentity_card").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select ID Type" },
      { value: "UID Aadhar", text: "UID Aadhar" },
      { value: "Driver's License", text: "Driver's License" },
      { value: "Passport", text: "Passport" },
      { value: "Voter ID", text: "Voter ID" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "ID Card Type is required";
      }
    }
  });

  $(".xidentity_card_no").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "ID Card No. is required";
      }
    }
  });

  $(".xexam_center1").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "exam center is required";
      }
    }
  });

  $(".xexam_center2").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "exam center is required";
      }
    }
  });

  $(".xexam_center3").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "exam center is required";
      }
    }
  });

  $(".xtwelft_pass_year").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft pass year is required";
      }
    }
  });

  $(".xtwelft_stream").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft stream is required";
      }
    }
  });

  $(".xtwelft_other_stream").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft other stream is required";
      }
    }
  });

  $(".xtwelft_board_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft board name is required";
      }
    }
  });

  $(".xtwelft_school_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft school name is required";
      }
    }
  });

  $(".xtwelft_school_address").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft school address is required";
      }
    }
  });

  $(".xtwelft_grade").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Twelft grade is required";
      }
    }
  });

  $(".xdegree_pass_year").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree pass year is required";
      }
    }
  });

  $(".xdegree_stream").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree stream is required";
      }
    }
  });

  $(".xdegree_other_stream").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree other stream is required";
      }
    }
  });

  $(".xdegree_col_univ").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree college university name is required";
      }
    }
  });
  $(".xdegree_col_name").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree college name is required";
      }
    }
  });
  $(".xdegree_col_address").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree college address is required";
      }
    }
  });

  $(".xdegree_grade").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Degree grade is required";
      }
    }
  });

  /*$('.xlanguage_hindi').editable({
        validate: function(value){
           if($.trim(value) == '')
           {
            return 'Language hindi grade is required';
           }
        }
    });*/

  $(".xlanguage_hindi").editable({
    //value: [2, 3],
    type: "checklist",
    url: "/post",
    sourceCache: false,
    source: [
      {
        value: "Read",
        text: "Read"
      },
      {
        value: "Write",
        text: "Write"
      },
      {
        value: "Speak",
        text: "Speak"
      }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Required";
      }
    }
  });

  $(".xlanguage_english").editable({
    //value: [2, 3],
    type: "checklist",
    url: "/post",
    sourceCache: false,
    source: [
      {
        value: "Read",
        text: "Read"
      },
      {
        value: "Write",
        text: "Write"
      },
      {
        value: "Speak",
        text: "Speak"
      }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Required";
      }
    }
  });

  /* $('.xlanguage_english').editable({
        validate: function(value){
           if($.trim(value) == '')
           {
            return 'Language english grade is required';
           }
        }
    });*/

  $(".xlanguage_other").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Language other grade is required";
      }
    }
  });

  $(".xspecialization_choice1").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Specialization choice1 is required";
      }
    }
  });

  $(".xspecialization_choice2").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Specialization choice2 is required";
      }
    }
  });

  $(".xspecialization_choice3").editable({
    validate: function(value) {
      if ($.trim(value) == "") {
        return "Specialization choice3 is required";
      }
    }
  });

  /* $('.xapplicant_photo').editable({
        validate: function(value){
           if($.trim(value) == '')
           {
            return 'Applicant photo is required';
           }
        }
    });*/

  $(".xdeclaration").editable({
    type: "select",
    url: "/post",
    sourceCache: false,
    source: [
      { value: "", text: "Select one" },
      { value: 0, text: "No" },
      { value: 1, text: "Yes" }
    ],
    validate: function(value) {
      if ($.trim(value) == "") {
        return "xdeclaration is required";
      }
    }
  });
});
