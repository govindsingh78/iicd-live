<?php require_once 'rightusercheck.php';?>

<?php
require_once '../meekrodb.2.3.class.php';
$countries = DB::query("select max(center_code) as center_code from exam_centers");

$centerCodeGenerated = $countries[0]['center_code'];
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->
        <style type="text/css">
        .error-show{
            color: red;
        }
        </style>
        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
               	<?php require_once 'header.php';?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Validation Header -->
                        <div class="content-header">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="header-section">
                                        <h1>Exam Center</h1>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs">
                                    <div class="header-section">
                                        <ul class="breadcrumb breadcrumb-top">
                                            <li>Home</li>
                                            <li><a href="">Exam Center List</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <!-- Form Validation Block -->
                                <div class="block">
                                    <!-- Form Validation Title -->
                                    <div class="block-title">
                                        <h2>Add Exam Center</h2>
                                    </div>
                                    <!-- END Form Validation Title -->

                                    <!-- Form Validation Form -->
                                    <form id="form-validation" action="save_examcenter.php" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

                                    <div class="form-group">
                                            <label class="col-md-3 control-label" for="center_name">Exam Center Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="center_name" class="form-control" required="" value=""  placeholder="Enter Exam Center Name. Max : 60 Charecter">
                                            </div>
                                      	</div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="center_name">Center Code <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="center_code" class="form-control" required="" placeholder="Enter Center Code. Max : 2 Digits" value="<?php echo $centerCodeGenerated + 1; ?>" readonly="readonly">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="city">City <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="city" class="form-control" required="" value=""  placeholder="Enter City Name.  Max : 30 Charecter ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="address">Address <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <textarea name="address" class="form-control" required="" value="" id="" rows="4" placeholder="Enter Address.  Max : 120 Charecter "></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="start_time">Contact person <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <textarea type="text" name="contact_person" class="form-control" required="" placeholder="Enter Contact person.  Max : 60 Charecter " rows="4"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="start_time">Landmark <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <textarea type="text" name="landmark" class="form-control" required="" placeholder="Enter Landmark. Max : 60 Charecter " rows="4"></textarea>
                                            </div>
                                        </div>


                                    	<!-- <div class="form-group">
                                            <label class="col-md-3 control-label" for="center_name">Exam Center Name <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="center_name" class="form-control" required="" placeholder="Enter Center Name">
                                            </div>
                                      	</div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="city">City <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="city" class="form-control" required="" placeholder="Enter City Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="address">Address <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <textarea name="address" class="form-control" required="" placeholder="Enter Address" id="" rows="4"></textarea>
                                            </div>
                                        </div> -->
                                      <!--   <div class="form-group">
                                            <label class="col-md-3 control-label" for="start_time">Start Time <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="start_time" class="form-control" required="" placeholder="Enter Start time">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="end_time">End Time <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="end_time" class="form-control" required="" placeholder="Enter End time">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="reporting_time">Reporting Time <span class="text-danger">*</span></label>
                                            <div class="col-md-6">
                                                <input type="text" name="reporting_time" class="form-control" required="" placeholder="Enter Reporting time">
                                            </div>
                                        </div> -->
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-3">
                                                <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit">Submit</button>
                                                <button type="reset" class="btn btn-effect-ripple btn-danger">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Form -->
                                </div>
                                <!-- END Form Validation Block -->
                            </div>
                        </div>
                        <!-- END Form Validation Content -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
		<script src="js/plugins/ckeditor/ckeditor.js"></script>
        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/formsValidation.js"></script>
        <script>$(function(){ FormsValidation.init(); });</script>

        <!-- <script type="text/javascript" src="js/jquery.validate.min.js"></script>

                                          <script type="text/javascript">

                                                  $('#form-validation').validate({
                                                  ignore: [],
                                                  errorElement: 'div',
                                                  errorClass: 'error-show',
                                                  focusInvalid: false,
                                                  rules:
                                                  {
                                                      "center_name": {
                                                      required: true,
                                                      maxlength: 60,
                                                      customvalidation: true

                                                      },

                                                      "city": {
                                                      required: true,
                                                      maxlength: 30,
                                                      customvalidation: true

                                                      },
                                                      "address": {
                                                      required: true,
                                                      maxlength: 120,
                                                      allowsomespecialcharwithsinglespace: true

                                                      }
                                                  },
                                                  messages:
                                                  {
                                                      "center_name": {
                                                      required: "Center Name is required",
                                                      maxlength: "Max 60 Charecters are allowed",
                                                      customvalidation: "Sorry, special characters , numbers & multiple whitespaces are not allowed"

                                                      },

                                                      "city": {
                                                      required: "City Name is required",
                                                      maxlength: "Max 30 Charecters are allowed",
                                                      customvalidation: "Sorry, special characters , numbers & multiple whitespaces are not allowed"

                                                      },
                                                      "address": {
                                                          required: "Address is required",
                                                          maxlength: "Max 120 Charecters are allowed",
                                                          allowsomespecialcharwithsinglespace: "Sorry, only  (),-@./#&+  special characters & single whitespace are allowed"

                                                      }
                                                  }
                                                  });
                                                  $.validator.addMethod("customvalidation",
                                                      function(value, element) {
                                                             //return /^[a-zA-Z ]+$/.test(value);
                                                             ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                                                             return /^([a-zA-Z]+\s)*[a-zA-Z]+$/.test(value);

                                                      },
                                                  "Sorry, special characters , numbers & multiple whitespaces are not allowed"
                                                  );

                                                    $.validator.addMethod("allowsomespecialcharwithsinglespace",
                                                      function(value, element) {
                                                             //return /^[a-zA-Z ]+$/.test(value);
                                                             ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                                                             ///^[-@./#&+\w\s]*$/
                                                             return /^([(),-@./#&+\w]+\s)*[(),-@./#&+\w]+$/.test(value);

                                                      },
                                                  "Sorry, only  (),-@./#&+ special characters & single whitespace are allowed"
                                                  );






                                          </script> -->




                                          <script type="text/javascript" src="js/jquery.validate.min.js"></script>

                                          <script type="text/javascript">

                                                  $('#form-validation').validate({
                                                  ignore: [],
                                                  errorElement: 'div',
                                                  errorClass: 'error-show',
                                                  focusInvalid: false,
                                                  rules:
                                                  {
                                                      "center_name": {
                                                      required: true,
                                                      maxlength: 60,
                                                      customvalidation: true

                                                      },
                                                      "center_code": {
                                                      required: true,
                                                      number: true,
                                                      maxlength: 2
                                                      //customvalidation: true,

                                                      },

                                                      "city": {
                                                      required: true,
                                                      maxlength: 30,
                                                      customvalidation: true

                                                      },
                                                      "address": {
                                                      required: true,
                                                      maxlength: 120,
                                                      allowsomespecialcharwithsinglespace: true


                                                      },
                                                      "contact_person": {
                                                          required: true,
                                                          maxlength: 60,
                                                          allowsomespecialcharwithsinglespace: true


                                                      },
                                                      "landmark": {
                                                          required: true,
                                                          maxlength: 60,
                                                          allowsomespecialcharwithsinglespace: true

                                                      }
                                                  },
                                                  messages:
                                                  {
                                                      "center_name": {
                                                      required: "Center Name is required",
                                                      maxlength: "Max 60 Charecters are allowed",
                                                      customvalidation: "Sorry, special characters , numbers & multiple whitespaces are not allowed"

                                                      },
                                                      "center_code": {
                                                      required: "Center Code is required",
                                                      number: "Center Code must be numbers only",
                                                      maxlength: "Max 2 Digits are allowed"


                                                      },

                                                      "city": {
                                                      required: "City Name is required",
                                                      maxlength: "Max 30 Charecters are allowed",
                                                      customvalidation: "Sorry, special characters , numbers & multiple whitespaces are not allowed"

                                                      },
                                                      "address": {
                                                          required: "Address is required",
                                                          maxlength: "Max 120 Charecters are allowed",
                                                          allowsomespecialcharwithsinglespace: "Sorry, only  (),-@./#&+  special characters & single whitespace are allowed"

                                                      },
                                                      "contact_person": {
                                                          required: "Contact Person Detail is required",
                                                          maxlength: "Max 60 Charecters are allowed",
                                                          allowsomespecialcharwithsinglespace: "Sorry, only  (),-@./#&+  special characters & single whitespace are allowed"

                                                      },
                                                      "landmark": {
                                                          required: "Landmark is required",
                                                          maxlength: "Max 60 Charecters are allowed",
                                                          // customvalidation: "Sorry, special characters , numbers & multiple whitespaces are not allowed",
                                                          allowsomespecialcharwithsinglespace: "Sorry, only  (),-@./#&+  special characters & single whitespace are allowed"

                                                      }
                                                  }
                                                  });
                                                  $.validator.addMethod("customvalidation",
                                                      function(value, element) {
                                                             //return /^[a-zA-Z ]+$/.test(value);
                                                             ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                                                             return /^([a-zA-Z]+\s)*[a-zA-Z]+$/.test(value);

                                                      },
                                                  "Sorry, special characters , numbers & multiple whitespaces are not allowed"
                                                  );

                                                    $.validator.addMethod("allowsomespecialcharwithsinglespace",
                                                      function(value, element) {
                                                             //return /^[a-zA-Z ]+$/.test(value);
                                                             ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                                                             ///^[-@./#&+\w\s]*$/
                                                             return /^([(),-@./#&+\w]+\s)*[(),-@./#&+\w]+$/.test(value);

                                                      },
                                                  "Sorry, only  (),-@./#&+ special characters & single whitespace are allowed"
                                                  );






                                          </script>
    </body>
</html>
<?php $DB->close();?>