<?php require_once 'rightusercheck.php';?>
<?php
require_once '../meekrodb.2.3.class.php';
//include('../phpmailer/PHPMailerAutoload.php');
require_once 'phpmailer/class.phpmailer.php';
$path = "https://www.iicd.ac.in/";

$id = $_POST['id'];

$hrEmail = 'govindsingh78@gmail.com';

$sqlquery = "SELECT users.id AS UserID, users.*, user_details.Programme,user_details.fathers_name,user_details.dob,user_details.phone,user_details.exam_center1,user_details.applicant_photo,user_details.id_proof_back,user_details.id_proof_front,user_details.signatures,exam_centers.center_name,exam_centers.city  FROM users";

$sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
$sqlquery = $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city";
$sqlquery = $sqlquery . " WHERE users.status = 1 AND users.id = '" . $id . "'";
$result = DB::queryFirstRow($sqlquery);

$first_name = $result['first_name'];
$middle_name = $result['middle_name'];
$last_name = $result['last_name'];
$fullname = $first_name . ' ' . $middle_name . ' ' . $last_name;
$fathers_name = $result['fathers_name'];
$Programme = $result['Programme'];
$enroll_id = $result['enroll_id'];

if ($Programme == 'PG') {
    $pname = "Master of Vocation in Crafts and Design";
} elseif ($Programme == 'UG') {
    $pname = "4 Year Integrated Bachelors Programme (CFPD + B. VOC)";
} else {
    $pname = "5 Year Integrated Masters Programme (CFPD + B. VOC. + M. VOC.)";
}

$dob = date('d/m/Y', strtotime($result['dob']));
$phone = $result['phone'];
$exam_center1 = $result['exam_center1'];
$center_name = $result['center_name'];
$applicant_photo = $path . 'images/' . $result['applicant_photo'];
//$dob_certificate = $path . 'images/' . $result['dob_certificate'];
$id_proof_front = $path . 'images/' . $result['id_proof_front'];
$id_proof_back = $path . 'images/' . $result['id_proof_back'];
$signatures = $path . 'images/' . $result['signatures'];

$phase1_attendence = $result['phase1_attendence'];
$phase1_result = $result['phase1_result'];
$phase2_attendence = $result['phase2_attendence'];
$phase2_result = $result['phase2_result'];

$date_of_entrance_test_parta = DB::queryFirstRow("SELECT * FROM settings WHERE id = 3");
$date_of_entrance_test_partb = DB::queryFirstRow("SELECT * FROM settings WHERE id = 9");
$parta_time_from_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 4");
$parta_time_to_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 5");
$partb_time_from_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 6");
$partb_time_to_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 7");
$partb_time_from_2 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 8");
$test_center_partb = DB::queryFirstRow("SELECT * FROM settings WHERE id = 10");
$address_test_center_partb = DB::queryFirstRow("SELECT * FROM settings WHERE id = 11");
$subjectLine = DB::queryFirstRow("SELECT * FROM settings WHERE id = 17");

$lineofSubject = strip_tags($subjectLine['value']);

if ($phase1_result == 1) {
    $labelofexam = "Date of Entrance Test (Part B)";
    $dateofexam = strip_tags($date_of_entrance_test_partb['value']);

    $center_name_b = strip_tags($test_center_partb['value']);
    $address_b = strip_tags($address_test_center_partb['value']);

} else {
    $labelofexam = "Date of Entrance Test (Part A)";
    $dateofexam = strip_tags($date_of_entrance_test_parta['value']);
}

//partbtimefrom1  partbtimeto1 partbtimefrom2
$partatimefrom1 = strip_tags($parta_time_from_1['value']);
$partatimeto1 = strip_tags($parta_time_to_1['value']);

$partbtimefrom1 = strip_tags($partb_time_from_1['value']);
$partbtimeto1 = strip_tags($partb_time_to_1['value']);

$partbtimefrom2 = strip_tags($partb_time_from_2['value']);

//partbtimefrom1  partbtimeto1 partbtimefrom2

$examcenter = "SELECT address FROM exam_centers WHERE city = '" . $result['exam_center1'] . "'";
$centerName = DB::queryFirstRow($examcenter);
$address = $centerName['address'];

$settings = "SELECT * FROM settings WHERE id = 1";
$settingsName = DB::queryFirstRow($settings);

$settings2 = "SELECT * FROM settings WHERE id = 2";
$settingsName2 = DB::queryFirstRow($settings2);

$value = $settingsName['value'];
$value2 = $settingsName2['value'];
//  $template_back = DB::queryFirstRow("SELECT * FROM `email_templates` WHERE slug='email-body'");
//$temp = $template_back['body'];
$temp = $value2;

$rightsideTitle = DB::queryFirstRow("SELECT * FROM settings WHERE id = 13");
$titleRightSide = strip_tags($rightsideTitle['value']);

/*$newbody = '<div style="
background-image: url(../images/main-body-bg.png);
background-position: center;
background-repeat: repeat;
background-attachment: fixed;">
<div style="width: 800px;margin: 0 auto;padding: 10px 0;">
<div style="float: left;width: 100px;">
<a href="#">
<img style="width: 100px; display: block;" src="'.$path.'images/logo1.png" alt="">
</a>
</div>
<div style="float: left;width: 450px;">
<a href="#">
<img style="width: 450px; display: block;" src="'.$path.'images/indian1.png" alt="">
</a>
</div>
<div style="float: left;width: 120px;">
<a href="#">
<img style="width: 120px; display: block;" src="'.$path.'images/admit1.png" alt="">
</a>
</div>
</div>
<div class="clearfix"></div>
<div class="admit-card-form">
<div style="width: 800px;margin: 0 auto;">
<div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;">
<div style="border-bottom: 1px solid #000;float: left;width: 800px;">
<div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Name</div>
<div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box; text-transform: capitalize;"><strong> '.$fullname.' </strong></div>
<div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Enrollment No.</div>
<div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$enroll_id.'</strong></div>
</div>
<div style="border-bottom: 1px solid #000;float: left;width: 800px;">
<div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Father`s Name</div>
<div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box; text-transform: capitalize;"><strong>'.$fathers_name.'</strong></div>
<div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Programme</div>
<div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$pname.'</strong></div>
</div>
<div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Date of Birth</div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box;"><strong>'.$dob.'</strong></div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Mobile No.</div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$phone.'</strong></div>
</div>
<div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
<div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Date of Entrance Test Part A :</div>
<div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box;"><strong>15/04/2018</strong></div>
<div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Test Center</div>
<div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>'.$center_name.'</strong></div>
</div>
<div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
<div style="height:60px;float: left;height: 60px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Address of Test Center</div>
<div style="height:60px;float: left;min-height: 84px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 497px;box-sizing: border-box;"><strong>'.$address.'</strong></div>

</div>
</div>
<h3 style="text-align: center;font-size: 20px;clear: both;">Entrance Test Attendance:</h3>
<div style="float: left; width: 800px;">
<div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;width: 800px;">
<div style="border-bottom: 1px solid #000;float: left;width: 800px;">
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 225px;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
margin: 0;
font-size: 20px;">Part A </h6>General Awareness, Creativity & Perception Test <strong style="display: block;
text-align: center;"> <br> (10:00 AM  to  1:00  PM) </strong></div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 200px;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
margin: 0;
font-size: 20px;">Part B </h6>Material,  Color & Conceptual  Test <strong style="display: block;
text-align: center;"> <br> (10:00 AM  to  1:00  PM) </strong></div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 190px;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
margin: 0;
font-size: 20px;">Part C </h6>Personal Interview <br> <strong style="display: block;
text-align: center;"> (2:00  PM  onwards) </strong></div>

</div>
<div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 225px;box-sizing: border-box;text-align: center;"><h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5></div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 200px;box-sizing: border-box;text-align: center;"><h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5></div>
<div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 190px;box-sizing: border-box;text-align: center;"><h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5></div></div>
</div>
</div>
<div style="float: left; width: 800px;">
<div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;width: 800px;margin-top:30px;">
<div style="border-bottom: 1px solid #000;float: left;width: 800px;">
<div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$applicant_photo.'" height="150" ></div>
<div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$dob_certificate.'" height="150"></div>
<div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$id_proof.'" height="150"></div>
<div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="'.$signatures.'" height="150"></div>
</div>

</div>
</div>
<h5 style="text-align: right;font-size: 18px;margin: 40px 0;clear: both;float: right;
width: 800px;"> <img style="max-width: 220px; display: block;float: right;" src="'.$path.'images/signature.png" alt=""></h5>
'.$value.'
</div>
</div>
</div>';
 */

$newbody = '<div style="
background-image: url(../images/main-body-bg.png);
background-position: center;
background-repeat: repeat;
background-attachment: fixed;">
        <div style="width: 800px;margin: 0 auto;padding: 10px 0;">
                             <div style="float: left;width: 100px;">
                                    <a href="#">
                                        <img style="width: 100px; display: block;" src="' . $path . 'images/logo1.png" alt="">
                                    </a>
                                </div>
                                 <div style="float: left;width: 450px;">
                                    <a href="#">
                                        <img style="width: 450px; display: block;" src="' . $path . 'images/indian1.png" alt="">
                                    </a>
                                </div>
                                <div style="float: left;width: 120px; text-align: center">
                                                        <h2 style="margin: 0px; font-size: 22px; font-weight: 900; display: inline-flex">
                                                        <strong>ADMIT</strong>
                                                        </h2>

                                                        <h2 style="margin: 0px; font-size: 22px; font-weight: 900;  display: inline-flex">
                                                        <strong>CARD</strong>
                                                        </h2>

                                                        <h4 style="font-size: 12px;  display: inline-flex">
                                                        ' . $titleRightSide . '
                                                        </h4>
                                </div>
                        </div>
                        <div class="clearfix"></div>
       <div class="admit-card-form">
         <div style="width: 800px;margin: 0 auto;">
           <div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;">
             <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Name</div>
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box; text-transform: capitalize;"><strong> ' . $fullname . ' </strong></div>
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Enrollment No.</div>
              <div style="height:40px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>' . $enroll_id . '</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Father`s Name</div>
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box; text-transform: capitalize;"><strong>' . $fathers_name . '</strong></div>
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Programme</div>
              <div style="height:80px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>' . $pname . '</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Date of Birth</div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box;"><strong>' . $dob . '</strong></div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Mobile No.</div>
              <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>' . $phone . '</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">' . $labelofexam . '</div>
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 160px;box-sizing: border-box;"><strong>' . $dateofexam . '</strong></div>';

if ($phase1_result == 1) {
    $newbody .= '<div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Test Center</div>
              <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>' . $center_name_b . '</strong></div>
            </div>
             <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
              <div style="height:60px;float: left;height: 60px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Address of Test Center</div>
              <div style="height:60px;float: left;min-height: 84px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 497px;box-sizing: border-box;"><strong>' . $address_b . '</strong></div>

            </div>
           </div>';
} else {

    $newbody .= '<div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Test Center</div>
                <div style="height:60px;float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 155px;box-sizing: border-box;"><strong>' . $center_name . '</strong></div>
              </div>
               <div style="border-bottom: 1px solid #000;float: left;width: 1170px;">
                <div style="height:60px;float: left;height: 60px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 140px;box-sizing: border-box;">Address of Test Center</div>
                <div style="height:60px;float: left;min-height: 84px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 497px;box-sizing: border-box;"><strong>' . $address . '</strong></div>

              </div>
             </div>';

}

$newbody .= '<h3 style="text-align: center;font-size: 20px;clear: both;">Entrance Test Attendance:</h3>';

if ($phase1_result == 1) {

    $newbody .= '<div style="list-style: none;
            margin-top: 0;
            margin-bottom: 10px;
            padding: 0px;

            clear: both;


            margin-bottom: 20px;
            padding: 0px;
            margin: 0px;
            float: left;
            width: 100%; display: flex;">
            <div style="float: left;
            width: 46%;
            font-size: 16px;
            padding: 10px;
            border-right: 1px solid #000;
            border-top: 1px solid #000;
            text-align: center;
            border-top: 0px;">
            <h6 style="text-align: center;
            margin: 0;
            font-size: 20px;">Part B-1 </h6>Material,  Color & Conceptual  Test <strong style="
            text-align: center;"> <br> (' . $partbtimefrom1 . '  to ' . $partbtimeto1 . ') </strong>
            <br>
            <h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5>

            </div>
            <div style="float: left;

            width:46%;

            font-size: 16px;

            padding: 10px;

            border-right: 1px solid #000;

            border-top: 1px solid #000;

            text-align: center;

            border-top: 0px;">
            <h6 style="text-align: center;
            margin: 0;
            font-size: 20px;">Part B-2 </h6>Personal Interview <br> <strong style="display: block;
            text-align: center;"> (' . $partbtimefrom2 . ' onwards) </strong>
            <br>
            <h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5>
            </div>


            </div>';
} else {
    $newbody .= '<div style="float: left; width: 800px;">
            <div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;width: 800px;">
            <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
            <div style="float: left;min-height: 41px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 11px;width: 100%;box-sizing: border-box;text-align: center;"><h6 style="text-align: center;
            margin: 0;
            font-size: 20px;">Part A </h6>General Awareness, Creativity & Perception Test <strong style="display: block;
            text-align: center;"> <br> (' . $partatimefrom1 . '  to ' . $partatimeto1 . ') </strong>
            <br>
            <h5 style="margin: 0;font-size: 20px;color: #ccc;">Invigilator Initial </h5>

            </div>

            </div>';

}

$newbody .= '<div style="float: left; width: 800px;">
            <div style="padding: 0px;border-top: 1px solid #000;border-left: 1px solid #000;float: left;list-style: none;width: 800px;margin-top:30px;">
              <div style="border-bottom: 1px solid #000;float: left;width: 800px;">
              <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="' . $applicant_photo . '" height="150" ></div>
            <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="' . $id_proof_front . '" height="150"></div>
          <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="' . $id_proof_back . '" height="150"></div>
          <div style="float: left;height: 150px;border-right: 1px solid #000;padding: 10px;display: block;font-size: 15px;width: 148px;box-sizing: border-box;text-align: center;"><img src="' . $signatures . '" height="150"></div>
          </div>

            </div>
          </div>
         <h5 style="text-align: right;font-size: 18px;margin: 40px 0;clear: both;float: right;
width: 800px;"> <img style="max-width: 220px; display: block;float: right;" src="' . $path . 'images/signature.png" alt=""></h5>
        ' . $value . '
         </div>
       </div>
    </div>';

include "mpdf/mpdf.php";

//include your mpdf library here

$mpdf = new mPDF('utf-8', 'A4-L');

// create an object of the class mpdf
$mpdf = new mPDF("c");
// write the html to the file

$mpdf->WriteHTML($newbody);
$filename = 'IICD' . time() . $id . '.pdf';
// generate the output
$rootpath = __DIR__ . '/admit_cards/' . $filename;

$output = $mpdf->Output($rootpath, 'F');

$mail = new PHPMailer;
$mail->isSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Username = 'admissions@iicd.ac.in';
$mail->Password = 'admission915';
$mail->setFrom($hrEmail, 'IICD');
$mail->addReplyTo($hrEmail, 'IICD');
$mail->addAddress($result['email']); //tl mail $result['email']
$mail->AddCC('admissions@iicd.ac.in');
$mail->addAttachment($rootpath);
$mail->Subject = " $lineofSubject ";
$mail->msgHTML($temp);
if (!$mail->send()) {
    $result['failed'] = 'failed';
    $result['message'] = 'Failed';
    echo json_encode($result);exit;
} else {

    $update = DB::update('users', array('admit_card' => $filename, 'pdf_sent' => 1), "id=%i", $id);
    $result['success'] = 'success';
    $result['message'] = 'Mail Sent Successfully';
    echo json_encode($result);exit;
}
