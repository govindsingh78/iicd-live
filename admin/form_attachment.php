<?php 
  session_start();
  include 'meekrodb.2.3.class.php';

  $query = "select * from user_details where user_id = '".$_SESSION['user_id']."'";
  $row = DB::queryFirstRow($query);

  $base_url = 'https://www.iicd.ac.in/';

  $photo_src = $base_url.'images/default.png';
  if($row['applicant_photo'] !=''){
    $photo_src = $base_url.'images/'.$row['applicant_photo'];
  }

  $dob_src = $base_url.'images/default.png';
  if($row['dob_certificate'] !=''){
    $dob_src = $base_url.'images/'.$row['dob_certificate'];
  }

  $id_src = $base_url.'images/default.png';
  if($row['id_proof'] !=''){
    $id_src = $base_url.'images/'.$row['id_proof'];
  }

  $sig_src = $base_url.'images/default.png';
  if($row['signatures'] !=''){
    $sig_src = $base_url.'images/'.$row['signatures'];
  }

?>
<form id="form_attach" name="form_attach">
  <br><br>
  <div class="group">
    <div class="col-md-4"> 
      <div id="error_file_name"></div>
    </div>  
  </div>
  <div class="group">
  </div>
  <div class="group">
   
   <div class="col-md-6"> 
     <label class="my-label" style="font-size: 10px">Signature of the candidate</label>
         <p class="pho-txt"> File size limit to 4MB, Supported file format JPG, JPEG. </p> 
         <input type='file' id="imgInp4" name="signatures" accept="image/jpg, image/jpeg"/>
          <div id='img_contain4'>
            <img id="blah4" align='middle' src="<?=$sig_src?>"/>
          </div>
  </div>                           
        
  </div>
          
<nav class="form-section-nav">
  <input type="hidden" name="action" id="action" value="save_attach">
  
   
  <input type="hidden" name="hidden_sig" id="hidden_sig" value="<?=$row['signatures']?>">
  <input type="hidden" name="file_extension_allow" id="file_extension_allow" value="1">
  
  <span id="btn_back_attach" class="btn-secondary form-nav-prev">  <img src="images/left-arrow.jpg" alt="left"> Prev</span>
  <span id="btn_next_attach" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
</nav>
</form>
<style type="text/css">
  #error_file_name{
    color: red;
    font-size: 14px;
    margin-top: -40px;
    letter-spacing: 3px;
    font-style: oblique;
    font-weight: bold;
  }
</style>

<script type="text/javascript">
$(document).ready(function(){  

    $("#imgInp4").change(function () {
      var fsize = this.files[0].size;
      check_file_type($(this).val(),fsize);
    });

    $("#btn_back_attach").unbind().click(function() {
      $('#specialization_container').load('form_specialization.php',function(e){
         $("#attachment_container" ).slideUp( "slow");
         $('#attachment_container').html('');
         $("#specialization_container" ).slideDown( "slow");
      });
    });

    $("#btn_next_attach").unbind().click(function() {
        
         
        var hidden_sig = $('#hidden_sig').val();

        if(hidden_sig ==''){
            if(!$('#form_attach').valid()){
              return false;
            }
        }

        if($('#file_extension_allow').val()==0){
          return false; 
        }
        
        var formData = new FormData($('form#form_attach')[0]);
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            contentType: false,
            cache: false,

            processData:false,
            success: function(response) {
              if(response.status == 1){
                $('#declaration_container').load('form_declaration.php',function(e){
                  $("#attachment_container" ).slideUp( "slow");
                  $('#attachment_container').html('');
                  $("#declaration_container" ).slideDown( "slow");
                });
                
              }
            }
        });
    });


    $('#form_attach').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules: 
        {
          "signatures": {
            required: true          
          }

        },
        messages: 
        {
         "signatures": {
            required: "Signatures is required"
          }
        }
  });

   



function readURL4(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah4').attr('src', e.target.result);

      $('#blah4').hide();
      $('#blah4').fadeIn(650);

    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp4").change(function() {
  readURL4(this);
});

function check_file_type(file_ext,fsize){

  var errorMsg = '';

  var fileExtension = ['jpeg','jpg'];

  if(fsize > 2097152){
    errorMsg = "File size limit 4mb";
  }

  
  if($.inArray(file_ext.split('.').pop().toLowerCase(), fileExtension) == -1 ) {
    
    if(errorMsg !=''){
      errorMsg = errorMsg+", Supported file formats: jpg,jpeg";

    }else{
      errorMsg = "Supported file formats: jpg,jpeg";
    }
  }

  if(errorMsg !=''){

    $('#error_file_name').html(errorMsg); 
    $('#file_extension_allow').val(0);
    return false;
  
  }else{
    
    $('#error_file_name').html(''); 
    $('#file_extension_allow').val(1);
  }

} 


});
</script>