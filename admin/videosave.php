<?php require_once('rightusercheck.php');?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); }?>
<?php 
    require_once('image-resizer.php');
    require_once('main.php');
	$DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); 
?>
<?php
	$edit				=   (!empty($_POST['edit'])) ? (int)$_POST['edit'] : 0;
    $id                 =   (!empty($_POST['id'])) ? (int)$_POST['id'] : 0;
    $title     	        =   (!empty($_POST['title'])) ? $_POST['title'] : null;
    $image              =   (!empty($_FILES['image']))? $_FILES['image']: null;
    $videolink	        =   (!empty($_FILES['videolink']))? $_FILES['videolink']: null;



    if ($_FILES['image']['name'] != '') {
      function GetImageExtension($imagetype)
    {
        if(empty($imagetype)) return false;
        switch($imagetype)
        {
           case 'image/bmp': return '.bmp';
           case 'image/gif': return '.gif';
           case 'image/jpeg': return '.jpg';
           case 'image/png': return '.png';
           default: return false;
        }
    }
     
    if ($_FILES["image"]["error"]==0) {  
    $temp_name=$_FILES["image"]["tmp_name"];
    $imgtype=$_FILES["image"]["type"];
    $ext= GetImageExtension($imgtype);
    $file_name='upload_images/'.time().'_'.rand(0, 99).$ext;
    $target_path = $file_name;           
    move_uploaded_file($temp_name, $target_path);
    $bigimagebasepath     = $file_name;
    } 
    }

    if($_FILES['videolink']['name'] != '')
    {
        $name = $_FILES['videolink']['name'];
        $extension = explode('.', $name);
        $extension = end($extension);
        $type = $_FILES['videolink']['type'];
        $size = $_FILES['videolink']['size'] /1024/1024;
        $random_name = rand();
        $tmp = $_FILES['videolink']['tmp_name'];
        $videolinks = 'videos/'.$random_name.'.'.$extension;
        
        
        if ((strtolower($type) != "video/mpg") && (strtolower($type) != "video/wma") && (strtolower($type) != "video/mov") 
        && (strtolower($type) != "video/flv") && (strtolower($type) != "video/mp4") && (strtolower($type) != "video/avi") 
        && (strtolower($type) != "video/qt") && (strtolower($type) != "video/wmv") && (strtolower($type) != "video/wmv"))
        {

             echo "<script language='javascript'>alert('Video Format Not Supported !');window.location = 'video.php';</script>";

        }else
        {
            move_uploaded_file($tmp, 'videos/'.$random_name.'.'.$extension);    
          
        }


    }


                    
 ?>

<?php   
    if (isset($edit))
    {
      if ((int)$edit == 0)
      {
        $sqlquery   = "INSERT INTO videos (title, image, videolink,created_at)";
        $sqlquery   = $sqlquery . " VALUES('".$title."', '".$bigimagebasepath."', '".$videolinks."', '".date("Y-m-d",time())."')";


        $result     =  $DB->savedata($sqlquery);
        $action     =  0;
      }
      else
      {
        $sqlquery   = "UPDATE videos SET title = '".$title."',  created_at = '".date("Y-m-d",time())."'";
        if(!empty($bigimagebasepath)) {
            $sqlquery   =  $sqlquery.", image = '".$bigimagebasepath."'";    
        }
        if(!empty($videolinks)) {
            $sqlquery   =  $sqlquery.", videolink = '".$videolinks."'";    
        }
        $sqlquery   =  $sqlquery." WHERE id = ".$id."";

        $result     =  $DB->updatedata($sqlquery);
        $action     =  1;  
      }
   }
    
    if ((int)$action == 0)
    {
        echo "<script language='javascript'>alert('Video is added successfully');window.location = 'displayvideos.php';</script>";
    }
    elseif ((int)$action == 1)
    {
        echo "<script language='javascript'>alert('Video is updated successfully');window.location = 'displayvideos.php';</script>";
    }
    else
    {
        echo "<script language='javascript'>history.go(-1);</script>";   
    }
?>
<?php $DB -> close(); ?>