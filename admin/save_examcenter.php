<?php require_once('rightusercheck.php');?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); }?>
<?php 
    //require_once('image-resizer.php');
    require_once('../meekrodb.2.3.class.php'); 
?>
<?php  
    if(isset($_POST['submit']))
    {
        // $center_name = (!empty($_POST['center_name']))? trim($_POST['center_name']): null;
        // $city = (!empty($_POST['city'])) ? trim($_POST['city']) : null;
        // $address = (!empty($_POST['address']))? trim($_POST['address']): null;
        // $start_time = (!empty($_POST['start_time']))? trim($_POST['start_time']): null;
        // $end_time = (!empty($_POST['end_time']))? trim($_POST['end_time']): null;
        // $reporting_time = (!empty($_POST['reporting_time']))? trim($_POST['reporting_time']): null;

        $center_name = (!empty($_POST['center_name']))? trim($_POST['center_name']): null;
        $city = (!empty($_POST['city'])) ? trim($_POST['city']) : null;
        $address = (!empty($_POST['address']))? trim($_POST['address']): null;
        $center_code = (!empty($_POST['center_code']))? trim($_POST['center_code']): null;
        $contact_person = (!empty($_POST['contact_person']))? trim($_POST['contact_person']): null;
        $landmark = (!empty($_POST['landmark']))? trim($_POST['landmark']): null;
        $start_time = (!empty($_POST['start_time']))? trim($_POST['start_time']): null;
        $end_time = (!empty($_POST['end_time']))? trim($_POST['end_time']): null;
        $reporting_time = (!empty($_POST['reporting_time']))? trim($_POST['reporting_time']): null;
        $centerStatus = (!empty($_POST['centerStatus']))? trim($_POST['centerStatus']): 1;
        
        $qry = DB::insert('exam_centers', array(
                'center_name' => $center_name,
                'city' => $city,
                'address' => $address,
                'contact_person' => $contact_person,
                'center_code' => $center_code,
                'landmark' => $landmark,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'reporting_time' => $reporting_time,
                'center_status' => $centerStatus,
                ));
       if ($qry == TRUE)
        {
            echo "<script language='javascript'>alert('Exam Center is added successfully');window.location = 'examcenter_list.php';</script>";
        }
        else
        {
            echo "<script language='javascript'>history.go(-1);</script>";   
        }
    }
?>
