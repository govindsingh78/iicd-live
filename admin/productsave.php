<?php require_once('rightusercheck.php');?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); }?>
<?php 
    require_once('image-resizer.php');
    require_once('main.php');
	$DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); 
?>
<?php
	$edit				=   (!empty($_POST['edit'])) ? (int)$_POST['edit'] : 0;
    $id                 =   (!empty($_POST['id'])) ? (int)$_POST['id'] : 0;
    
    $catid              =   (!empty($_POST['catid'])) ? (int)$_POST['catid'] : 0;
    $producttype        =   (!empty($_POST['producttype'])) ? (int)$_POST['producttype'] : 0;
    
    $productname        =   (!empty($_POST['productname']))? trim($_POST['productname']): null;
    $seo_url			=   (!empty($_POST['seo_url']))? trim($_POST['seo_url']): null;
    $productprice     	=   (!empty($_POST['productprice'])) ? (float)$_POST['productprice'] : 0;
    $instock	        =   (!empty($_POST['instock']))? trim($_POST['instock']): null;
    $description       	=   htmlentities($_POST['description'],ENT_QUOTES,"utf-8");
    
	$miniimage	        =   null;
	$thumbimage         =   null;
    $mediumimage        =   null;
    $bigimage           =   null;
    
    $status             =   (!empty($_POST['status'])) ? (int)$_POST['status'] : 0;
    $sortorder          =   (!empty($_POST['sortorder'])) ? (int)$_POST['sortorder'] : 0;
    
    $maxid = rand();
   
    if(floatval($_FILES['bigimage']['size']) > 0)
    {
        $image = new ResizeImage();
        $image->load($_FILES['bigimage']['tmp_name']);
        
        $image->resizeToWidth(700);
        $bigimage    		=   $image->newFileName($_FILES['bigimage']['name'], $maxid, "_xl");
        //$savepath       	=   $dirpath.$bigimage;
        $savepath       	=   "upload_images/product_image/".$bigimage;
        $bigimagebasepath   =   $SiteUrl."product_image/".$bigimage;
        //Delete if already exist
        $deletepath     =   $savepath;
        $image->delete($deletepath);
        $image->save($savepath, $image->image_type, 100, null);
		
		//Thumb Image 
        $image->resizeToWidth(400);
        $thumbimage     	=   $image->newFileName($_FILES['bigimage']['name'], $maxid, "_m2");
        $savepath       	=   "upload_images/product_image/".$thumbimage;
        $thumbimagebasepath =    $SiteUrl."/upload_images/product_image/".$thumbimage;
        //Delete if already exist
        $deletepath     =   $savepath;
        $image->delete($deletepath);
        $image->save($savepath, $image->image_type, 100, null);
	}
    
?>
<?php   
    if (isset($edit))
    {
      if ((int)$edit == 0)
      {
        $sqlquery   = "INSERT INTO tblproducts (catid, producttype, productname, productprice, instock, description, bigimage, mediumimage, thumbimage, miniimage,status, sortorder)";
        $sqlquery   = $sqlquery . " VALUES(".$catid.", ".$producttype.", '".$productname."', '".$productprice."', '".$instock."', '".$description."', '".$bigimagebasepath."', '".$mediumimage."', '".$thumbimagebasepath."', '".$miniimage."', ".$status.", ".$sortorder.")";
        $result     =  $DB->savedata($sqlquery);
        $action     =  0;
      }
      else
      {
        $sqlquery   = "UPDATE tblproducts SET catid = ".$catid.", producttype = ".$producttype.", productname = '".$productname."', productprice = ".$productprice.", instock = '".$instock."', description = '".$description."', status = ".$status.", sortorder = ".$sortorder."";
        if(!empty($bigimage)) {
            $sqlquery   =  $sqlquery.", bigimage = '".$bigimagebasepath."', thumbimage = '".$thumbimagebasepath."'";    
        }
        $sqlquery   =  $sqlquery." WHERE id = ".$id."";
        $result     =  $DB->updatedata($sqlquery);
        $action     =  1;  
      }
   }
    
    if ((int)$action == 0)
    {
        echo "<script language='javascript'>alert('Product is added successfully');window.location = 'displayproduct.php';</script>";
    }
    elseif ((int)$action == 1)
    {
        echo "<script language='javascript'>alert('Product is updated successfully');window.location = 'displayproduct.php';</script>";
    }
    else
    {
        echo "<script language='javascript'>history.go(-1);</script>";   
    }
?>
<?php $DB -> close(); ?>