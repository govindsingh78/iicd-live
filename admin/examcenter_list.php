<?php require_once('rightusercheck.php'); ?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); } ?>
<?php 
    require_once('../meekrodb.2.3.class.php');
    /*$DB = new DBConfig();
    $DB -> config();
    $DB -> conn(); */
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>IICD</title>

        <meta name="description" content="IICD">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <!-- END Preloader -->

            <!-- Page Container -->
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <?php require_once('header.php'); ?>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- Datatables Block -->
                        <!-- Datatables is initialized in js/pages/uiTables.js -->
                        <div class="block full">
                            <div class="block-title">
                                <h2>Exam Center List</h2>
                            	<a href="add_examcenter.php" class="btn btn-success" style="float: right; margin-right: 10px; margin-top: 3px;"><i class="fa fa-plus"></i> Add Exam Center</a>
                            </div>
                            <div class="table-responsive">
                            <?php
                            	// how many rows to show per page
				                	$rowsPerPage = 30;

				              	// by default we show first page
				                	$pageNum = 1;

				                // if $_GET['page'] defined, use it as page number
				                	if(!empty($_GET['page']) && (int)$_GET['page'] > 0){
				                    	$pageNum = (int)$_GET['page'];
				                  	} else {
				                    	$pageNum =  1;
				               		}
				                        
				              	// counting the offset
				                	$offset = ($pageNum - 1) * $rowsPerPage;
                                     $sqlquery = DB::query("SELECT * FROM exam_centers order by city ASC");

				                    $counter = DB::count();
									if ($counter > 0) 
				                    { 
				                    	// print the random numbers
				     		?>
                                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 50px;">Center code</th>
                                            <th>City</th>
                                            <th>Center Name</th>
                                            <th>Address</th>
                                            <th>Contact person</th>
                                            <th>Landmark</th>
                                            <!-- <th>Status</th> -->
                                             <?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) == "Administrator") { ?>
                                            <th class="text-center" style="width: 75px;"><i class="fa fa-flash"></i></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
		                            	
		                              	foreach ($sqlquery as $value)
		                              	{
		                                 	               
		                          	?>
                                        <tr>
                                            <td class="text-center"><?php echo $value['center_code']; ?></td>
                                            <td><?php echo $value['city']; ?></td>
                                            <td><?php echo $value['center_name']; ?> &nbsp; <?php 
                                            $center_status = $value['center_status']; 
                                            if($center_status == 1){
                                            ?>
                                             <span title="Active Exam Center" class="btn btn-effect-ripple btn-xs btn-success clearfix">Active Exam Center</span>
                                            <?php } else{ ?>
                                                <span title="Inactive Exam Center" class="btn btn-effect-ripple btn-xs btn-danger clearfix">Inactive Exam Center</span>
                                            <?php } ?>
                                               </td>
                                            <td><?php echo $value['address']; ?></td>
                                            <td><?php echo $value['contact_person']; ?></td>
                                            <td><?php echo $value['landmark']; ?></td>

                                            
                                           
                                            <?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) == "Administrator") { ?>
                                            <td class="text-center">
                                            
                                           

                                            <a href="edit_examcenter.php?id=<?php echo $value['id'];?>" title="Edit Specialization" class="btn btn-effect-ripple btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                            <a href="delete_examcenter.php?id=<?php echo $value['id'];?>" data-toggle="modal" title="Delete Specialization" class="btn btn-effect-ripple btn-xs btn-danger detelebtn" onclick="return confirm('Are you sure you want to delete this item?');" username="<?php //  echo $rowdata['title'];?>" id="<?php //  echo $rowdata['id'];?>"><i class="fa fa-times"></i></a>
                                              
                                        </td>
                                            <?php } ?>
                                        </tr>
                                        <div id="modal-fade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	                                        <div class="modal-dialog">
	                                            <div class="modal-content">
	                                                <div class="modal-header">
	                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                                                    <h3 class="modal-title"><strong>Delete File</strong></h3>
	                                                </div>
	                                                <div class="modal-body">
	                                               		Are you sure want to delete exam center <span class="showuser" style="font-weight: bold;"></span> ?.
	                                                </div>
	                                                <div class="modal-footer">
	                                                    <a href="#" class="btn btn-effect-ripple btn-primary myprocesdelete">Delete</a>
	                                                    <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
                                	<?php
                                		}
                                	?>
                                    </tbody>
                                </table>
                              	<?php
                              		}else{										
                              	?>
                              	<table class="table table-striped table-bordered table-vcenter">
                              		<tr>
                              			<td align="center">No Record Found</td>
                              		</tr>
                              	</table>
                              	<?php } ?>
                            </div>
                        </div>
                        <!-- END Datatables Block -->
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="js/pages/uiTables.js"></script>
        <script>$(function(){ UiTables.init(); });</script>
    </body>
</html>
<script type="text/javascript">
	$( document ).ready(function() {
		$('.detelebtn').click(function() {
			$('#modal-fade').fadeIn("fast");
			var id=$(this).attr('id');
			var username=$(this).attr('username');
			$('#modal-fade .showuser').text(username);
			$('#modal-fade .myprocesdelete').attr('href','delete_examcenter.php?id='+id+'&delete=1');
		});
	});
</script>

<?php /*$DB -> close();*/ ?>