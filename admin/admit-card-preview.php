<?php require_once 'rightusercheck.php';?>
<?php
require_once '../meekrodb.2.3.class.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>IICD | Admit Card</title>

    <link rel="stylesheet" type="text/css" href="../css/stylead.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">



    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0-RC1/css/bootstrap-datepicker3.standalone.min.css'>
    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>


<body>
    <div class="container">

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="pull-left">
                    <!-- printDiv -->
                    <a id='btn' class="btn btn-primary" onclick='printDiv()'>Print Admit Card(s)</a>&nbsp;<a id='btn1'
                        class="btn btn-primary" onclick='previewDiv()'>Email Admit Card(s)</a>
                </div>
                <div class="pull-right">
                    <input type="checkbox" id="ckbCheckAll" /> Check/Uncheck All
                </div>
            </div>
        </div>
        <div class="row" id='DivIdToPrint'>
            <?php
$ids = $_GET['id'];
$uniqueIds = explode(",", $ids);
foreach ($uniqueIds as $id) {
    $id = base64_decode($id);
    ?>
            <!--#### CSS will be Used for Printing or Not Printing ####-->
            <style type="text/css">
                @media print {
                    .element-not-to-print {
                        display: none;
                    }
                }
            </style>
            <!-- Message will be printed here if mail sent or not starts -->
            <div class="alert alert-dismissible alert-primary" id="messageDiv_<?=$id;?>"></div>
            <!-- Message will be printed here if mail sent or not ends -->

            <div id="divIdtoPrint<?=$id;?>" class="col-md-12 admission-form divIdtoPrint element-not-to-print" style="border: 2px solid #c84242; padding: 0px; margin: 10px; ">
                <div class="container header-first" style="width: 1170px;padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;">

                    <input type="checkbox" name="chk" class="checkBoxClass element-not-to-print" id="checkbox<?=$id;?>"
                        value="<?=$id;?>" style="float: right; margin-top: 5px;" />
                    <!--##### Jquery to Detect Changes in State of Checkbox #####-->
                    <script>
                        $('#checkbox<?=$id;?>').change(function () {
                            if ($(this).is(":checked")) {
                                $("#divIdtoPrint<?=$id;?>").removeClass("element-not-to-print");
                            }
                            if (!$(this).prop("checked")) {
                                $("#divIdtoPrint<?=$id;?>").addClass("element-not-to-print");
                                $("#ckbCheckAll").prop("checked", false);
                            }
                        })
                    </script>
                    <!--##### Jquery to Detect Changes in State of Checkbox #####-->

                    <div class="mrg-btm" style="margin-bottom: 15px; width: 100%; float: left; border-top-left-radius: 5px; text-align: center; border-top-right-radius: 5px; padding: 0px; display: inline-flex;">
                        <div class="col-md-2 text-center" style="width: 16.66666667%; float: left; position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px; text-align: center;">
                            <div class="company-logo" style="width: 100%;
float: left;
padding: 25px 0px 2px 0px;">
                                <a href="#" style="">
                                    <img src="../images/logo1.png" alt="" style="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-7 text-center" style="text-align: center;
position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;
float: left;
width: 58.33333333%;">
                            <div class="company-logo" style="width: 100%;
float: left;
padding: 25px 0px 2px 0px;">
                                <a href="#" style="">
                                    <img src="../images/indian1.png" alt="" style="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 text-center" style="text-align: center;
position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;
float: left;
width: 25%;">
                            <div class="company-logo" style="width: 100%;float: left;">
                                <h2 style="margin: 0px; font-size: 40px; font-weight: 900;">
                                    ADMIT
                                </h2>
                                <h2 style="margin: 0px; font-size: 40px; font-weight: 900;">
                                    CARD
                                </h2>
                                <h4 style="font-size: 22px;">
                                    <?php $rightsideTitle = DB::queryFirstRow("SELECT * FROM settings WHERE id = 13");
    $titleRightSide = strip_tags($rightsideTitle['value']);
    echo $titleRightSide;?>

                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix" style="clear: both;"></div>
                <?php

    $sqlquery = "SELECT users.id AS UserID, users.*, user_details.Programme,user_details.fathers_name,user_details.dob,user_details.phone,user_details.exam_center1,user_details.applicant_photo,user_details.id_proof_front,user_details.id_proof_back,user_details.signatures,exam_centers.center_name,exam_centers.city  FROM users";

    $sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
    $sqlquery = $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city";
    $sqlquery = $sqlquery . " WHERE users.status = 1 AND users.id = '" . $id . "'";
    $result = DB::queryFirstRow($sqlquery);

    ?>
                <div class="admit-card-form" style="">
                    <div class="container" style="padding-right: 15px;
padding-left: 15px;
margin-right: auto;
margin-left: auto;
width: 1170px;">
                        <ul class="first-form" style="list-style: none;
margin-top: 0;
margin-bottom: 10px;
padding: 0px;
border-top: 1px solid #000;
border-left: 1px solid #000;">
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Name</span>
                                <span style="width: 40%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo $result['first_name'] . ' ' . $result['middle_name'] . ' ' . $result['last_name']; ?></strong></span>
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Enrollment
                                    No.</span>
                                <span style="width: 20%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;">
                                    <?php echo $result['enroll_id']; ?></span>
                            </li>
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Father's
                                    Name</span>
                                <span style="width: 40%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo $result['fathers_name']; ?></strong></span>
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Programme</span>
                                <span style="width: 20%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;">
                                    <?php echo $result['Programme']; ?></span>
                            </li>
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Date
                                    of Birth</span>
                                <span style="width: 40%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo date('d/m/Y', strtotime($result['dob'])); ?></strong></span>
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Mobile
                                    No.</span>
                                <span style="width: 20%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;">
                                    <?php echo $result['phone']; ?></span>
                            </li>
                            <!-- Phase wise condition as per the students apllicability -->
                            <?php
$phase1_attendence = $result['phase1_attendence'];
    $phase1_result = $result['phase1_result'];
    $phase2_attendence = $result['phase2_attendence'];
    $phase2_result = $result['phase2_result'];
    ?>
                            <!-- Build a query to select data from Settings Table -->


                            <?php
$date_of_entrance_test_parta = DB::queryFirstRow("SELECT * FROM settings WHERE id = 3");
    $date_of_entrance_test_partb = DB::queryFirstRow("SELECT * FROM settings WHERE id = 9");
    $parta_time_from_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 4");
    $parta_time_to_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 5");
    $partb_time_from_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 6");
    $partb_time_to_1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 7");
    $partb_time_from_2 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 8");
    $test_center_partb = DB::queryFirstRow("SELECT * FROM settings WHERE id = 10");
    $address_test_center_partb = DB::queryFirstRow("SELECT * FROM settings WHERE id = 11");

    if ($phase1_result == 1) {
        ?>
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Date
                                    of Entrance Test (Part B)</span>
                                <span style="width: 40%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo strip_tags($date_of_entrance_test_partb['value']); ?>
                                        <!-- 15/04/2018 -->

                                        <?php
} else {
        ?>
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style=" height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Date
                                    of Entrance Test (Part A)</span>
                                <span style=" width: 40%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo strip_tags($date_of_entrance_test_parta['value']); ?>
                                        <!-- 15/04/2018 -->

                                        <?php
}
    ?>
                                    </strong></span>
                                <?php if ($phase1_result == 1) {?>

                                <span style=" height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Test
                                    Center</span>
                                <span style=" width: 20%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;">
                                    <?php echo strip_tags($test_center_partb['value']); ?></span>
                            </li>
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Address
                                    of Test Center</span>

                                <span style="width: 80%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo strip_tags($address_test_center_partb['value']); ?></strong></span>



                                <?php } else {?>

                                <span style=" height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Test
                                    Center</span>
                                <span style=" width: 20%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;">
                                    <?php echo $result['center_name']; ?></span>
                            </li>
                            <li class="clearfix" style="display: flex; clear: both;
border-bottom: 1px solid #000;">
                                <span style="height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;

width: 20%;

background: #ebebeb;">Address
                                    of Test Center</span>
                                <?php $examcenter = "SELECT address FROM exam_centers WHERE city = '" . $result['exam_center1'] . "'";
        $centerName = DB::queryFirstRow($examcenter);

        ?>
                                <span style="width: 80%; height: 60px;

float: left;

min-height: 41px;

border-right: 1px solid #000;

padding: 10px;

display: block;

font-size: 15px;"><strong>
                                        <?php echo $centerName['address']; ?></strong></span>




                                <?php }?>





                            </li>
                        </ul>
                        <h3 style="font-family: inherit;
font-weight: 500;
line-height: 1.1;
color: inherit;
margin-top: 20px;
margin-bottom: 10px;
text-align: center;
font-size: 20px;">Entrance
                            Test Attendance:</h3>





                        <div class="secound-form-box clearfix" style="clear: both;

border: 1px solid #000;
margin-bottom: 20px; overflow: hidden; height: auto">
                            <ul class="clearfix" style="list-style: none;
margin-top: 0;
margin-bottom: 10px;
padding: 0px;

clear: both;


margin-bottom: 20px;
padding: 0px;
margin: 0px;
float: left;
width: 100%; display: flex;">

                                <!-- Decide if student is applicable for phase 1 only -->
                                <?php
if ($phase1_result == 1) {
        ?>

                                <li style="float: left;
width: 50%;
font-size: 16px;
padding: 10px;
border-right: 1px solid #000;
border-top: 1px solid #000;
text-align: center;
border-top: 0px;">
                                    <h1 style="margin: .67em 0;

margin-top: 0.67em;
margin-bottom: 0.67em;

font-size: 2em;

font-family: inherit;

font-weight: 500;

line-height: 1.1;

color: inherit;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

font-size: 26px;

text-align: center;">Part
                                        B-1</h1>
                                    <p style="margin: 0 0 10px;

font-size: 14px;

text-align: center;">Material, Color
                                        & Conceptual Test</p>
                                    <h2 style="font-family: inherit;

font-weight: 500;

line-height: 1.1;

color: inherit;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

font-size: 26px;

text-align: center;

font-size: 16px;

text-align: center;">(
                                        <?php echo strip_tags($partb_time_from_1['value']); ?> to
                                        <?php echo strip_tags($partb_time_to_1['value']); ?>)</h2>

                                    <h4 style="clear: both;text-align: center;">Invigilator Initial</h4>
                                </li>
                                <li style="float: left;

width: 50%;

font-size: 16px;

padding: 10px;

border-right: 1px solid #000;

border-top: 1px solid #000;

text-align: center;

border-top: 0px;">
                                    <h1 style="margin: .67em 0;

margin-top: 0.67em;
margin-bottom: 0.67em;

font-size: 2em;

font-family: inherit;

font-weight: 500;

line-height: 1.1;

color: inherit;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

font-size: 26px;

text-align: center;">Part
                                        B-2</h1>
                                    <p style="margin: 0 0 10px;

font-size: 14px;

text-align: center;">Personal
                                        Interview</p>
                                    <h2 style="font-family: inherit;

font-weight: 500;

line-height: 1.1;

color: inherit;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

font-size: 26px;

text-align: center;

font-size: 16px;

text-align: center;">(
                                        <?php echo strip_tags($partb_time_from_2['value']); ?> Onwards)</h2>
                                    <h4 style="clear: both;text-align: center;">Invigilator Initial</h4>
                                </li>
                                <!-- <li style="width: 50%"></li>
              <li style="width: 50%"><h4 style="">Invigilator Initial</h4></li> -->
                                <?php
} else {
        ?>


                                <li style="float: left;

width: 100%;

font-size: 16px;

padding: 10px;

border-right: 1px solid #000;

border-top: 1px solid #000;

text-align: center;

border-top: 0px;">
                                    <h1 style="margin: .67em 0;

margin-top: 0.67em;
margin-bottom: 0.67em;

font-size: 2em;

font-family: inherit;

font-weight: 500;

line-height: 1.1;

color: inherit;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

font-size: 26px;

text-align: center;">Part
                                        A</h1>
                                    <p style="margin: 0 0 10px;

font-size: 14px;

text-align: center;">General
                                        Awareness, Creativity & Perception Test</p>
                                    <h2 style="font-family: inherit;

font-weight: 500;

line-height: 1.1;

color: inherit;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

margin-top: 20px;

margin-bottom: 10px;

text-align: center;

font-size: 20px;

font-size: 26px;

text-align: center;

font-size: 16px;

text-align: center;">(
                                        <?php echo strip_tags($parta_time_from_1['value']); ?> to
                                        <?php echo strip_tags($parta_time_to_1['value']); ?>)</h2>
                                    <h4 style="clear: both;text-align: center;">Invigilator Initial</h4>
                                </li>


                                <!-- <li style="width: 50%"></li> -->



                                <?php
}
    ?>





                            </ul>
                        </div>
                        <div class="secound-form-photo" style="border: 1px solid #000;
margin-bottom: 20px;">
                            <ul style="list-style: none;
padding: 0px;
margin: 0px;
float: left;
width: 100%; display: flex; clear: both;">
                                <li style="float: left;
width: 25%;
height: 270px;
font-size: 16px;
padding: 20px;
border-right: 1px solid #000;
border-top: 1px solid #000;
text-align: center;
border-top: 0px;"><img
                                        src="../images/<?php echo $result['applicant_photo']; ?>" width="180" height="240"
                                        alt="Applicant Photo" /></li>
                                <li style="float: left;
width: 25%;
height: 270px;
font-size: 16px;
padding: 20px;
border-right: 1px solid #000;
border-top: 1px solid #000;
text-align: center;
border-top: 0px;"><img
                                        src="../images/<?php echo $result['signatures']; ?>" width="180" height="240"
                                        alt="Applicant Signature" /></li>
                                <li style="float: left;
width: 25%;
height: 270px;
font-size: 16px;
padding: 20px;
border-right: 1px solid #000;
border-top: 1px solid #000;
text-align: center;
border-top: 0px;"><img
                                        src="../images/<?php echo $result['id_proof_front']; ?>" width="180" height="240"
                                        alt="ID Proof (Front)" /></li>
                                <li style="float: left;
width: 25%;
height: 270px;
font-size: 16px;
padding: 20px;
border-right: 1px solid #000;
border-top: 1px solid #000;
text-align: center;
border-top: 0px;"><img
                                        src="../images/<?php echo $result['id_proof_back']; ?>" width="180" height="240"
                                        alt="ID Proof (Back)" /></li>
                            </ul>
                            <div class="clearfix" style="clear: both;"></div>
                        </div>

                        <h5 style="font-family: inherit;
font-weight: 500;
line-height: 1.1;
color: inherit;
text-align: right;
font-size: 18px;
margin: 40px 0;
position: relative;">Deputy
                            Registration Academics

                            <img src="../images/signature.png" alt="signature" /> </h5>

                        <?php $settings = "SELECT * FROM settings WHERE id = 1";
    $settingsName = DB::queryFirstRow($settings);

    ?>

                        <?php echo $settingsName['value']; ?>
                    </div>
                </div>
                <div class="clearfix" id="sendadmit" style=""></div>
                <hr>

            </div>


            <?php
}
?>
        </div>
    </div>
    <script type="text/javascript">
        function SendAdmit(id) {
            console.log(id + "New");
            // $("#sendadmit").val("sending...");
            // $("#sendadmit").attr("disabled", "disabled");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "admit-cardnew.php",
                data: "id=" + id,
                success: function (response) {
                    console.log(response.message);
                    $('#messageDiv_' + id).append('<div id="innermessageDiv_' + id +
                        '" class="col-md-12" style="margin: 0px; padding: 0px;">' + response.message +
                        '</div></div>');
                    //   location.reload();
                }
            });
        }





        //########## function to print admit cards starts
        function printDiv() {
            console.log("Yes Checked In !!");
            //Checking if none is check alert the user to check a single checkbox at least
            var countCheck = ($("input[name='chk']:checked").length);
            if (countCheck == 0) {
                alert("Please check at least one checkbox to Print Admit Card(s) !!");
            } else {
                var divToPrint = document.getElementById('DivIdToPrint');
                var newWin = window.open('', 'Print-Window');
                newWin.document.open();
                newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
                newWin.document.close();
                setTimeout(function () {
                    newWin.close();
                }, 1000);
            }
        }
        //########## function to print admit cards ends

        //########## Function to get all the checked ID(s) for Preview Admit Cards starts

        function previewDiv() {
            //Checking if none is check alert the user to check a single checkbox at least
            var countCheck = ($("input[name='chk']:checked").length);
            if (countCheck == 0) {
                alert("Please check at least one checkbox to Send Admit Card via Email !!");
            } else {
                $.each($("input[name='chk']:checked"), function () {
                    console.log($(this).val());
                    return SendAdmit($(this).val());
                });
            }
        }

        //########## Function to get all the checked ID(s) for Preview Admit Cards ends


        //########## Check Uncheck All Checkbox Jquery ##########//
        $(document).ready(function () {
            $("#ckbCheckAll").click(function () {
                $(".checkBoxClass").prop('checked', $(this).prop('checked'));
                if (!$(this).prop("checked")) {
                    $(".divIdtoPrint").addClass("element-not-to-print");
                    $("#ckbCheckAll").prop("checked", false);
                } else {
                    $(".divIdtoPrint").removeClass("element-not-to-print");

                }
            });
        });
        //########## Check Uncheck All Checkbox Jquery ##########//
    </script>

</body>

</html>