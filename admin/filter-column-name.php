<?php 
  session_start();
  require_once('../meekrodb.2.3.class.php');
$fieldName = $_GET['fieldName'];
if($fieldName != '')
{
?>
 
<div class="form-group col-md-5" style="display: flex;">
<input type="checkbox" name="chk_<?=$fieldName;?>" value="<?=$fieldName;?>" checked="checked" class="pull-left"/>&nbsp;
<select id="sel_<?=$fieldName;?>" name="sel_<?=$fieldName;?>" class="form-control">
<option value="~">contains</option>
<option value="!~">doesn't contain</option>
<option value="!*">none</option>
<option value="*">any</option>
</select>
</div>

<div class="form-group col-md-7">
<input class="form-control" placeholder="<?=$fieldName;?>" name="txt_<?=$fieldName;?>" type="text" value="">
</div>
<?php } ?>