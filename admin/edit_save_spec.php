<?php require_once('rightusercheck.php');?>
<?php if(!empty($_SESSION['adminyncrights']) && trim($_SESSION['adminyncrights']) != "Administrator") { header('Location : index.php'); }?>
<?php 
    //require_once('image-resizer.php');
    require_once('../meekrodb.2.3.class.php'); 
?>
<?php  
    if(isset($_POST['submit']))
    {
        $id = $_POST['id'];
        $seats = (!empty($_POST['seats'])) ? (int)$_POST['seats'] : 0;
        $duration = (!empty($_POST['duration']))? trim($_POST['duration']): null;
        $cname = (!empty($_POST['cname'])) ? trim($_POST['cname']) : null;
        $sname = (!empty($_POST['sname']))? trim($_POST['sname']): null;
        
       $qry = DB::update('specialisations', array(
                      'sname' => $sname,
                      'cname' => $cname,
                      'duration' => $duration,
                      'seats' => $seats,
                      ), "id=%i", $id);
       if ($qry == TRUE)
        {
            echo "<script language='javascript'>alert('Specialization is updated successfully');window.location = 'specializations_list.php';</script>";
        }
        else
        {
            echo "<script language='javascript'>history.go(-1);</script>";   
        }
    }
?>
