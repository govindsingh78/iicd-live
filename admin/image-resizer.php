<?php 
class ResizeImage {
 
   var $image;
   var $image_type;
   var $image_new_name;
   var $image_rand_id;
   var $image_file_ext;
 
   function load($filename) {
 
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      } else {
         trigger_error('Unsupported filetype!', E_USER_WARNING);
      }
      
   }
   
   function newFileName($filename, $maxid, $midname){
        $image_file_ext = substr($filename,strripos($filename, '.'));
        $image_new_name = $maxid.$midname.$image_file_ext;
        return $image_new_name;
   }
   
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=100, $permissions=null) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      } else {
         trigger_error('Failed resize image!', E_USER_WARNING);
      }
      
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      } 
   }
   
   function getWidth() { 
      return imagesx($this->image);
   }
   
   function getHeight() { 
      return imagesy($this->image);
   }
   
   function resizeToHeight($height) { 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }
   
   /*
   function resize($width, $height) {
         //Check if GD extension is loaded
         if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            trigger_error("GD is not loaded", E_USER_WARNING);
            return false;
         }
         
         //If image dimension is smaller, do not resize
         if ($this->getWidth() <= $width && $this->getHeight() <= $height) {
            $nHeight = $this->getHeight();
            $nWidth = $this->getWidth();
         } else {
            //yeah, resize it, but keep it proportional
              if ($width/$this->getWidth() > $height/$this->getHeight()) {
                $nWidth = $width;
                $nHeight = $this->getHeight()*($width/$this->getWidth());
              } else {
                $nWidth = $this->getWidth()*($height/$this->getHeight());
                $nHeight = $height;
              }
         }
         $nWidth = round($nWidth);
         $nHeight = round($nHeight);
         
         $new_image = imagecreatetruecolor($nWidth, $nHeight);
         
         if(($this->image_type == IMAGETYPE_GIF) OR ($this->image_type == IMAGETYPE_PNG)){
            imagealphablending($new_image, false);
            imagesavealpha($new_image,true);
            $transparent = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
            imagefilledrectangle($new_image, 0, 0, $nWidth, $nHeight, $transparent);
         }
         imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $nWidth, $nHeight, $this->getWidth(), $this->getHeight());
         
         $this->image = $new_image;
   }
   */
   function delete($filepath) {
      if(file_exists($filepath) && is_writable($filepath)){
        unlink($filepath);    
      }
   }      
 
}
?>