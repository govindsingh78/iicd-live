<?php
session_start();
include 'meekrodb.2.3.class.php';

$query = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";

$row = DB::queryFirstRow($query);

$centers = DB::query("select * from exam_centers");

?>
<form id="form_exam" name="form_exam">
 <div class="my-dtl-feed">
<div class="col-md-12">
<div class="group">
<div class="col-md-4">
  <div class="my-input-bx field required-field">
      <div class="selectContainer">
          <label class="my-label">Center Name First Choice
          </label>
           <span class="bar"></span>
          <select id="exam_center1" name="exam_center1" class="form-control">
            <option value="">Select Center</option>
              <?php
foreach ($centers as $val) {
    $selected = '';
    if ($val['city'] == $row['exam_center1']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $val['city'] . '" ' . $selected . '>' . $val['city'] . '</option>';
}
?>
          </select>
       </div>
   </div>
</div>

<div class="col-md-4">
<div class="my-input-bx field required-field">
    <div class="selectContainer">
        <label class="my-label">Center Name Second Choice
        </label>
         <span class="bar"></span>
        <select id="exam_center2" name="exam_center2" class="form-control">
        <option value="">Select Center</option>
               <?php
foreach ($centers as $val) {
    $selected = '';
    if ($val['city'] == $row['exam_center2']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $val['city'] . '" ' . $selected . '>' . $val['city'] . '</option>';
}
?>
        </select>
     </div>
 </div>
</div>

<div class="col-md-4 my-select">
<div class="my-input-bx field required-field">
    <div class="selectContainer">
        <label class="my-label">Center Name Third Choice
        </label>
         <span class="bar"></span>
        <select id="exam_center3" name="exam_center3" class="form-control">
        <option value="">Select Center</option>
              <?php
foreach ($centers as $val) {
    $selected = '';
    if ($val['city'] == $row['exam_center3']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $val['city'] . '" ' . $selected . '>' . $val['city'] . '</option>';
}
?>
        </select>
     </div>
 </div>
</div>
<nav class="form-section-nav">
  <input type="hidden" name="action" id="action" value="save_exam">
  <span id="btn_back_exam" class="btn-secondary form-nav-prev"><img src="images/left-arrow.jpg" alt="left"> Prev</span>
  <span id="btn_next_exam" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span><div class="loader" style="position: fixed; top: 35%; left: 48%;"></div>
</nav>
</div>
</div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function(){

    $("#btn_back_exam").unbind().click(function() {
      $('#address_container').load('form_address.php',function(e){
          $("#exam_container" ).slideUp( "slow");
          $('#exam_container').html('');
          //$("#address_container" ).slideDown( "slow");
          $("#address_container" ).slideDown( "slow", function(e) {
              window.scrollTo(0,500);
          });
      });
    });

    $("#btn_next_exam").unbind().click(function() {

        if(!$('#form_exam').valid()){
          return false;
        }

      //  var formData = new FormData($('form#form_exam')[0]);
var formData = $('form#form_exam').serialize();

        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            beforeSend: function() {
                    $('.loader').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
            },
            success: function(response) {
              if(response.status == 1){
                $('#hs_container').load('form_hs.php',function(e){
                  $("#exam_container" ).slideUp( "slow");
                  $('#exam_container').html('');
                  //$("#hs_container" ).slideDown( "slow");
                  $("#hs_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,700);
                     });
                });
              }
            }
        });

    });

    $('#form_exam').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "exam_center1": {
            required: true
          },
          "exam_center2": {
            required: true
          },
          "exam_center3": {
            required: true
          }

        },
        messages:
        {
         "exam_center1": {
            required: "Exam center is required"
          },
          "exam_center2": {
            required: "Exam center is required"
          },
          "exam_center3": {
            required: "Exam center is required"
          }
        }
  });
$('select').on('change', function(event ) {
   var prevValue = $(this).data('previous');
$('select').not(this).find('option[value="'+prevValue+'"]').show();
   var value = $(this).val();
  $(this).data('previous',value); $('select').not(this).find('option[value="'+value+'"]').hide();
});
$('select').trigger('change');

});
</script>