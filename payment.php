<?php
session_start();
include 'meekrodb.2.3.class.php';
if (!isset($_SESSION['user_id'])) {
    header('location: admission.php');
}
$sql = "SELECT users.*, user_details.* FROM users";
$sql = $sql . " LEFT JOIN user_details ON user_details.user_id = users.id";
$sql = $sql . " WHERE user_details.user_id = '" . $_SESSION['user_id'] . "'";

$data = DB::queryFirstRow($sql);

###### Update Users Position on Registeration Page
$trackingNo = $data['signup_tracking'];
$update_tracker['signup_tracking'] = 8;
$update_tracker['declaration'] = 1;
//if ($trackingNo < 7) {
$updateTracker = DB::update('user_details', $update_tracker, "user_id=%s", $_SESSION['user_id']);
//}
###### Update Users Position on Registeration Page

#########- Generate Enrollment ID Starts -#########
// $userEnrolled = $_SESSION['user_id'];
// generateEnrollmentId($userEnrolled);

// #########- Function to Generate Enrollment ID Starts -#########
// function generateEnrollmentId($userEnrolled)
// {
//     $sqlquery = "SELECT users.id AS UserID,users.status, user_details.Programme, user_details.exam_center1,exam_centers.city,exam_centers.center_code FROM users";
//     $sqlquery = $sqlquery . " LEFT JOIN user_details ON user_details.user_id = users.id";
//     $sqlquery = $sqlquery . " LEFT JOIN exam_centers ON user_details.exam_center1 = exam_centers.city where users.id='" . $_SESSION['user_id'] . "'";
//     $sqlquery = DB::queryFirstRow($sqlquery);
//     $enrollmentExist = $sqlquery['enroll_id'];
//     //if(empty($enrollmentExist)){
//     ######- Comparing the Params Exist in Enrollment ID -#####
//     $center_code = $sqlquery['center_code'];
//     $programme = $sqlquery['Programme'];
//     $id = $sqlquery['UserID'];
//     $user_id = str_pad($id, 4, '0', STR_PAD_LEFT);
//     $year = date("y");
//     $likeEnroll = $programme . "" . $year . "" . $center_code;
//     $sql = "SELECT * FROM users where enroll_id like '%$likeEnroll%'";
//     $sql = DB::query($sql);
//     $countsql = DB::count($sql);
//     $centerCodeVirtualId = $countsql + 1;
//     $centerCodeVirtualId_Formatted = str_pad($centerCodeVirtualId, 3, '0', STR_PAD_LEFT);
//     //$enrollmentPG = $programme.$year.$center_code.$user_id.'-'.$centerCodeVirtualId_Formatted;
//     //New Enrollment ID format removing userid
//     $enrollmentPG = $programme . $year . $center_code . '-' . $centerCodeVirtualId_Formatted;
//     if ($enrollmentExist !== $enrollmentPG) {
//         $update = DB::update('users', array('enroll_id' => $enrollmentPG), "id=%i", $id);
//     }

// }
//}

//print_r($_SESSION);
// Merchant key here as provided by Payu
$MERCHANT_KEY = "tUIPrE";

// Merchant Salt as provided by Payu
$SALT = "DH32f67b";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

if ($data['nationality'] == "India") {
    $paisa = 1500;
} else {
    $paisa = 3000;
}

$tid = mt_rand(111111, 999999) . time();

$key = $MERCHANT_KEY;
$txnid = $tid;
//$amount = 1;
$amount = $paisa;
$productinfo = "Admission Fee";
$firstname = $data['first_name'];
$email = $data['email'];
$udf1 = "";
$udf2 = "";
$udf3 = "";
$udf4 = "";
$udf5 = "";
$udf6 = "";
$udf7 = "";
$udf8 = "";
$udf9 = "";
$udf10 = "";

$phone = $data['phone'];
$lastname = $data['lastname'];
$address1 = $data['address1'];
$address2 = $data['address2'];
$zipcode = $data['pin_code'];

$success_url = "https://www.iicd.ac.in/success.php";
$failed_url = "https://www.iicd.ac.in/failure.php";
$cancel_url = "https://www.iicd.ac.in/admission.php";
$service_provider = "payu_paisa";

$hash = '';
//---------------------

$hasTemp = $key . "|" . $txnid . "|" . $amount . "|" . $productinfo . "|" . $firstname . "|" . $email . "|" . $udf1 . "|" . $udf2 . "|" . $udf3 . "|" . $udf4 . "|" . $udf5 . "|" . $udf6 . "|" . $udf7 . "|" . $udf8 . "|" . $udf9 . "|" . $udf10;
$hasTemp .= "|" . $SALT;
$hash = strtolower(hash('sha512', $hasTemp));

//------------------------
$action = $PAYU_BASE_URL . '/_payment';
?>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>IICD | Register Form</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />

<!-- Analytics Code -->
<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-75923092-1', 'auto');  ga('send', 'pageview'); </script>
  <!-- Facebook Pixel Code -->
  <script>
  ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
          n.callMethod ?
              n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
  }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1151196271591907');
  fbq('track', 'PageView');
  </script>
  <noscript>
      <img height="1" width="1" src="https://www.facebook.com/tr?id=1151196271591907&ev=PageView
&noscript=1" />
  </noscript>
  <!-- End Facebook Pixel Code -->
<body>

    <div class="admission-form">
   	 	<div class="container">
     		 <div class="form-pay">
                <div class="pay-sec">
                    <div class="pay-heading">
                        <div class="col-md-6">
                             Your are Paying to
                          </div>

                         <div class="col-md-3">
                             Amount
                        </div>
                        <div class="col-md-3">
                             <a href="logout.php">
                            <i class="fa fa-power-off fa-fw pull-right"></i>
                            Log out
                        </a>
                        </div>
                    </div>
                    <div class="pay-body">
                    	<div class="col-md-6  pay-logo">
                             <img src="images/company-logo1.png" alt="logo">
                          </div>

                         <div class="col-md-6 pay-logo">
                             <i class="fa fa-rupee"></i><?php echo $paisa; ?>
                        </div>
                    </div>

                    <div class="pay-footer">
                      <form action="<?php echo $action ?>" method="post" name="payuForm">
                         <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                          <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                          <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                          <input type="hidden" name="amount" value="<?php echo $amount ?>" />
                          <input type="hidden" name="firstname" value="<?php echo $firstname ?>" />
                          <input type="hidden" name="email" value="<?php echo $email ?>" />
                          <input type="hidden" name="phone" value="<?php echo $phone ?>" />
                          <input type="hidden" name="productinfo" value="<?php echo $productinfo ?>" />
                          <input type="hidden" name="surl" value="<?php echo $success_url ?>" />
                          <input type="hidden" name="furl" value="<?php echo $failed_url ?>" />
                          <input type="hidden" name="service_provider" value="payu_paisa" />
                    	<div class="col-md-12 text-center" style="display: none">
                            <button id="button" onclick="ga('send', 'event', 'button', 'click', 'payment');" type="submit" class="ripple dwnlod-btn">Make Payment</button>
                      </div>
                      </form>
                        <form>
                        <div class="col-md-12 text-center">
                            <a id="buttonPreview" onclick="updatetracker();" type="submit" class="ripple dwnlod-btn" style="color: #FFF !important; cursor: pointer">Make Payment</a>
                        </div>
                        </form>


                    </div>
                </div>
            </div>
    	</div>
    </div>



<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/selecters.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script>
function updatetracker(){
   // console.log("Govind");
    	 $.ajax({
          type: "POST",
          url: "updatetracker.php",
          dataType: "json",
          data: "usersteps=8",
          success: function (response) {
           console.log(response);
           $('#button').trigger('click');
          }
      	});
}


// $(document).ready(function(){
//     $("#button").click(function(){
//         $.ajax({
//             url: "updatetracker.php",
//             type: "GET",
//             data: "usersteps=9",
//             success: function(response){
//                 console.log(response);
//                 $('#button').find('button').trigger('click');

//             }
//         });
//     });
// });


</script>


</body>
</html>
