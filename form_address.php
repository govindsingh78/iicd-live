<?php
session_start();
error_reporting(12);
include 'meekrodb.2.3.class.php';

$query = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";

$row = DB::queryFirstRow($query);

$countries = DB::query("select * from country");

// Dynamic value for images section

$base_url = 'https://www.iicd.ac.in/';

$proofidcards = $row['identity_card'];

if ($proofidcards == "UID Aadhar") {
    $id_src_front = $base_url . 'images/id_proof_front/uidaadharfront.jpg';
    $id_src_back = $base_url . 'images/id_proof_back/uidaadharback.jpg';
} else if ($proofidcards == "Driver\'s License") {
    $id_src_front = $base_url . 'images/id_proof_front/dlfront.jpg';
    $id_src_back = $base_url . 'images/id_proof_back/dlback.jpg';
} else if ($proofidcards == "Passport") {
    $id_src_front = $base_url . 'images/id_proof_front/passportfront.jpg';
    $id_src_back = $base_url . 'images/id_proof_back/passportback.jpg';
} else if ($proofidcards == "Voter ID") {
    $id_src_front = $base_url . 'images/id_proof_front/voteridfront.jpg';
    $id_src_back = $base_url . 'images/id_proof_back/voteridback.jpg';
} else {
    $id_src_front = $base_url . 'images/id_proof_front/defaultfront.png';
    $id_src_back = $base_url . 'images/id_proof_back/defaultback.png';
}

if ($row['id_proof_front'] != '') {
    $id_src_front = $base_url . 'images/' . $row['id_proof_front'];
}
if ($row['id_proof_back'] != '') {
    $id_src_back = $base_url . 'images/' . $row['id_proof_back'];
}

$idcards = array('UID Aadhar' => 'UID Aadhar', 'Driver\'s License' => 'Driver\'s License', 'Passport' => 'Passport', 'Voter ID' => 'Voter ID');

?>
<form id="form_address" name="form_address">
   <div class="my-dtl-feed">
      <div class="col-md-12">
        <p> Permanent Address </p> <br/>
        <div class="group">
            <div class="col-md-4">
             <div class="my-input-bx field required-field">


                <input type="text" id="address_line1" name="address_line1" class="form-control" value="<?=$row['address_line1']?>">
                <span class="bar"></span>
                <label>House No./Flat No./Building Name</label>
             </div>
            </div>


          <div class="col-md-4">
             <div class="my-input-bx field">

                <input type="text" id="block_or_tehsil" name="block_or_tehsil" class="form-control" value="<?=$row['block_or_tehsil']?>">
                <span class="bar"></span>
                <label> Village/Block/Tehsil/Locality/Landmark</label>
             </div>
            </div>

            <div class="col-md-4">  
                  <div class="my-input-bx field required-field">    
                    <div class="selectContainer"> 
                        <label class="my-label">Country</label>
                         <span class="bar"></span>
                        <select name="country" class="form-control" id="country">  
                         <option value="">Select Country</option>
                         <?php
                          foreach ($countries as $val) {
                              $selected = '';
                              if($val['id']==$row['country']){
                                  $selected = 'selected="selected"';
                              }
                              echo '<option value="'.$val['id'].'" '.$selected.'>'.$val['country_name'].'</option>';
                          }
                          ?>
                        </select>
                     </div>
                  </div>
            </div>

         </div>

<div class="group">





  <div class="col-md-4">
             <div class="my-input-bx field required-field">
              <div class="selectContainer">
                  <label class="my-label">State</label>
                   <span class="bar"></span>
                   <select id="state" name="state" class="form-control">
                    <option value="">Select State</option>
                  </select>
               </div>
              </div>
            </div>

 <div class="col-md-4">
                <div class="my-input-bx field required-field">
                  <div class="selectContainer">
                      <label class="my-label">City</label>
                       <span class="bar"></span>
                       <select name="city" class="form-control" id="city">
                        <option value="">Select City</option>
                      </select>

                   </div>
                </div>
            </div>


              <div class="col-md-4">
             <div class="my-input-bx field required-field">

                <input type="text" id="pin_code" name="pin_code" value="<?=$row['pin_code']?>" class="form-control">
                <span class="bar"></span>
                <label> Pin Number</label>

             </div>
            </div>
</div>



                          <?php
$check_correspondece_address = $row['addresscheck'];
if ($check_correspondece_address == 0) {
    $checked_status = "";
} else {
    $checked_status = "checked='checked'";
}
?>
      <p>Correspondence  address</p><br/>
        <div class="group">
	      	<div class="col-md-8">
	      	  <input type="checkbox" id="addresscheck" name="addresscheck" value="1" <?php echo $checked_status; ?> />  Check this box if Permanent Address and Correspondence Address are the same.
			</div>
        </div>	<br><br/>	<br>

        <div class="group"><br/>
            <div class="col-md-4">
             <div class="my-input-bx field required-field">
                <input type="text" id="caddress_line1" name="caddress_line1" class="form-control" value="<?=$row['caddress_line1']?>">
                <span class="bar"></span>
                <label>House No./Flat No./Building Name</label>
             </div>
            </div>











                            <div class="col-md-4">
             <div class="my-input-bx field">
                <input type="text" id="cblock_or_tehsil" name="cblock_or_tehsil" class="form-control" value="<?=$row['cblock_or_tehsil']?>">
                <span class="bar"></span>
                <label>Village/Block/Tehsil/Locality/Landmark</label>
             </div>
            </div>

            <div class="col-md-4">  
                  <div class="my-input-bx field required-field">    
                    <div class="selectContainer"> 
                        <label class="my-label">Country</label>
                         <span class="bar"></span>
                        <select name="ccountry" class="form-control" id="ccountry">  
                         <option value="">Select Country</option>
                         <?php
                          foreach ($countries as $val) {
                              $selected = '';
                              if($val['id']==$row['country']){
                                  $selected = 'selected="selected"';
                              }
                              echo '<option value="'.$val['id'].'" '.$selected.'>'.$val['country_name'].'</option>';
                          }
                          ?>
                        </select>
                     </div>
                  </div>
            </div>

          </div>
          <div class="group">






            <div class="col-md-6">
             <div class="my-input-bx field required-field">
              <div class="selectContainer">
                  <label class="my-label">State</label>
                   <span class="bar"></span>
                   <select id="cstate" name="cstate" class="form-control">
                    <option value="">Select State</option>
                  </select>
               </div>
              </div>
            </div>


            <div class="col-md-6">
                <div class="my-input-bx field required-field">
                  <div class="selectContainer">
                      <label class="my-label">City</label>
                       <span class="bar"></span>
                       <select name="ccity" class="form-control" id="ccity">
                        <option value="">Select City</option>
                      </select>

                   </div>
                </div>
            </div>



         </div>
        <div class="group">


            <div class="col-md-4">
             <div class="my-input-bx field required-field">
                <input type="text" id="cpin_code" name="cpin_code" value="<?=$row['cpin_code']?>" class="form-control">
                <span class="bar"></span>
                <label> Pin Number</label>
             </div>
            </div>


        <!-- Placing first 2 image upload section Here starts -->



     <div class="col-md-8">
              <div class="my-input-bx field required-field">
              <div class="selectContainer">
              <span class="bar"></span>
                <label class="my-label"> Identity Proof </label>
                  <select id="identity_card" name="identity_card" class="form-control">
                    <option value="">Select ID Type</option>
                      <?php
foreach ($idcards as $key => $val) {
    $selected = '';
    if ($key == $row['identity_card']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
                  </select>
                </div>
              </div>
        </div>





    </div>




<div class="group">
<div class="col-md-12">
  <div id="error_file_name" class="col-md-12"></div>
  <div id="error_file_name1" class="col-md-12"></div>
</div>
</div>


<br/>

        <div class="group" id="showhideidentitycard" style="display: none;">

        <div class="col-md-4">
        <div class="my-input-bx field required-field">
        <input type="text" id="identity_card_no" name="identity_card_no" value="<?=$row['identity_card_no']?>" class="form-control">
        <span class="bar"></span>
        <label><span class="writeIdCardName"> </span> No.</label>
        </div>
        </div>

        <div class="col-md-4">
        <label class="my-label" style="font-size: 10px"><span class="writeIdCardName"> </span>  Front View</label>
        <p class="pho-txt"> The photograph should of minimum 100 kb and maximum 200 kb size. Supported file format JPG, JPEG. </p>
        <div id="createDynamicElement">
        <input type='file' id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg"  />
       </div>
        <div id='img_contain3_front'>
        <img id="blah3_front" align='middle' src="<?=$id_src_front?>" style="width: 275px; height: 200px"/>
        </div>
        </div>

        <div class="col-md-4">
        <label class="my-label" style="font-size: 10px"><span class="writeIdCardName"> </span>  Back View</label>
        <p class="pho-txt"> The photograph should of minimum 100 kb and maximum 200 kb size. Supported file format JPG, JPEG. </p>
        <div id="createDynamicElement1">
        <input type='file' id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg"  />
        </div>
        <div id='img_contain3_back'>
        <img id="blah3_back" align='middle' src="<?=$id_src_back?>" style="width: 275px; height: 200px"/>
        </div>
        </div>

        </div>
        <div id="writeFlag" style="display: none"></div>

<!-- Placing first 2 image upload section Here ends -->

      <nav class="form-section-nav">
        <input type="hidden" name="action" id="action" value="save_address">
        <input type="hidden" name="hidden_idname" id="hidden_idname" value="<?=$row['identity_card']?>">
        <input type="hidden" name="hidden_idno" id="hidden_idno" value="<?=$row['identity_card_no']?>">
       <input type="hidden" name="hidden_id_front" id="hidden_id_front" value="<?=$row['id_proof_front']?>">
       <input type="hidden" name="hidden_id_back" id="hidden_id_back" value="<?=$row['id_proof_back']?>">
       <input type="hidden" name="file_extension_allow" id="file_extension_allow" value="1">
        <span id="btn_back_address" class="btn-secondary form-nav-prev"><img src="images/left-arrow.jpg" alt="left"> Prev</span>
        <span id="btn_next_address" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span><div class="loader" style="position: fixed; top: 35%; left: 48%;"></div>
      </nav>

  </div>
  </div>
</div>
</form>
<style type="text/css">
  #error_file_name{
    color: red;
    font-size: 12px;
    margin-top: -55px;
    letter-spacing: 3px;
    font-style: oblique;
    font-weight: bold;
    clear: both;
  }
  #error_file_name1 {
    color: red;
    font-size: 12px;
    margin-top: -40px;
    letter-spacing: 3px;
    font-style: oblique;
    font-weight: bold;
    clear: both;
}
</style>



<script type="text/javascript">
$(document).ready(function(){

          var countryid = 101;
          if(countryid !=''){
          $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
          $('#state').html(response.data);
          }
          });
          }

          var ccountryid = 101;
          if(ccountryid !=''){
          $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + ccountryid,
          success: function (response) {
          $('#cstate').html(response.data);
          }
          });
          }


        var identity_card= $("#identity_card").val();



        if(identity_card !== ""){
        $('#showhideidentitycard').show();
        $('#writeFlag').text('false');
        $('.writeIdCardName').text(identity_card);
        }else{
          $('#writeFlag').text('true');
        $('#showhideidentitycard').hide();
        }


        $('#identity_card').change(function(){

        var hidden_id_front = $('#hidden_id_front').val();
        var hidden_id_back = $('#hidden_id_back').val();

        console.log(hidden_id_front + 'Test' + hidden_id_back);


        var hidden_idname = $('#hidden_idname').val();
        var identity_card= $(this).val();
        if(identity_card == hidden_idname){


        $('#writeFlag').text('false');

        }else{

        $('#writeFlag').text('true');
        }


         if(identity_card == ''){
            $('#showhideidentitycard').hide();
         }

        var hidden_idno = $('#hidden_idno').val();



        //change the image src url respectively using jquery
        $('#img_contain3_front').html('');
        $('#img_contain3_back').html('');
        $("#identity_card_no").val('');
        $('.writeIdCardName').text(identity_card);

         console.log(identity_card);
        if(identity_card == "" && identity_card !== hidden_idname){
        $('#showhideidentitycard').hide();

        // $("#imgInp3_front").val();
        // $("#imgInp3_back").val();


        $('#createDynamicElement1').html('<input type="file" id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg"/>');
        $('#createDynamicElement').html('<input type="file" id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg"/>');

        }else if(identity_card == "UID Aadhar" && identity_card !== hidden_idname){
        $('#showhideidentitycard').show();

        // $("#imgInp3_front").val();
        // $("#imgInp3_back").val();


        //  $('#createDynamicElement1').html('<input type="file" id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg"/>');
        // $('#createDynamicElement').html('<input type="file" id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg"/>');

        $('#img_contain3_front').html('<img id="blah3_front" align="middle" src="<?=$base_url?>images/id_proof_front/uidaadharfront.jpg" style="width: 275px; height: 200px"/>');
        $('#img_contain3_back').html('<img id="blah3_back" align="middle" src="<?=$base_url?>images/id_proof_back/uidaadharback.jpg" style="width: 275px; height: 200px"/>');


        }
        else if(identity_card == "Driver\'s License" && identity_card !== hidden_idname){
        $('#showhideidentitycard').show();


        // $("#imgInp3_front").val();
        // $("#imgInp3_back").val();

        //  $('#createDynamicElement1').html('<input type="file" id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg"/>');
        // $('#createDynamicElement').html('<input type="file" id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg"/>');


        $('#img_contain3_front').html('<img id="blah3_front" align="middle" src="<?=$base_url?>images/id_proof_front/dlfront.jpg" style="width: 275px; height: 200px"/>');
        $('#img_contain3_back').html('<img id="blah3_back" align="middle" src="<?=$base_url?>images/id_proof_back/dlback.jpg" style="width: 275px; height: 200px"/>');


        }
        else if(identity_card == "Passport" && identity_card !== hidden_idname){
        $('#showhideidentitycard').show();

        // $("#imgInp3_front").val();
        // $("#imgInp3_back").val();


        //  $('#createDynamicElement1').html('<input type="file" id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg"/>');
        // $('#createDynamicElement').html('<input type="file" id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg"/>');

        $('#img_contain3_front').html('<img id="blah3_front" align="middle" src="<?=$base_url?>images/id_proof_front/passportfront.jpg" style="width: 275px; height: 200px"/>');
        $('#img_contain3_back').html('<img id="blah3_back" align="middle" src="<?=$base_url?>images/id_proof_back/passportback.jpg" style="width: 275px; height: 200px"/>');


        }
        else if(identity_card == "Voter ID" && identity_card !== hidden_idname){
        $('#showhideidentitycard').show();


        // $("#imgInp3_front").val();
        // $("#imgInp3_back").val();

        //  $('#createDynamicElement1').html('<input type="file" id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg" value=""/>');
        // $('#createDynamicElement').html('<input type="file" id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg" value=""/>');

        $('#img_contain3_front').html('<img id="blah3_front" align="middle" src="<?=$base_url?>images/id_proof_front/voteridfront.jpg" style="width: 275px; height: 200px"/>');
        $('#img_contain3_back').html('<img id="blah3_back" align="middle" src="<?=$base_url?>images/id_proof_back/voteridback.jpg" style="width: 275px; height: 200px"/>');

        }else{
        $("#identity_card_no").val(hidden_idno);

        // $("#imgInp3_front").val(hidden_id_front);
        // $("#imgInp3_back").val(hidden_id_back);



        // $('#createDynamicElement1').html('<input type="file" id="imgInp3_back" name="id_proof_back" accept="image/jpg, image/jpeg" value="<?=$row['id_proof_back']?>"/>');
        // $('#createDynamicElement').html('<input type="file" id="imgInp3_front" name="id_proof_front" accept="image/jpg, image/jpeg" value="<?=$row['id_proof_front']?>"/>');

        $('#img_contain3_front').html('<img id="blah3_front" align="middle" src="<?=$base_url?>images/'+hidden_id_front+'" style="width: 275px; height: 200px"/>');
        $('#img_contain3_back').html('<img id="blah3_back" align="middle" src="<?=$base_url?>images/'+hidden_id_back+'" style="width: 275px; height: 200px"/>');

        }
        });




    $("#imgInp3_front").change(function () {
      var fsize_front = this.files[0].size;
      var frontVal = $("#imgInp3_front").val();
      check_file_type_front(frontVal,fsize_front);

      console.log(frontVal);
      console.log("dsdsdsd");
    });

    $("#imgInp3_back").change(function () {
      var backVal = $("#imgInp3_back").val();
      var fsize_back = this.files[0].size;
      check_file_type_back(backVal,fsize_back);

    });


    $("#btn_back_address").unbind().click(function() {
      $('#personal_container').load('form_personal.php',function(e){
          $("#address_container" ).slideUp( "slow");
          $('#address_container').html('');
          //$("#personal_container" ).slideDown( "slow");
          $("#personal_container" ).slideDown( "slow", function(e) {
              window.scrollTo(0,400);
          });
      });
    });


    $("#btn_next_address").unbind().click(function() {

      var hidden_id_front = $('#hidden_id_front').val();
      var hidden_id_back = $('#hidden_id_back').val();
      var hidden_idname = $('#hidden_idname').val();
      var identity_card= $('#identity_card').val();

      //Solution for validating both front and back doc proofs
       var errorTextFront = $('#error_file_name').text();
       var errorTextBack = $('#error_file_name1').text();
       if(errorTextFront !== '' || errorTextBack !== ''){
        return false;
       }
       //Solution for validating both front and back doc proofs


      if(!$('#form_address').valid()){
              return false;
            }


      // check_file_type_back(backVal,fsize_back);
      // check_file_type_front(frontVal,fsize_front);





      // if(hidden_id_front =='' || hidden_id_back ==''){
      //       if(!$('#form_address').valid()){
      //         return false;
      //       }
      //   }



      // if(hidden_id_front =='' || hidden_id_back ==''){
      //   return false;
      // }else if(!$('#form_address').valid()){
      //   return false;
      // }

        if($('#file_extension_allow').val()==0){
          return false;
        }

      var formData = new FormData($('form#form_address')[0]);

        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            contentType: false,
            cache: false,

            processData:false,
            beforeSend: function() {
                    $('.loader').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
            },
            success: function(response) {
                if(response.status == 1){
                  $('#exam_container').load('form_exam.php',function(e){

                    $("#address_container" ).slideUp( "slow");
                    $('#address_container').html('');
                    $("#exam_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,600);
                     });

                  });

                   $('.loader').html('');
                }
            }
        });

    });

    $('#form_address').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "address_line1": {
            required: true
          },


          "block_or_tehsil": {
            required: false,
            maxlength: 60,
            customvalidationwithnumber: true
          },


          "state": {
            required: true
          },
          "city": {
            required: true
          },
          "pin_code": {
            required: true,
            number: true,
            maxlength: 6
          },
          "caddress_line1": {
            required: true
          },


          "cblock_or_tehsil": {
            required: false,
            maxlength: 60,
            customvalidationwithnumber: true
          },


          "cstate": {
            required: true
          },
          "ccity": {
            required: true
          },
          "cpin_code": {
            required: true,
            number:true,
            maxlength: 6
          },
          "identity_card": {
            required: true
          },
          "identity_card_no": {
            required: true,
            maxlength: 18,
            custombhamashah: true
          },
          "id_proof_front": {
            required: function(){
              if($('#writeFlag').html() == "true"){
                return true;

              }else{
              return false;

              }

            }
          },
          "id_proof_back": {
            required: function(){
              if($('#writeFlag').html() == "true"){
                return true;
              }else{
              return false;

              }

            }
          }


        },
        messages:
        {
         "address_line1": {
            required: "House No./Flat No./Building Name is required"


          },


          "block_or_tehsil": {
            required: "Village/Block/Tehsil/Locality/Landmark  is required",
            maxlength: "Max length 60 charecters allowed",
            customvalidationwithnumber: "Please enter valid Village/Block/Tehsil/Locality/Landmark"
          },

          "state": {
            required: "State is required"
          },
          "city": {
            required: "City is required"
          },
          "pin_code": {
            required: "Pincode is required",
          },
          "caddress_line1": {
            required: "House No./Flat No./Building Name   is required"
          },



          "cblock_or_tehsil": {
            required: "Village/Block/Tehsil/Locality/Landmark  is required",
            maxlength: "Max length 60 charecters allowed",
            customvalidationwithnumber: "Please enter valid Village/Block/Tehsil/Locality/Landmark"
          },


          "cstate": {
            required: "State is required"
          },
          "ccity": {
            required: "City is required"
          },
          "cpin_code": {
            required: "Pincode is required",
          },
          "identity_card": {
            required: "Identity Card type is Required"
          },
          "identity_card_no": {
            required: "Please fill your respective card no.",
            maxlength: "Max length allowed is 18 Digits",
            custombhamashah: "Sorry, special characters are not allowed"
          },
          "id_proof_front": {
            required: "ID Proof Front View is required",
          },
          "id_proof_back": {
            required: "ID Proof Back View is required",
            }

        }
  });
  $.validator.addMethod("customvalidation",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                return /^([a-zA-Z]+\s)*[a-zA-Z]*$/.test(value);

        },
    "Sorry, special characters , numbers & multiple whitespaces are not allowed"
    );
    $.validator.addMethod("customvalidationwithnumber",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                return /^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]*$/.test(value);

        },
    "Sorry, special characters  & multiple whitespaces are not allowed"
    );
    $.validator.addMethod("custombhamashah",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                return /^[a-zA-Z0-9]*$/.test(value);

        },
    "Sorry, special characters are not allowed"
    );



  $("#state").unbind().change(function() {
    var stateid = $(this).val();
    if(stateid !=''){
      $.ajax({
          type: "POST",
          url: "showcity.php",
          dataType: "json",
          data: "stateid=" + stateid,
          success: function (response) {
            $('#city').html(response.data);
          }
      });
    }
  })


  $("#country").unbind().change(function() {
    var countryid = $(this).val();
    if(countryid !=''){
      $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#state').html(response.data);
          }
      });
    }
  })





  if("<?=$row['country'] != ''?>"){

    var countryid = "<?php echo $row['country']; ?>";
    $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#state').html(response.data);
          }
      });
  }


  if("<?=$row['state'] != ''?>"){

    var stateid = "<?php echo $row['state']; ?>";

    $.ajax({
      type: "POST",
      url: "showcity.php",
      dataType: "json",
      data: "stateid=" + stateid,
      success: function (response) {
        $('#city').html(response.data);
      }
    });
  }


  $("#cstate").unbind().change(function() {
    var stateid = $(this).val();
    if(stateid !=''){
      $.ajax({
          type: "POST",
          url: "showcity.php",
          dataType: "json",
          data: "stateid=" + stateid,
          success: function (response) {
            $('#ccity').html(response.data);
          }
      });
    }
  })


  $("#ccountry").unbind().change(function() {
    var countryid = $(this).val();
    if(countryid !=''){
      $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#cstate').html(response.data);
          }
      });
    }
  })



  if("<?=$row['ccountry']?> !=''"){

    var countryid = "<?php echo $row['ccountry']; ?>";
    $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + countryid,
          success: function (response) {
            $('#cstate').html(response.data);
          }
      });
  }


  if("<?=$row['cstate']?> !=''"){

    var stateid = "<?php echo $row['cstate']; ?>";

    $.ajax({
      type: "POST",
      url: "showcity.php",
      dataType: "json",
      data: "stateid=" + stateid,
      success: function (response) {
        $('#ccity').html(response.data);
      }
    });
  }


$('#addresscheck').click(function() {
    var c=$('#addresscheck').val();
    if ($('#addresscheck').is(':checked')) {
    	var al1=$('#address_line1').val();
    ///	var al2=$('#address_line2').val();


    	var block_or_tehsil=$('#block_or_tehsil').val();



    	var st=$('#state').val();

    	var ct=$('#city').val();
      var pc=$('#pin_code').val();
    	var cnt=$('#country').val();
    	$("#caddress_line1").val(al1);
    //	$("#caddress_line2").val(al2);


      $("#cblock_or_tehsil").val(block_or_tehsil);

    	$("#ccountry").val(cnt);
   		 $.ajax({
          type: "POST",
          url: "showstate.php",
          dataType: "json",
          data: "countryid=" + cnt,
          success: function (response) {
            $('#cstate').html(response.data);
            $("#cstate").val(st);
          }
      	});

   		 $.ajax({
	      type: "POST",
	      url: "showcity.php",
	      dataType: "json",
	      data: "stateid=" + st,
	      success: function (response) {
	        $('#ccity').html(response.data);
	        $("#ccity").val(ct);
	      }
	    });

    	$("#cpin_code").val(pc);
    }else{
      $("#caddress_line1").val('');
     // $("#caddress_line2").val('');
      $("#ccountry").val('');
      $("#cstate").val('');
      $("#ccity").val('');
      $("#cpin_code").val('');
      $("#cvillage_name").val('');
      $("#cblock_or_tehsil").val('');
    }
});






// Function related to image upload

function readURL3_front(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah3_front').attr('src', e.target.result);

    $('#blah3_front').hide();
    $('#blah3_front').fadeIn(650);

  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp3_front").change(function() {
  console.log('wqqwqwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww');
readURL3_front(this);
});


function readURL3_back(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah3_back').attr('src', e.target.result);

    $('#blah3_back').hide();
    $('#blah3_back').fadeIn(650);

  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp3_back").change(function() {
readURL3_back(this);
});



// This is the function to check image uploaded by user valid or not

function check_file_type_front(file_ext_front,fsize_front){



var errorMsg = '';



var fileExtension = ['jpeg','jpg'];

if(fsize_front < 102400 || fsize_front > 204800){

errorMsg = "Front view should be of minimum 100 kb and maximum 200 kb Size";

}


if($.inArray(file_ext_front.split('.').pop().toLowerCase(), fileExtension) == -1 ) {

  if(errorMsg !=''){
    errorMsg = errorMsg+", Front view  Supported file formats: jpg,jpeg";

  }else{
    errorMsg = " Front view  Supported file formats: jpg,jpeg";
  }
}

if(errorMsg !=''){

  $('#error_file_name').html(errorMsg);
  $('#file_extension_allow').val(0);
  return false;
  console.log('False 1 Returned');

}else{

  $('#error_file_name').html('');
  $('#file_extension_allow').val(1);
}

}




function check_file_type_back(file_ext_back,fsize_back){

var errorMsg1 = '';


var fileExtension = ['jpeg','jpg'];

if(fsize_back < 102400 || fsize_back > 204800){

errorMsg1 = "Back view should be of minimum 100 kb and maximum 200 kb Size";

}


if($.inArray(file_ext_back.split('.').pop().toLowerCase(), fileExtension) == -1 ) {

  if(errorMsg1 !=''){
    errorMsg1 = errorMsg1+", Back view Supported file formats: jpg,jpeg";

  }else{
    errorMsg1 = " Back view Supported file formats: jpg,jpeg";
  }
}

if(errorMsg1 !=''){

  $('#error_file_name1').html(errorMsg1);
  $('#file_extension_allow').val(0);

  console.log('False 2 Returned');
  return false;

}else{

  $('#error_file_name1').html('');
  $('#file_extension_allow').val(1);
}

}
//Function related to image upload

// console.log(errorMsg+' Console '+errorMsg1)


});
</script>