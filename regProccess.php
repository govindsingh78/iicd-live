<?php 
    session_start();
    include 'meekrodb.2.3.class.php';
    require_once "phpmailer/class.phpmailer.php";
    
    $email = $_POST['email'];
    $data = DB::queryFirstRow("SELECT * FROM users WHERE email = '$email'");
    $recaptcha = $_POST['captcha_code'];
    
    if(!$data){
         if($_SESSION['captcha_code'] == $_POST['captcha_code']){

            $first_name = $_POST['first_name'];
            $middle_name = $_POST['middle_name'];
            $last_name = $_POST['last_name'];
            $email = $_POST['email'];
            $password = md5($_POST['password']);
                
            $otp = mt_rand(111111,999999);
            $_SESSION['otp'] = $otp;
            
            $_SESSION['first_name'] = $first_name;
            $_SESSION['middle_name'] = $middle_name;
            $_SESSION['last_name'] = $last_name;
            $_SESSION['uemail'] = $email;
            $_SESSION['password'] = $password;  

            $message = "<body background='images/main-body-bg.png'> 
                        <div style='width:100%; text-align:center;' >
                            <div style='width:600px; margin:0 auto; background-color:#fff; padding:15px; box-shadow: 0px 0px 10px 2px #ccc;'>
                                    <h1 style='text-align: center;'><img src='https://www.iicd.ac.in/images/company-logo1.png' alt='logo'></h1>
                                    <h2 style='text-align: center;'> Dear ".$first_name.", </h2>
                                      <P>Welcome to IICD!!<P>
                                      <P>
                                        Your one-time OTP for the verification of E-mail Id is: <strong> ".$otp." </strong>.
                                        </P>
                                        <P>******************************************************* </P>
                                        <P>This is an auto-generated email. Do not reply to this email.</P>
                              </div>
                            </div>
                </body>";        
        
            // php mailer code starts
            $mail = new PHPMailer(true);
            $mail->IsSMTP(); // telling the class to use SMTP
        
            $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $mail->Port = 465;                   // set the SMTP port for the GMAIL server
        
            $mail->Username = 'admissions@iicd.ac.in';
            $mail->Password = 'admission915';
        
            $mail->SetFrom('admissions@iicd.ac.in', 'IICD');
            $mail->AddAddress($email);
        
            $mail->Subject = trim("Email Verifcation - IICD");
            $mail->MsgHTML($message);
        
            try {
                $mail->send();
                $result['data'] = $_SESSION;
                $result['success'] = 'success';
                $result['message'] = 'Registered successfully, Now proceed with registration form submission by login...';
                echo json_encode($result);
            } 
            catch (Exception $ex) {
                $msg = $ex->getMessage();
                $result['error'] = 'error';
                $result['message'] = $msg;
                echo json_encode($result);
            }
         }
         else {
            $result['recaptcha'] = [];
             $result['recaptcha']['error'] = 'error';
            $result['recaptcha']['message'] = 'Captcha did not matched,Please re-enter your Captcha';
            echo json_encode($result);
         }
    }
    else {
        $result['email_err'] = [];
        $result['email_err']['error'] = 'error';
        $result['email_err']['message'] = 'Email already exists';
        echo json_encode($result);
    }
?>