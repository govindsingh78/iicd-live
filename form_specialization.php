<?php
session_start();
include 'meekrodb.2.3.class.php';

$query = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";
$row = DB::queryFirstRow($query);

//$specialisations = DB::query("select * from specialisations");
$specialisations = array('Fired Material Design' => 'Fired Material Design', 'Soft Material Design' => 'Soft Material Design', 'Hard Material Design' => 'Hard Material Design', 'Fashion Design' => 'Fashion Design');

$specialisationsp = array('Fired Material Specialization' => 'Fired Material Specialization', 'Hard Material Specialization' => 'Hard Material Specialization', 'Soft Material Specialization' => 'Soft Material Specialization');
?>
<form id="form_spec" name="form_spec">
   <div class="my-dtl-feed">
    <div class="col-md-12">
  <?php if ($row['Programme'] == 'UG') {?>

   <div class="group" id="ugs">
          <div class="col-md-4">
           <div class="my-input-bx  field required-field">
              <div class="selectContainer">
                  <label class="my-label">First Choice
                  </label>
                   <span class="bar"></span>
                    <select id="specialization_choice1" name="specialization_choice1" class="form-control"  required>
                    <option value="">Select Specialization</option>

                      <?php
foreach ($specialisations as $val) {
    $selected = '';
    if ($val == $row['specialization_choice1']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
}
    ?>
                    </select>
               </div>
           </div>
          </div>
          <div class="col-md-4">
             <div class="my-input-bx  field required-field">
                <div class="selectContainer">
                    <label class="my-label">Second Choice
                    </label>
                     <span class="bar"></span>
                    <select id="specialization_choice2" name="specialization_choice2" class="form-control" required>
                   <option value="">Select Specialization</option>
                        <?php
foreach ($specialisations as $val) {
        $selected = '';
        if ($val == $row['specialization_choice2']) {
            $selected = 'selected="selected"';
        }
        echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
    }
    ?>
                      </select>
                 </div>
             </div>
          </div>

          <div class="col-md-4">
            <div class="my-input-bx  field required-field">
                <div class="selectContainer">
                    <label class="my-label">Third Choice
                    </label>
                     <span class="bar"></span>
                    <select id="specialization_choice3" name="specialization_choice3" class="form-control"  required>
                    <option value="">Select Specialization</option>
                        <?php
foreach ($specialisations as $val) {
        $selected = '';
        if ($val == $row['specialization_choice3']) {
            $selected = 'selected="selected"';
        }
        echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
    }
    ?>
                    </select>
                 </div>
            </div>
          </div>
    </div>
  <?php } else {
    ?>


    <div class="group" id="pgs">
          <div class="col-md-4">
           <div class="my-input-bx field required-field">
              <div class="selectContainer">
                  <label class="my-label">First Choice
                  </label>
                   <span class="bar"></span>
                    <select id="specialization_choice1" name="specialization_choice1" class="form-control">
                    <option value="">Select Specialization</option>

                      <?php
foreach ($specialisationsp as $val) {
        $selected = '';
        if ($val == $row['specialization_choice1']) {
            $selected = 'selected="selected"';
        }
        echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
    }
    ?>
                    </select>
               </div>
           </div>
          </div>
          <div class="col-md-4">
             <div class="my-input-bx  field required-field">
                <div class="selectContainer">
                    <label class="my-label">Second Choice
                    </label>
                     <span class="bar"></span>
                    <select id="specialization_choice2" name="specialization_choice2" class="form-control">
                   <option value="">Select Specialization</option>
                        <?php
foreach ($specialisationsp as $val) {
        $selected = '';
        if ($val == $row['specialization_choice2']) {
            $selected = 'selected="selected"';
        }
        echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
    }
    ?>
                      </select>
                 </div>
             </div>
          </div>

          <div class="col-md-4">
            <div class="my-input-bx  field required-field">
                <div class="selectContainer">
                    <label class="my-label">Third Choice
                    </label>
                     <span class="bar"></span>
                    <select id="specialization_choice3" name="specialization_choice3" class="form-control">
                    <option value="">Select Specialization</option>
                        <?php
foreach ($specialisationsp as $val) {
        $selected = '';
        if ($val == $row['specialization_choice3']) {
            $selected = 'selected="selected"';
        }
        echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
    }
    ?>
                    </select>
                 </div>
            </div>
          </div>
    </div>

    <?php
}?>

      <nav class="form-section-nav">
        <input type="hidden" name="action" id="action" value="save_spec">
        <span id="btn_back_spec" class="btn-secondary form-nav-prev"><img src="images/left-arrow.jpg" alt="left"> Prev</span>
        <div class="loader" style="position: fixed; top: 35%; left: 48%;"></div><span id="btn_next_spec" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span>
      </nav>

    </div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function(){


    $("#btn_back_spec").unbind().click(function() {
      $('#language_container').load('form_language.php',function(e){
          $("#specialization_container" ).slideUp( "slow");
          $('#specialization_container').html('');
          $("#language_container" ).slideDown( "slow");
      });
    });


    $("#btn_next_spec").unbind().click(function() {

        if(!$('#form_spec').valid()){
          return false;
        }

      //  var formData = new FormData($('form#form_spec')[0]);
var formData = $('form#form_spec').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            dataType: "json",
            cache: false,
            beforeSend: function() {
                    $('.loader').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
            },
            success: function(response) {
              if(response.status == 1){
                $('#declaration_container').load('form_declaration.php',function(e){
                  $("#specialization_container" ).slideUp( "slow");
                  $('#specialization_container').html('');
                  //$("#attachment_container" ).slideDown( "slow");
                  $("#declaration_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,1150);
                     });
                });
              }
            }
        });

    });

    $('#form_spec').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "specialization_choice1": {
            required: true
          },
          "specialization_choice2": {
            required: true
          },
          "specialization_choice3": {

            required: true
          }

        },
        messages:
        {
         "specialization_choice1": {
            required: "Specialisations is required"
          },
          "specialization_choice2": {
            required: "Specialisations is required"
          },
          "specialization_choice3": {
            required: "Specialisations is required"
          }
        }
  });

$('select').on('change', function(event ) {
   var prevValue = $(this).data('previous');
$('select').not(this).find('option[value="'+prevValue+'"]').show();
   var value = $(this).val();
  $(this).data('previous',value); $('select').not(this).find('option[value="'+value+'"]').hide();
});
$('select').trigger('change');

});
</script>