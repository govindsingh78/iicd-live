<form id="form_dec" name="form_dec">
  <div class="row">
    <div class="col-md-12">
        <div class="agr-check-bx">
            <h4> Declaration Of Candidate</h4>
              <div class="ok-polick-chk" style="display: inline-flex;">
                <input type="checkbox" id="chk_dec" name="declaration" class="form-control"><p>I hereby declare that all statements made in this application form are true, complete and correct to the best of my knowledge and belief. In the event of any information being found false, I understand that my candidature is liable to be canceled.
</p>
              </div>
          </div>
      </div>
  </div>

  <div class="col-md-3 pull-left">
    <button type="button" id="btn_back_dec" class="ripple login-btn">Back</button>
    </div>
    <div class="col-md-3 pull-right">
    <button type="button" id="btn_next_dec" class="ripple login-btn">Submit</button>
  </div>

</form>

<script type="text/javascript">
$(document).ready(function(){

    $("#btn_back_dec").unbind().click(function() {
      $('#specialization_container').load('form_specialization.php',function(e){
         $("#declaration_container" ).slideUp( "slow");
         $('#declaration_container').html('');
         $("#specialization_container" ).slideDown( "slow");
      });
    });

    $("#btn_next_dec").unbind().click(function() {
        if(!$('#form_dec').valid()){
          return false;
        }
        /*Analytics code tracking*/
        ga('send', 'event', 'form', 'submit', 'submit_form');
        /*/Analytics code tracking*/
        window.location = 'payment.php';
    });

    $('#form_dec').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "declaration": {
            required: true
          },
        },
        messages:
        {
         "declaration": {
            required: "You need to agree with candidate declaration"
          }
        }
  });










});
</script>
