<?php
session_start();
include 'meekrodb.2.3.class.php';

$query = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";
$queryu = "select * from users where id = '" . $_SESSION['user_id'] . "'";

$row = DB::queryFirstRow($query);
$rowuser = DB::queryFirstRow($queryu);

$dob = '';

$male_selected = 'selected="selected"';
$female_selected = '';

if ($row['dob'] != '' && $row['dob'] != '0000-00-00') {
    $dob = date('m/d/Y', strtotime($row['dob']));
}

if ($row['gender'] != '' && trim($row['gender']) == 'female') {
    $male_selected = '';
    $female_selected = 'selected="selected"';
}

$m_status = array('Single' => 'Single', 'Married' => 'Married', 'Separated' => 'Separated', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced');

$category = array('General' => 'General', 'OBC' => 'OBC', 'SC' => 'SC', 'ST' => 'ST', 'PH' => 'PH');

$countries = DB::query("select * from country");

$domicile = array('yes' => 'Yes', 'no' => 'No');

$income = array('Below 1 lac' => 'Below 1 lac', '1 lac to 5 lacs' => '1 lac to 5 lacs', '5 lacs to 10 lacs' => '5 lacs to 10 lacs', '10 lacs to 15 lacs' => '10 lacs to 15 lacs', '15 lacs to 20 lacs' => '15 lacs to 20 lacs', '20 lacs to Above' => '20 lacs to Above');
$religion = array('Hindu' => 'Hindu', 'Muslim' => 'Muslim', 'Sikh' => 'Sikh', 'Christian' => 'Christian', 'Jain' => 'Jain', 'Buddhist' => 'Buddhist', 'Other' => 'Other');
$minority = array('Yes' => 'Yes', 'No' => 'No');
$bplapl = array('Yes' => 'Yes', 'No' => 'No');
$castecategory = array('General' => 'General', 'SC' => 'SC', 'ST' => 'ST', 'BC- Non Creamy Layer' => 'BC- Non Creamy Layer', 'MBC- Non Creamy Layer' => 'MBC- Non Creamy Layer');
$studentarea = array('Urban' => 'Urban', 'Rural' => 'Rural');
$othercategory = array('Kashmiri Migrants (KM)' => 'Kashmiri Migrants (KM)', 'Differently/Specially Abled' => 'Differently/Specially Abled', 'None' => 'None');

// Dynamic value for images section

$base_url = 'https://www.iicd.ac.in/';

$photo_src = $base_url . 'images/default.png';
if ($row['applicant_photo'] != '') {
    $photo_src = $base_url . 'images/' . $row['applicant_photo'];
}

$dob_src = $base_url . 'images/default.png';
if ($row['dob_certificate'] != '') {
    $dob_src = $base_url . 'images/' . $row['dob_certificate'];
}

$sig_src = $base_url . 'images/default.png';
if ($row['signatures'] != '') {
    $sig_src = $base_url . 'images/' . $row['signatures'];
}

?>

<link rel="stylesheet" href="css/bootstrap-datepicker3.standalone.min.css">
<script src="js/bootstrap-datepicker.min.js"></script>

<form id="form_personal" name="form_personal">
  <div class="my-dtl-feed">
<div class="col-md-12">

  <div class="group">
        <div class="col-md-3">
           <div class="my-input-bx field required-field">
              <label class="my-label"> Candidate Name</label>
              <span class="bar"></span>
              <input type="text" id="name" title="Candidate name" class="form-control" disabled="disabled" value="<?php echo $rowuser['first_name'] . ' ' . $rowuser['middle_name'] . ' ' . $rowuser['last_name'] ?>">
           </div>
        </div>

        <div class="col-md-3">
           <div class="my-input-bx field required-field">
              <label class="my-label">Mobile Number (Candidate)</label>
              <span class="bar"></span>
              <input type="text" id="phone" name="phone" value="<?=$row['phone']?>" class="form-control" required>


           </div>
        </div>

        <div class="col-md-3">
           <div class="my-input-bx field required-field">
           <label class="my-label"> Candidate Email</label>
              <span class="bar"></span>
              <input type="text" id="email" name="email" class="form-control" disabled="disabled" value="<?php echo $_SESSION['uemail'] ?>">
           </div>
        </div>

 <div class="col-md-3">
          <div class="my-input-bx field required-field" >

              <label class="my-label">Date of Birth</label>
              <span class="bar"></span>
              <input type="text" id="dob" name="dob" class="form-control" value="<?=$dob?>" required title="Date of Birth">

          </div>
        </div>

    </div>






    <div class="group">


        <div class="col-md-4">
          <div class="my-input-bx field required-field" >
                <div class="selectContainer" >
                    <label class="my-label">Gender</label>
                     <span class="bar"></span>
                    <select id="gender" name="gender" class="form-control">
                      <option value="male" <?=$male_selected?> >Male</option>
                      <option value="female" <?=$female_selected?>>Female</option>
                    </select>
                 </div>
            </div>
        </div>

        <div class="col-md-4 ">
              <div class="my-input-bx field required-field">
                <div class="selectContainer">
                    <label class="my-label">Marital status</label>
                     <span class="bar"></span>
                      <select name="marital_status" class="form-control">
                      <?php
foreach ($m_status as $key => $val) {
    $selected = '';
    if ($key == $row['marital_status']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
                      </select>
                 </div>
              </div>
        </div>


 <div class="col-md-4">
          <div class="my-input-bx field required-field">
              <div class="selectContainer">
                  <label class="my-label">Nationality</label>
                   <span class="bar"></span>
                  <select id="nationality" name="nationality" class="form-control">
                    <option value="">Select Nationality</option>
                    <?php
$india = false;
foreach ($countries as $val) {
    $selected = '';
    if ($val['country_name'] == $row['nationality']) {
        if ($row['nationality'] == 'India') {
            $india = true;
        }
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $val['country_name'] . '" ' . $selected . '>' . $val['country_name'] . '</option>';
}
?>
                  </select>
               </div>
          </div>
        </div>


    </div>



    <div class="group">
      <div class="col-md-4">
         <div class="my-input-bx field required-field">

            <label class="my-label">Father's Name</label>
            <span class="bar"></span>
            <input type="text" id="fathers_name" name="fathers_name" value="<?=$row['fathers_name']?>" class="form-control" required>

         </div>
      </div>

      <div class="col-md-4">
         <div class="my-input-bx field required-field">
            <label class="my-label">Father`s Mobile Number</label>
            <span class="bar"></span>
            <input type="text" id="phone_father" name="phone_father" value="<?=$row['phone_father']?>" class="form-control" required>


         </div>
      </div>

      <div class="col-md-4">
         <div class="my-input-bx field required-field">

         <label class="my-label">E-mail Id (Father)</label>
            <span class="bar"></span>
            <input type="text" id="email_father" name="email_father" value="<?=$row['email_father']?>" class="form-control" required>

         </div>
      </div>

    </div>
    <div class="group">


     <div class="col-md-4">
         <div class="my-input-bx field required-field">

            <label class="my-label">Mother's Name</label>
            <span class="bar"></span>
            <input type="text" id="mothers_name" name="mothers_name" value="<?=$row['mothers_name']?>" class="form-control" required>

         </div>
      </div>

      <div class="col-md-4">
         <div class="my-input-bx">
         <label class="my-label">Mother`s Mobile Number</label>
                  <span class="bar"></span>
            <input type="text" id="phone_mother" name="phone_mother" value="<?=$row['phone_mother']?>" class="form-control"  >
            <span class="bar"></span>

         </div>
      </div>

      <div class="col-md-4">
         <div class="my-input-bx">
            <input type="text" id="email_mother" name="email_mother" value="<?=$row['email_mother']?>" class="form-control">
            <span class="bar"></span>
            <label>E-mail Id (Mother)</label>
         </div>
      </div>

    </div>

<!-- Inserting 9 extra new fields using 3 group -->
<div class="group">

    <div class="col-md-4">
      <div class="my-input-bx">
        <input type="text" id="bhamashah" name="bhamashah" value="<?=$row['bhamashah']?>" class="form-control">
        <span class="bar"></span>
        <label>Bhamashah</label>
      </div>
    </div>

     <div class="col-md-4">
      <div class="my-input-bx">
        <input type="text" id="alternate_email" name="alternate_email" value="<?=$row['alternate_email']?>" class="form-control">
        <span class="bar"></span>
        <label>Alternate Email</label>
      </div>
    </div>


    <div class="col-md-4">
          <div class="my-input-bx field required-field">
            <div class="selectContainer">
                  <label class="my-label">Religion
                 </label><span class="bar"></span>
                <select id="religion" name="religion" class="form-control">
                <option value="">Select Religion</option>
                    <?php
foreach ($religion as $key => $val) {
    $selected = '';
    if ($key == $row['religion']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
                </select>
             </div>
          </div>
        </div>

        <div class="col-md-4 pull-right" id="showhideReligion" style="clear: both; display: none">
        <div class="my-input-bx">
        <input type="text" id="other_religion" name="other_religion" value="<?=$row['other_religion']?>" class="form-control">
        <span class="bar"></span>
        <label>Other Religion</label>
        </div>
        </div>



</div>
<br/>

<div class="group">
<br/>


    <div class="col-md-4">
          <div class="my-input-bx field required-field">
            <div class="selectContainer">
                  <label class="my-label">Minority
                 </label><span class="bar"></span>
                <select id="minority" name="minority" class="form-control">
                <option value="">Select Minority</option>
                    <?php
foreach ($minority as $key => $val) {
    $selected = '';
    if ($key == $row['minority']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
                </select>
             </div>
          </div>
        </div>

    <div class="col-md-4">
          <div class="my-input-bx field required-field">
            <div class="selectContainer">
                  <label class="my-label">Is student belongs to BPL/APL?
                 </label><span class="bar"></span>
                <select id="bpl_apl" name="bpl_apl" class="form-control">
                <option value="">Select BPL/APL Category</option>
                    <?php
foreach ($bplapl as $key => $val) {
    $selected = '';
    if ($key == $row['bpl_apl']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
                </select>
             </div>
          </div>
        </div>


      <div class="col-md-4">
      <div class="my-input-bx field required-field">
      <input type="text" id="emergency_phone_no" name="emergency_phone_no" value="<?=$row['emergency_phone_no']?>" class="form-control">
      <span class="bar"></span>
      <label>Emergency Phone No.</label>
      </div>
      </div>


</div>


<div class="group">



<div class="col-md-4">
      <div class="my-input-bx field required-field">
        <div class="selectContainer">
              <label class="my-label">Caste Category
             </label><span class="bar"></span>
            <select id="caste_category" name="caste_category" class="form-control">
            <option value="">Select Caste Category</option>
                <?php
foreach ($castecategory as $key => $val) {
    $selected = '';
    if ($key == $row['caste_category']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
            </select>
         </div>
      </div>
    </div>

<div class="col-md-4">
      <div class="my-input-bx field required-field">
        <div class="selectContainer">
              <label class="my-label">Other Category
             </label><span class="bar"></span>
            <select id="other_category" name="other_category" class="form-control">
            <option value="">Select Other Category</option>
                <?php
foreach ($othercategory as $key => $val) {
    $selected = '';
    if ($key == $row['other_category']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
            </select>
         </div>
      </div>
    </div>


 <div class="col-md-4">
      <div class="my-input-bx field required-field">
        <div class="selectContainer">
              <label class="my-label">student belongs to Area?
             </label><span class="bar"></span>
            <select id="students_area" name="students_area" class="form-control">
            <option value="">Select Students belong to Area</option>
                <?php
foreach ($studentarea as $key => $val) {
    $selected = '';
    if ($key == $row['students_area']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
            </select>
         </div>
      </div>
    </div>

</div>
<!-- Inserting 9 extra new fields using 3 group -->







 <div class="group">

     <div class="col-md-4">
         <div class="my-input-bx field">

            <input type="text" id="guardians_name" name="guardians_name" value="<?=$row['guardians_name']?>" class="form-control">
           <span class="bar"></span>
            <label>Local Guardians's Name</label>
         </div>
      </div>

      <div class="col-md-4">
         <div class="my-input-bx">
         <label class="my-label">Local Guardians`s Mobile Number</label>
                  <span class="bar"></span>
            <input type="text" id="phone_guardian" name="phone_guardian" value="<?=$row['phone_guardian']?>" class="form-control"  >
            <span class="bar"></span>

         </div>
      </div>

      <div class="col-md-4">
         <div class="my-input-bx">
          <label class="my-label"> Relation with Local Guardians</label>
          <span class="bar"></span>
            <input type="text" id="guardians_relation" name="guardians_relation" value="<?=$row['guardians_relation']?>" class="form-control">
            <span class="bar"></span>
         </div>
      </div>
    </div>

<br/>
    <div class="group">

        <div class="col-md-4">
           <div class="my-input-bx ">
            <label class="my-label">Local Guardian E-mail</label>
            <span class="bar"></span>
             <input type="text" id="email_guardian" name="email_guardian" value="<?=$row['email_guardian']?>" class="form-control">
           </div>
        </div>

        <div class="col-md-4">
          <div class="my-input-bx field required-field">
            <div class="selectContainer">
                  <label class="my-label">Annual Family Income<br>
                <span class="label-subtxt">(Please submit the support document/certificate at the time admission)</span></label><span class="bar"></span>
                <select id="family_income" name="family_income" class="form-control">
                <option value="">Select Annual Family Income</option>
                    <?php
foreach ($income as $key => $val) {
    $selected = '';
    if ($key == $row['family_income']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
                </select>
             </div>
          </div>
        </div>

        <div class="col-md-4">
         <div class="my-input-bx field required-field">
          <div class="selectContainer">
              <label class="my-label">  Do you belong to a traditional or practicing craft family* <br>  <span class="label-subtxt">(If yes, please submit the support document / certificate at the time admission)</span></label><span class="bar"></span>
              <select id="craft_relation" name="craft_relation" class="form-control">
                <option value="">Select</option>
                  <?php
foreach ($domicile as $key => $val) {
    $selected = '';
    if ($key == $row['craft_relation']) {
        $selected = 'selected="selected"';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
}
?>
              </select>
           </div>
          </div>
        </div>

    </div>

     <div class="group" id="craftname">
       <div class="col-md-4">
         <div class="my-input-bx field required-field">
            <input type="text" id="craft_name" name="craft_name" value="<?=$row['craft_name']?>" class="form-control">
            <span class="bar"></span>
            <label>Craft Name</label>
         </div>
      </div>
     </div>




    <div class="group">
      <div class="col-md-12">
        <div class="my-input-bx field required-field">
          <label class="my-label"> Medical / Health Information
                  <br>
                  <span class="label-subtxt"> (Please mention any chronic health issue which needs attention) </span>
          </label>
          <textarea class="form-control" id="medical_info" name="medical_info" cols="12" col-md-12s="15"><?=$row['medical_info']?></textarea>
        </div>
      </div>



<!-- Placing first 2 image upload section Here starts -->
<br/>


<div class="group">
<div class="col-md-12">
  <div class="col-md-12" id="error_file_name"></div>
  <div class="col-md-12" id="error_file_name1"></div>
</div>
</div>


<br/>
    <div class="group">

            <div class="col-md-6">
            <label class="my-label" style="font-size: 10px">Photo of the applicant (approx size(360*450 px))</label>
            <p class="pho-txt">The photograph should be of minimum 100 kb and maximum 200 kb Size, Supported file format JPG, JPEG. </p>
              <input type='file' id="imgInp1" name="applicant_photo" accept="image/jpg, image/jpeg*" value="<?=$row['applicant_photo']?>"/>
              <div id='img_contain1'>
                <img id="blah1" align='middle' src="<?=$photo_src?>"/>
              </div>
            </div>

            <div class="col-md-6">
            <label class="my-label" style="font-size: 10px">Signature of the candidate</label>
            <p class="pho-txt"> The signature should be of minimum 20 kb and maximum 50 kb Size, Supported file format JPG, JPEG. </p>
            <input type='file' id="imgInp4" name="signatures" accept="image/jpg, image/jpeg" value="<?=$row['signatures']?>"/>
            <div id='img_contain4'>
            <img id="blah4" align='middle' src="<?=$sig_src?>"/>
            </div>
            </div>

    </div>
<!-- Placing first 2 image upload section Here ends -->

 <div id="writeFlag" style="display: none"></div>
 <div id="writeFlagPersonal" style="display: none"></div>

      <br>
      <nav class="form-section-nav">
        <input type="hidden" name="action" id="action" value="save_personal">
        <input type="hidden" name="hidden_photo" id="hidden_photo" value="<?=$row['applicant_photo']?>">
        <input type="hidden" name="hidden_sig" id="hidden_sig" value="<?=$row['signatures']?>">
        <input type="hidden" name="file_extension_allow" id="file_extension_allow" value="1">

        <span id="btn_back_personal" class="btn-secondary form-nav-prev"> <img src="images/left-arrow.jpg" alt="left"> Prev </span>
        <span id="btn_next_personal" class="btn-std form-nav-next"> Save & Next <img src="images/right-arrow.jpg" alt="left"></span><div class="loader" style="position: fixed; top: 35%; left: 48%;"></div>
      </nav>
    </div>
</div>
</div>
</form>
<style type="text/css">
  #error_file_name{
    color: red;
    font-size: 14px;
    margin-top: -55px;
    letter-spacing: 3px;
    font-style: oblique;
    font-weight: bold;
  }
  #error_file_name1{
    color: red;
    font-size: 14px;
    margin-top: -40px;
    letter-spacing: 3px;
    font-style: oblique;
    font-weight: bold;
    clear: both;
  }
</style>
<script type="text/javascript">
$(document).ready(function(){


        var hidden_photo = $('#hidden_photo').val();
        var hidden_sig = $('#hidden_sig').val();
        // var identity_card= $("#identity_card").val();
        if(hidden_photo !== ""){
        $('#writeFlag').text('false');
        }else{
        $('#writeFlag').text('true');
        }

        if(hidden_sig !== ""){
        $('#writeFlagPersonal').text('false');
        }else{
        $('#writeFlagPersonal').text('true');
        }


        var otherReligion= $("#religion").val();
        if(otherReligion == "Other"){
        $('#showhideReligion').show();
        }else{
        $('#showhideReligion').hide();
        }




      $("#imgInp1").change(function () {
      var photoType = "photo";
      var fsize = this.files[0].size;
      check_file_type(photoType,$(this).val(),fsize);
      });

      $("#imgInp4").change(function () {
      var photoType = "signature";
      var fsize = this.files[0].size;
      check_file_type1(photoType,$(this).val(),fsize);
      });


    $('#dob').datepicker({
      orientation: 'left bottom',
      autoclose: true,
      todayHighlight: true
    });

    $("#btn_back_personal").unbind().click(function() {
        $('#graduate_container').load('form_graduate.php',function(e){
          $("#personal_container" ).slideUp( "slow");
          $('#personal_container').html('');
          //$("#graduate_container" ).slideDown( "slow");
          $("#graduate_container" ).slideDown( "slow", function(e) {
              window.scrollTo(0,300);
          });
        });
    });
     <?php
if ($india) {?>
      $("#domicile").prop('disabled',false);
    <?php } else {?>
      $("#domicile").prop('disabled',true);
    <?php }?>
   // $("#domicile").val('no');
    $("#nationality").change(function () {
       var val = $(this).val();
       if (val != "India") {
         $("#domicile").prop('disabled',true);
         $("#domicile").val('no');
        }else{
          $("#domicile").prop('disabled',false);
          $("#domicile").val('yes');
        }
      });
    $('#craftname').hide();
     $("#craft_relation").change(function () {
       var val = $(this).val();
       if (val != "yes") {
         $('#craftname').hide();
        }else{
          $('#craftname').show();
        }
      });

     $("#phone_guardian").prop('disabled',true);
     $("#guardians_relation").prop('disabled',true);
     $("#email_guardian").prop('disabled',true);
     $("#guardians_name").blur(function () {
      var val = $(this).val();
      if (val !='') {
         $("#phone_guardian").prop('disabled',false);
         $("#guardians_relation").prop('disabled',false);
         $("#email_guardian").prop('disabled',false);
      }else{
       $("#phone_guardian").prop('disabled',true);
       $("#guardians_relation").prop('disabled',true);
       $("#email_guardian").prop('disabled',true);

      }

      });


    $("#btn_next_personal").unbind().click(function() {

      console.log('Govind');

        var hidden_photo = $('#hidden_photo').val();
        var hidden_sig = $('#hidden_sig').val();
        // if(hidden_photo !== ''){
        //   $("#imgInp1").val(hidden_photo);
        // }
        // if(hidden_sig !== ''){
        //   $("#imgInp4").val(hidden_sig);
        // }

       //Solution for validating both front and back doc proofs
       var errorTextFront = $('#error_file_name').text();
       var errorTextBack = $('#error_file_name1').text();
       if(errorTextFront !== '' || errorTextBack !== ''){
        return false;
       }
       //Solution for validating both front and back doc proofs

         console.log(hidden_photo);
         console.log(hidden_sig);
         if(!$('#form_personal').valid()){
              return false;
            }

        // if(hidden_photo =='' || hidden_sig ==''){
        //     if(!$('#form_personal').valid()){
        //       return false;
        //     }
        // }




        if($('#file_extension_allow').val()==0){
          return false;
        }



       var formData = new FormData($('form#form_personal')[0]);

       console.log(formData);
       //var formData = $('form#form_personal').serialize();
        $.ajax({
            type: "POST",
            url:"admission-save.php",
            data:  formData,
            data:  formData,
            dataType: "json",
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function() {
                    $('.loader').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
            },
            success: function(response) {
                if(response.status == 1){
                  console.log("Personel Upload works");
                  $('#address_container').load('form_address.php',function(e){

                        $("#personal_container" ).slideUp( "slow");
                        $('#personal_container').html('');
                        $("#address_container" ).slideDown( "slow", function(e) {
                           window.scrollTo(0,550);
                        });

                  });
                }
            }
        });

    });





    $('#form_personal').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "phone": {
            required: true,
            phoneno:true
          },
          "dob": {
            required: true
          },
          "gender": {
            required: true
          },
          "marital_status": {
            required: true
          },

          "nationality": {
            required: true
          },


          "email": {
            required: true,
            email :true
          },
          "fathers_name": {
            required: true,
            customvalidation: true
          },
          "mothers_name": {
            required: true,
            customvalidation: true
          },
          "bhamashah": {
            minlength: 7,
            maxlength: 7,
            custombhamashah: true
          },
          "alternate_email": {
            email: true,
            customemail: true

          },

          "religion": {
             required: true
          },
          "minority": {
             required: true
          },
          "bpl_apl": {
             required: true
          },

          "emergency_phone_no": {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10


          },
          "caste_category": {
            required: true

          },
          "other_category": {
            required: true

          },
          "students_area": {
            required: true

          },



          "phone_parents": {
            required: true,
            phoneno:true
          },
          "email_parents": {
            required: true,
            email :true
          },


          "craft_relation": {
             required: true
          },
          "family_income": {
            required: true
          },
          "craft_name": {
            required: function(element){
            return $("#craft_relation option:selected").val() == "yes";
            }
          },
          "medical_info": {
            required: true
          },

          "applicant_photo": {
            required: function(){
              if($('#writeFlag').html() == "true"){
                return true;

              }else{
              return false;

              }

            }
          },
          "signatures": {
            required: function(){
              if($('#writeFlagPersonal').html() == "true"){
                return true;
              }else{
              return false;

              }

            }
          }

        },
        messages:
        {
          "phone": {
            required: "Phone is required",
          },
         "dob": {
            required: "DOB is required"
          },
          "gender": {
            required: "Gender is required"
          },
          "marital_status": {
            required: "Marital Status is required"
          },

           "nationality": {
            required: "Nationality is required"
          },


          "email": {
            required: "Email is required",
            email: "Please enter a valid email address"
          },
          "fathers_name": {
            required: "Father`s name is required",
            customvalidation: "Sorry, no special characters and number allowed"
          },
          "mothers_name": {
            required: "Mother`s name is required",
            customvalidation: "Sorry, no special characters and number allowed"
          },

          "bhamashah": {
            minlength: "Bhamashah number must be 7 digits/charecter",
            maxlength: "Bhamashah number must be 7 digits/charecter",
            custombhamashah: "Sorry, special characters are not allowed"
          },
          "alternate_email": {
            email: "Please enter a valid email address",
            customemail: "Please enter a valid email address"

          },
          "religion": {
             required: "Religion is required "
          },
          "minority": {
             required: "Minority field is required "
          },
          "bpl_apl": {
             required: "Student belongs to BPL/APL is required"
          },

          "emergency_phone_no": {

           required: "Emergency Phone No is required",
           digits: "Only numbers are allowed ",
            minlength: "Minimum 10 digits allowed",
            maxlength: "Maximum 10 digits allowed"

          },
          "caste_category": {
            required: "Caste Category is required"

          },
          "other_category": {
            required: "Other Category is required"

          },
          "students_area": {
            required: "Student belongs to Urban/Rural is required"

          },
          "phone_parents": {
            required: "Parents phone is required",
          },
          "email_parents": {
            required: "Parents email is required",
            email: "Please enter a valid email address"
          },
          "craft_relation": {
            required: "Relation is required"
          },
          "family_income": {
            required: "Family income is required"
          },
          "craft_name": {
            required: "Craft name is required"
          },
          "medical_info": {
            required: "Medical information is required"
          },
          "applicant_photo": {
            required: "Applicant Photo is required",
          },
          "signatures": {
            required: "Applicant Signature is required",
            }
        }
    });


   $.validator.addMethod("customvalidation",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                return /^([a-zA-Z]+\s)*[a-zA-Z]+$/.test(value);

        },
    "Sorry, special characters , numbers & multiple whitespaces are not allowed"
    );

     $.validator.addMethod("customvalidationwithnumber",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                return /^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/.test(value);

        },
    "Sorry, special characters  & multiple whitespaces are not allowed"
    );

     $.validator.addMethod("custombhamashah",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                return /^[a-zA-Z0-9]+$/.test(value);

        },
    "Sorry, special characters are not allowed"
    );

      $.validator.addMethod("allowsomespecialcharwithsinglespace",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                ///^[-@./#&+\w\s]*$/
                return /^([(),-@./#&+\w]+\s)*[(),-@./#&+\w]+$/.test(value);

        },
    "Sorry, only  (),-@./#&+ special characters & single whitespace are allowed"
    );

    $.validator.addMethod("customemail",
        function(value, element) {
                //return /^[a-zA-Z ]+$/.test(value);
                ///^([a-zA-Z0-9]+\s)*[a-zA-Z0-9]+$/
                ///^[-@./#&+\w\s]*$/
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);

        },
    "Please enter a valid email !!"
    );

// Function related to image upload


function readURL(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah1').attr('src', e.target.result);

    $('#blah1').hide();
    $('#blah1').fadeIn(650);

  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp1").change(function() {
readURL(this);
});

function readURL4(input) {

if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
    $('#blah4').attr('src', e.target.result);

    $('#blah4').hide();
    $('#blah4').fadeIn(650);

  }

  reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp4").change(function() {
readURL4(this);
});


//new function to create other religion textbox

$('#religion').change(function(){
  var otherReligion= $(this).val();
  if(otherReligion == "Other"){
      $('#showhideReligion').show();
    }else{
      $('#showhideReligion').hide();
    }

});

// This is the function to check image uploaded by user valid or not

function check_file_type(photoType,file_ext,fsize){



var errorMsg = '';

var fileExtension = ['jpeg','jpg'];

if(photoType == "photo" && (fsize < 102400 || fsize > 204800)){

   errorMsg = "The photograph should be of minimum 100 kb and maximum 200 kb Size";
   console.log(fsize);

}



if($.inArray(file_ext.split('.').pop().toLowerCase(), fileExtension) == -1 ) {

  if(errorMsg !=''){
    errorMsg = errorMsg+", Supported file formats: jpg,jpeg";

  }else{
    errorMsg = "Supported file formats: jpg,jpeg";
  }
}

if(errorMsg !=''){

  $('#error_file_name').html(errorMsg);
  $('#file_extension_allow').val(0);
  return false;

}else{

  $('#error_file_name').html('');
  $('#file_extension_allow').val(1);
}

}

function check_file_type1(photoType,file_ext,fsize){



var errorMsg1 = '';

var fileExtension = ['jpeg','jpg'];

  if(photoType == "signature" && (fsize < 20480 || fsize > 51200)){
  errorMsg1 = "The signature should be of minimum 20 kb and maximum 50 kb Size";
  console.log(fsize);
}



if($.inArray(file_ext.split('.').pop().toLowerCase(), fileExtension) == -1 ) {

  if(errorMsg1 !=''){
    errorMsg1 = errorMsg1+", Supported file formats: jpg,jpeg";

  }else{
    errorMsg1 = "Supported file formats: jpg,jpeg";
  }
}

if(errorMsg1 !=''){

  $('#error_file_name1').html(errorMsg1);
  $('#file_extension_allow').val(0);
  return false;

}else{

  $('#error_file_name1').html('');
  $('#file_extension_allow').val(1);
}

}
//Function related to image upload
});

</script>