<?php
session_start();
include 'meekrodb.2.3.class.php';
$maxlifetime = ini_get("session.gc_maxlifetime");
$expireAfter = 60;

if (!isset($_SESSION['uemail'])) {
    header('location: admission.php');
}

if (isset($_SESSION['last_action'])) {
    $mnt = 59;

    $secondsInactive = time() - $_SESSION['last_action'];
    $expireAfterSeconds = $expireAfter * 60;

    $remaining_time = $expireAfterSeconds - $secondsInactive;
    $minutes = $remaining_time / 60;
    $minute = explode(".", $minutes);
    $mnt = $minute[0];
    if ($mnt >= 60) {
        $mnt = 59;
    }

    $sec = substr($minute[1], 0, 2);
    if ($sec <= 60) {
        $sec = $sec;
    } else {
        $sec = $sec - 59;
    }

    if (!$sec) {
        $sec = 59;
    }

    if ($secondsInactive >= $expireAfterSeconds) {
        session_unset();
        session_destroy();
        header('location: admission.php');
    }
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width">
<title>IICD | Register Form</title>
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
<style type="text/css">

#img_contain1{
border:1px solid grey;
margin-top:10px;
width:270px;

}

#imgInp1{
margin-left:1px;
padding:10px 0px 10px 0px;
background-color:#f7f1f1;

}
#blah1{
height:200px;
width: 270px;
display:block;
margin-left: auto;
margin-right: auto;
padding:5px;
}

#img_contain2{
border:1px solid grey;
margin-top:10px;
width:270px;

}

#imgInp2{
margin-left:1px;
padding:10px 0px 10px 0px;
background-color:#f7f1f1;

}
#blah2{
height:200px;
width: 270px;
display:block;
margin-left: auto;
margin-right: auto;
padding:5px;
}

#img_contain3{
border:1px solid grey;
margin-top:10px;
width:270px;

}

#imgInp3{
margin-left:1px;
padding:10px 0px 10px 0px;
background-color:#f7f1f1;

}
#blah3{
height:200px;
width: 270px;
display:block;
margin-left: auto;
margin-right: auto;
padding:5px;
}


#img_contain4{
border:1px solid grey;
margin-top:10px;
width:270px;

}

#imgInp4{
margin-left:1px;
padding:10px 0px 10px 0px;
background-color:#f7f1f1;

}
#blah4{
height:200px;
width: 270px;
display:block;
margin-left: auto;
margin-right: auto;
padding:5px;
}
</style>
<!-- Analytics Code -->
<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-75923092-1', 'auto');  ga('send', 'pageview'); </script>
<!-- Facebook Pixel Code -->
<script>
! function(f, b, e, v, n, t, s) {
if (f.fbq) return;
n = f.fbq = function() {
n.callMethod ?
n.callMethod.apply(n, arguments) : n.queue.push(arguments)
};
if (!f._fbq) f._fbq = n;
n.push = n;
n.loaded = !0;
n.version = '2.0';
n.queue = [];
t = b.createElement(e);
t.async = !0;
t.src = v;
s = b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t, s)
}(window, document, 'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1151196271591907');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=1151196271591907&ev=PageView
&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<div class="admission-form">
<div class="btn-std">
<a href="logout.php" class="btn btn-primary" style="color: #FFFFFF !important">
<i class="fa fa-power-off fa-fw pull-right"></i>
Log out
</a>
</div>
<div class="logout_time">
<P>Your session will logout in <span id="timer"><?php echo $mnt; ?>:<?php echo $sec; ?></span></P>
</div>
<div class="mrg-btm">
<div class="col-md-12 text-center">
<div class="company-logo">
<a href="#">
<img src="images/company-logo1.png" alt="">
</a>
</div>
<!-- Code to make title dynamic which can be change from admin panel setting tab -->
<?php
$headline1 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 12");
$headline2 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 13");
$headline3 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 14");
$headline4 = DB::queryFirstRow("SELECT * FROM settings WHERE id = 15");
?>
<h2> <?php echo strip_tags($headline1['value']); ?> </h2>
<div class="form-sub-title">
<span> <?php echo strip_tags($headline2['value']); ?>  </span><br>
<span> <?php echo strip_tags($headline3['value']); ?>  </span><br>
<span> <?php echo strip_tags($headline4['value']); ?>  </span>
</div>
<!-- Code to make title dynamic which can be change from admin panel setting tab -->

</div>
</div>
<div class="container">
<div class="row" style="padding: 10px">
<div class="col-sm-12 col-sm-offset-2 flash-message" style="margin: auto;"></div>
<?php
//##### make checkbox checked and proceed button disabled

$queryDisclaimer = "select * from user_details where user_id = '" . $_SESSION['user_id'] . "'";
$rowDisclaimer = DB::queryFirstRow($queryDisclaimer);
$disclaimer_check = $rowDisclaimer['disclaimer_check'];
if ($disclaimer_check == 1) {
//print in case candidate already agreed
    $checked_disclaimer = 'checked="checked"';
} else {
    $checked_disclaimer = '';
}
?>
<div class="col-sm-12 col-sm-offset-2" style="margin: auto; border: 1px solid">
<form id="form_disclaimer" name="form_disclaimer">
<h6><input type="checkbox" id="disclaimer_check" name="disclaimer_check" value="1" <?=$checked_disclaimer;?> /><strong>Documents:</strong> It is the duty of candidates to upload the documents in a recognizable and readable format as given in the Admissions Guideline. Inappropriate pictures and document / certificates / record will not be accepted. Candidates will have to resend the error-bearing documents. IICD will not be responsible for any mistake in information in the admission form.</h6>
<h6><strong>Payment:</strong> Candidates must ensure that they receive the reference-id upon completion of payment. In case they donot receive the payment reference id within 24 hr of payment completion of a bank working day, they are requested to contact IICD at admissions@iicd.ac.in with proper proof of payment (bank statement, credit/ debit card transaction report etc.). IICD will not be responsible for any loss of data due to internet or technical errors.</h6>
<nav class="form-section-nav">
<input type="hidden" name="action" id="action" value="save_disclaimer">
<?php
if ($disclaimer_check == 1) {
//print in case candidate already agreed
} else {
    echo '<span id="btn_next_disclaimer" class="btn-std form-nav-next"> Agree &amp; Proceed <img src="images/right-arrow.jpg" alt="left"></span><div class="loaderDisclaimer" style="position: fixed; top: 35%; left: 48%;"></div>';
}
?>
</span>
</nav>
</div>





</div>
</div>
<main>
<fieldset>
<legend> Step 1 : Select Graduate Programme </legend>
<div id="graduate_container" class="form-section"></div>
</fieldset>

<fieldset>
<legend>Step 2 : Personal Details </legend>
<div id="personal_container" class="form-section"></div>

</fieldset>

<fieldset>
<legend> Step 3 : Address </legend>
<div id="address_container"  tabindex='1' class="form-section" style="overflow: hidden;
height: auto;"></div>
</fieldset>

<fieldset>
<legend>Step 4 : Exam Center</legend>

<div id="exam_container" class="form-section"></div>
</fieldset>

<fieldset>
<legend> Step 5 : 10+2 / HS </legend>
<div id="hs_container" class="form-section"></div>
</fieldset>

<fieldset id="degree_section">
<legend> Step <span id="lbl_deg">6</span> : Degree
</legend>
<div id="degree_container" class="form-section"></div>
</fieldset>

<fieldset>
<legend> Step <span id="lbl_lng">7</span> : Language Known</legend>
<div id="language_container" class="form-section"></div>
</fieldset>

<fieldset>
<legend> Step <span id="lbl_spec">8</span> : Preferences For Specialization</legend>
<div id="specialization_container" class="form-section"></div>
</fieldset>

<!-- <fieldset>
<legend> Step <span id="lbl_attach">9</span> : Attachments</legend>
<div id="attachment_container" class="form-section"></div>
</fieldset> -->

<div id="declaration_container"></div>
</main>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/formalize.js" type="text/javascript"></script>

<script type="text/javascript" src="js/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

//####### in case user had already agreed to disclaimer #####//
<?php if ($disclaimer_check == 1) {?>
$('#graduate_container').load('form_graduate.php',function(e){
});
<?php }?>


//####### Adding one more step Disclaimer #######//

$("#btn_next_disclaimer").unbind().click(function() {

if(!$('#form_disclaimer').valid()){
        return false;
}

var formData = new FormData($('form#form_disclaimer')[0]);

  $.ajax({
      type: "POST",
      url:"admission-save.php",
      data:  formData,
      dataType: "json",
      contentType: false,
      cache: false,

      processData:false,
      beforeSend: function() {
              $('.loaderDisclaimer').html('<img src="admin/images/spinner.gif" alt="" width="45" height="45">');
      },
      success: function(response) {
          if(response.status == 1){
            $('#graduate_container').load('form_graduate.php',function(e){
            //   $("#address_container" ).slideUp( "slow");
               $('#btn_next_disclaimer').css('display', 'none');
              $("#graduate_container" ).slideDown( "slow", function(e) {
                     window.scrollTo(0,400);
               });

            });

             $('.loaderDisclaimer').html('');
          }
      }
  });

});

$('#form_disclaimer').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "disclaimer_check": {
            required: true
          }


        },
        messages:
        {
            "disclaimer_check": {
            required: "Disclaimer is mandatory to check before proceeding to registeration!",
            }

        }
  });

//####### Adding one more step Disclaimer #######//

//var hidden_id_front = $('#hidden_id_front').val();

// <form id="form_disclaimer" name="form_disclaimer">

// $('#graduate_container').load('form_graduate.php',function(e){
// });

$.validator.addMethod("customvalidation",
function(value, element) {
return /^[a-zA-Z ]+$/.test(value);
},
"Sorry, no special characters and number allowed"
);

jQuery.validator.addMethod("phoneno", function(phone_number, element) {
phone_number = phone_number.replace(/\s+/g, "");
return this.optional(element) || phone_number.length == 10 &&
phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
}, 'Enter valid phone number');

jQuery.validator.addMethod("validate_pincode",function(value, element) {

if(value !=''){
if(/(^\d{6}$)/.test( value )){
return true;
}else{
return false;
}
}else{
return true;
}
},"Enter a valid pincode.");

});
</script>



<script type="text/javascript">
$(document).ready(function(){
$('.flash-message').html('<div class="alert alert-success" id="flash-msg">Login Successfully</div>');
$("#flash-msg").delay(5000).fadeOut("slow");

document.getElementById('timer').innerHTML = <?php echo $mnt; ?> + ":" + <?php echo $sec; ?>;
startTimer();

function startTimer() {
var presentTime = document.getElementById('timer').innerHTML;
var timeArray = presentTime.split(/[:]+/);
var m = timeArray[0];
var s = checkSecond((timeArray[1] - 1));
if(s == 59){
m = m - 1;
}
if(m < 0){
window.location.href = 'https://www.iicd.ac.in/admission.php';
}

document.getElementById('timer').innerHTML = m + ":" + s;
setTimeout(startTimer, 1000);
}

function checkSecond(sec) {
if (sec < 10 && sec >= 0) {
sec = "0" + sec
}; // add zero in front of numbers < 10

if (sec < 0) {
sec = "59"
};
return sec;
}

$('#sectional').formalize({
timing: 300,
nextCallBack: function(){
if (validateEmpty($('#sectional .open'))){
scrollToNewSection($('#sectional .open'));
return true;
};
return false;
},
prevCallBack: function(){
return scrollToNewSection($('#sectional .open').prev())
}
});

$('input').on('keyup change', function(){
$(this).closest($('.valid')).removeClass('valid');
});

function validateEmpty(section){
var errors = 0;
section.find($('.required-field')).each(function(){
var $this = $(this),
input = $this.find($('input'));
if (input.val() === ""){
errors++;
$this.addClass('field-error');
$this.append('\<div class="form-error-msg">This field is required!\</div>');
}
});
if (errors > 0){
section.removeClass('valid');
return false;
}
section.find($('.field-error')).each(function(){
$(this).removeClass('field-error');
});
section.find($('.form-error-msg')).each(function(){
$(this).remove();
});
section.addClass('valid');
return true;
}

function scrollToNewSection(section){
var top = section.offset().top;
$("html, body").animate({
scrollTop: top
}, '200');
return true;
}
});
</script>

<script language="javascript" type="text/javascript">
$(document).ready(function() {
$('#country').change(function() {
$("option:selected", $(this)).each(function () {
var countryid = $(this).val();
$.ajax({
type: "POST",
url: "showstate.php",
data: "countryid=" + countryid,
success: function (data) {
$('#divcountry').html(data);
$('#divcountry').slideDown('slow');
}
});
});
});
});
</script>

<script language="javascript" type="text/javascript">
$(document).on('change', '#state', function() {

//  $('#state').change(function() {
$("option:selected", $(this)).each(function () {
var stateid = $(this).val();
$.ajax({
type: "POST",
url: "showcity.php",
data: "stateid=" + stateid,
success: function (data) {
$('#divstate').html(data);
$('#divstate').slideDown('slow');
}
});
});
});

</script>

<script>
function readURL(input) {

if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function(e) {
$('#blah1').attr('src', e.target.result);

$('#blah1').hide();
$('#blah1').fadeIn(650);

}

reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp1").change(function() {
readURL(this);
});
</script>
<script>
function readURL2(input) {

if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function(e) {
$('#blah2').attr('src', e.target.result);

$('#blah2').hide();
$('#blah2').fadeIn(650);

}

reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp2").change(function() {
readURL2(this);
});
</script>
<script>
function readURL3(input) {

if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function(e) {
$('#blah3').attr('src', e.target.result);

$('#blah3').hide();
$('#blah3').fadeIn(650);

}

reader.readAsDataURL(input.files[0]);
}
}

$("#imgInp3").change(function() {
readURL3(this);
});
</script>

</body>
</html>